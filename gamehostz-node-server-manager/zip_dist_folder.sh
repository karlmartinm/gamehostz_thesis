#!/bin/sh -l
apt-get install zip -y

mkdir zip
zip -r ./zip/"$1" dist/* node_modules
ls -lah
cd zip && ls -lah
