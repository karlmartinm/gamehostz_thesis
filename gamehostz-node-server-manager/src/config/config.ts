import { join } from 'path';
import { parseBoolean } from '../util/util';

require('dotenv').config({ path: join(__dirname, '../.env') });

export default {
	app: {
		port: process.env.APP_PORT || 3000,
		prod: process.env.APP_ENV === 'prod',
		storagePath: process.env.APP_STORAGE_PATH || '/storage',
		basePath: process.env.APP_BASE_PATH,
		uuid: process.env.APP_UUID || '',
		supportsSave: parseBoolean(process.env.APP_SUPPORTS_SAVE),
		isLoadGame: parseBoolean(process.env.APP_IS_LOAD_GAME)
	},
	graylog: {
		host: process.env.GL_HOST,
		port: process.env.GL_PORT,
		hostname: process.env.GL_HOSTNAME
	},
	java: {
		host: process.env.JAVA_HOST,
		port: process.env.JAVA_PORT || 80,
		authKey: process.env.JAVA_AUTHKEY
	},
	supported_games: [
		{
			name: 'mc',
			class_file: '../games/Minecraft',
			folder: 'minecraft',
			lgsm_name: 'mcserver',
			gamedig_name: 'minecraft',
			default_port: '25565',
			query_port: '25566',
			close_when_empty_for_sec: 300,
			empty_server_check_interval_sec: 30,
			max_wait_for_server_initial_start_sec: 900 // 15 min
		},
		{
			name: 'csgo',
			class_file: '../games/Csgo',
			folder: 'csgo',
			lgsm_name: 'csgoserver',
			gamedig_name: 'csgo',
			default_port: '27015',
			query_port: '27015',
			close_when_empty_for_sec: 300,
			empty_server_check_interval_sec: 30,
			max_wait_for_server_initial_start_sec: 1800 // 30min
		},
		{
			name: 'is',
			class_file: '../games/InsurgencySandstorm',
			folder: 'insurgencysandstorm',
			lgsm_name: 'inssserver',
			gamedig_name: 'insurgencysandstorm',
			default_port: '27102',
			query_port: '27131',
			close_when_empty_for_sec: 300,
			empty_server_check_interval_sec: 30,
			max_wait_for_server_initial_start_sec: 1800 // 30min
		}
	]
};

