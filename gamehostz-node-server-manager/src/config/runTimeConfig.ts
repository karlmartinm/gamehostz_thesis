import { Settings } from '../entities/GameSettings.interface';
import { GameConfig } from '../entities/GameConfig.interface';

export const runTimeConfig = {
	gameSettings: [] as Settings[],
	gameConfig: {} as GameConfig,
	facility: ''
};
