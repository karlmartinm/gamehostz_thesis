import * as child_process from "child_process";
import { spawnSync } from "child_process";
import * as fs from "fs";

import { Command, Commands } from '../entities/Commands.interface';
import { GameStatus } from './GameStatus';
import { GameStatuses } from '../entities/GameStatuses.enum';
import config from '../config/config';
import { runTimeConfig } from '../config/runTimeConfig';
import { shutdown } from '../functions/shutdown';
import graylog from '../functions/graylog';
import { sleep } from '../util/util';
import { ServerEventType } from '../entities/GameEvent.interface';
import { informJavaServerStop } from '../functions/callouts';

const logger = graylog();

export class GeneralSetup {
	private readonly commands: Commands;

	constructor(private gameStatus: GameStatus) {
		this.commands = {
			startGame: {
				cmd: `./${runTimeConfig.gameConfig.lgsm_name} start`,
				cwd: `${config.app.basePath}/${runTimeConfig.gameConfig.folder}`
			},
			restartGame: {
				cmd: `./${runTimeConfig.gameConfig.lgsm_name} restart`,
				cwd: `${config.app.basePath}/${runTimeConfig.gameConfig.folder}`
			},
			stopGame: {
				cmd: `./${runTimeConfig.gameConfig.lgsm_name} stop`,
				cwd: `${config.app.basePath}/${runTimeConfig.gameConfig.folder}`
			}
		};
	}

	async startGame(): Promise<void> {
		await this.gameStatus.setStatus(GameStatuses.StartingGame);
		this.runSyncCommand(this.commands.startGame);
	}

	async restartGame(): Promise<void> {
		await this.gameStatus.setStatus(GameStatuses.RestartingGame);
		this.runSyncCommand(this.commands.restartGame);
	}

	async stopGame(): Promise<void> {
		await this.gameStatus.setStatus(GameStatuses.GameStopped);
		this.runSyncCommand(this.commands.stopGame);
	}

	async killVPS(eventType: ServerEventType): Promise<void> {
		await this.gameStatus.setStatus(GameStatuses.VPSKill);
		shutdown(eventType);
	}

	runGameCommand(cmd: string) {
		console.log('Game.runCommand');
		let terminal = child_process.spawn('bash');
		terminal.stdin.write(`tmux send -t ${runTimeConfig.gameConfig.lgsm_name}.0 "${cmd}" ENTER`);
		terminal.stdin.end();
	}

	// this should only be used internally - should NOT take user input
	private runSyncCommand(command: Command): void {
		console.log(`running command : ${command.cmd} in ${command.cwd}`);
		const commandKey = command.cmd.split(' ');
		const args = commandKey.slice(1, commandKey.length);
		let result = spawnSync(commandKey[0], args, {
			cwd: (command.cwd) ? command.cwd : process.cwd(),
			encoding: 'utf-8',
			stdio: 'pipe'
		});
		console.log(result.output);
	}

	private makeGameFolder(): void {
		console.log('makeGameFolder');
		if (!fs.existsSync(runTimeConfig.gameConfig.folder)) {
			fs.mkdirSync(runTimeConfig.gameConfig.folder);
		}
	}
}
