import * as SocketIO from 'socket.io';
import { Socket } from 'socket.io';
import { Tail } from 'tail';
import * as ip from 'ip';
import { QueryResult } from 'gamedig';
import { existsSync } from 'fs';
import { LocalStorage } from 'node-persist';
import { isEqual } from 'lodash';

import { GameStatus } from './GameStatus';
import { GeneralSetup } from './GeneralSetup';
import { ClientCommand } from '../entities/ClientCommand.interface';
import { GameStatuses } from '../entities/GameStatuses.enum';
import { GameDig } from './GameDig';
import {
	CLIENT_RESPONSE_EVENT,
	ClientResponse,
	MESSAGE_EVENT_TYPE,
	MessageStatusEvent,
	SOCKET_EVENT
} from '../entities/ClientResponse.interface';
import { runTimeConfig } from '../config/runTimeConfig';
import graylog from '../functions/graylog';
import config from '../config/config';
import { EventHandler } from './EventHandler';
import { ServerEventType } from '../entities/GameEvent.interface';

const logger = graylog();

export class GameManager {

	private client: Socket = null as any;
	private gameConsoleLog: Tail;
	private generalSetup: GeneralSetup = null as any;
	private gameSetup: any = null as any;
	private gameDig: GameDig;
	private readonly ip: string;
	private emptyServerSince: Date | null = null;
	private eventHandler: EventHandler;
	private lastGameDigStatus: QueryResult;
	private readonly emptyServerManagerIntervalFreq: number;
	private serverStateManagerInterval: NodeJS.Timeout = null as any;
	private hasGameInitiallyStarted = false;
	private readonly gameShouldStartBy: number;

	constructor(private gameStatus: GameStatus,
				private storage: LocalStorage,
				private io: SocketIO.Server) {
		this.ip = ip.address();
		this.emptyServerManagerIntervalFreq = Math.ceil(runTimeConfig.gameConfig.empty_server_check_interval_sec * 1000);
		this.gameDig = new GameDig(runTimeConfig.gameConfig);

		// noinspection JSIgnoredPromiseFromCall
		this.start();
		this.gameConsoleLog = null as any;
		this.eventHandler = new EventHandler();
		this.lastGameDigStatus = {} as QueryResult;
		this.gameShouldStartBy = Math.ceil(new Date().getTime() + runTimeConfig.gameConfig.max_wait_for_server_initial_start_sec * 1000);
	}

	async start() {
		try {
			this.generalSetup = new GeneralSetup(this.gameStatus);
			const GameController = require(runTimeConfig.gameConfig.class_file).default;
			this.gameSetup = new GameController(this.gameStatus);

			if (!this.gameStatus.isGameStatus(GameStatuses.StartingGame)) {
				await this.gameSetup.serverInit();
				await this.generalSetup.startGame();
			}
			this.startSocketIo();
			this.serverStateManagerInterval = setInterval(this.activeStateManager.bind(this), this.emptyServerManagerIntervalFreq);
		} catch (e) {
			logger.error(e);
			logger.error('app failed, panic and notify masters');
		}
	}

	private async activeStateManager() {
		if (!this.hasGameInitiallyStarted && new Date().getTime() > this.gameShouldStartBy) {
			// TODO server never started, shutdown without credit cost
			// TODO still leaves open the case when user immediately manages to send server stop command which will still
			this.serverFinalShutdown(ServerEventType.SHUTDOWN_NEVER_STARTED);
		}

		if (this.gameStatus.isGameStatus(GameStatuses.GameStopped)) {
			// TODO add delay
			// TODO stopped server should still close eventually
			return;
		}
		let result;
		try {
			result = await this.gameDig.run(this.ip) as QueryResult;
		} catch (e) {
			logger.error('Gamedig Failed. Game still starting up');
			return;
		}
		this.hasGameInitiallyStarted = true;

		this.emptyServerManager(result);

		if (isEqual(result, this.lastGameDigStatus)) {
			return;
		}

		this.lastGameDigStatus = result;
		await this.gameStatus.setStatus(GameStatuses.GameRunning);
		this.eventHandler.serverEvent(ServerEventType.PLAYER_COUNT, result.players.length);
		this.emit({ type: CLIENT_RESPONSE_EVENT.STATUS_EVENT, data: result } as ClientResponse);
	}

	private tail(): void {
		if (!existsSync(`${config.app.basePath}/${runTimeConfig.gameConfig.folder}/log/console/${runTimeConfig.gameConfig.lgsm_name}-console.log`)) {
			return;
		}
		this.gameConsoleLog = new Tail(`${config.app.basePath}/${runTimeConfig.gameConfig.folder}/log/console/${runTimeConfig.gameConfig.lgsm_name}-console.log`,
			{ fromBeginning: true }
		) as Tail;
		this.gameConsoleLog.on('line', (data: string) => {
			this.emit({ type: CLIENT_RESPONSE_EVENT.CONSOLE_EVENT, data } as ClientResponse);
		});
		this.gameConsoleLog.on('error', (error: any) => {
			logger.error('ERROR: ', error);
		});
	}

	startSocketIo() {
		logger.log('startSocketIo');
		this.io.on('connection', (client: Socket) => {
			logger.log('connectionEvent');
			this.client = client;
			this.client.on('action', this.ioAction.bind(this));
			this.client.on('disconnect', this.disconnectEvent.bind(this));

			this.eventHandler.serverEvent(ServerEventType.CLIENT_CONNECT);
			this.sendInitialDataToClient();
		});
	}

	emit(event: ClientResponse) {
		if (this.client && this.client.connected) {
			this.client.emit(SOCKET_EVENT.DATA, event);
		}
	}

	private sendInitialDataToClient(): void {
		console.log('this.sendInitialDataToClient');
		this.tail();
		this.emit({
			type: CLIENT_RESPONSE_EVENT.STATUS_EVENT,
			data: (isEqual(this.lastGameDigStatus, {})) ? 'offline' : this.lastGameDigStatus
		} as ClientResponse);
	}

	private ioAction(data: any): void {
		try {
			const command: ClientCommand = data;
			this.eventHandler.userCommandEvent(command);

			switch (command.cmd) {
				case 'start':
					this.generalSetup.startGame();
					break;
				case 'restart':
					this.generalSetup.restartGame();
					break;
				case 'stop':
					this.generalSetup.stopGame();
					break;
				case 'kill':
					this.serverFinalShutdown(ServerEventType.SHUTDOWN_USER);
					break;
				default:
					this.generalSetup.runGameCommand(command.cmd);
					break;
			}
			logger.log('action', data);
		} catch (e) {
			logger.error(e);
		}
	}

	private disconnectEvent(): void {
		logger.log('disconnect');
		this.client.disconnect(); // not needed as client disconnect
		if (this.gameConsoleLog !== null) {
			this.gameConsoleLog.unwatch();
			this.gameConsoleLog = null as any;
		}
		this.eventHandler.serverEvent(ServerEventType.CLIENT_DISCONNECT);
	}

	private emptyServerManager(qr: QueryResult): void {
		if (qr.players.length !== 0) {
			this.emptyServerSince = null;
			return;
		}

		if (this.emptyServerSince === null) {
			this.emptyServerSince = new Date();
			this.eventHandler.serverEvent(ServerEventType.SERVER_EMPTY);
			return;
		}

		const closeTime = Math.ceil(this.emptyServerSince.getTime() + (runTimeConfig.gameConfig.close_when_empty_for_sec * 1000));
		this.emit({
			type: CLIENT_RESPONSE_EVENT.MESSAGE_EVENT,
			data: { type: MESSAGE_EVENT_TYPE.SHUTDOWN_TIMER, value: closeTime } as MessageStatusEvent
		});

		if (closeTime < new Date().getTime()) {
			this.serverFinalShutdown(ServerEventType.SHUTDOWN_EMPTY);
		}
	}

	private serverFinalShutdown(eventType: ServerEventType) {
		clearInterval(this.serverStateManagerInterval);
		this.emit({
			type: CLIENT_RESPONSE_EVENT.MESSAGE_EVENT,
			data: { type: MESSAGE_EVENT_TYPE.SHUTDOWN } as MessageStatusEvent
		});
		this.eventHandler.serverEvent(eventType);
		this.generalSetup.killVPS(eventType);
	}
}
