import { query, QueryResult, Type } from 'gamedig';

import { GameConfig } from '../entities/GameConfig.interface';

export class GameDig {

	constructor(private readonly gameConfig: GameConfig) {
	}

	async run(ip: string): Promise<QueryResult> {
		const result = await query({
			type: this.gameConfig.gamedig_name as Type,
			host: ip,
			port: +this.gameConfig.query_port
		});
		delete result.raw;
		return result;
	}
}
