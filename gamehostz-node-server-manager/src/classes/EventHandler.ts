import {
	GameEvent, GameEventJsonValueType,
	GameEventServerEvent,
	GameEventSource,
	GameEventType,
	GameEventUserCommand,
	ServerEventType
} from '../entities/GameEvent.interface';
import config from '../config/config';
import { sendEvent } from '../functions/callouts';
import graylog from '../functions/graylog';
import { ClientCommand } from '../entities/ClientCommand.interface';

const logger = graylog();

class GameEventBuilder implements GameEvent {

	serverId: string = config.app.uuid;
	userId: string;
	source: GameEventSource = GameEventSource.SM;
	type: GameEventType;
	additionalJson: GameEventUserCommand | GameEventServerEvent;

	constructor(type: GameEventType, additionalJson: GameEventUserCommand | GameEventServerEvent) {
		this.userId = '';
		this.type = type;
		this.additionalJson = additionalJson;
	}
}

export class EventHandler {
	userCommandEvent(command: ClientCommand) {
		const userCommand: GameEventUserCommand = {
			value: command.cmd
		};
		const gameEvent = new GameEventBuilder(GameEventType.USER_COMMAND, userCommand);

		this.sendEvent(gameEvent);
	}

	serverEvent(type: ServerEventType, value?: GameEventJsonValueType) {
		const serverEvent: GameEventServerEvent = {
			value,
			type
		};
		const gameEvent = new GameEventBuilder(GameEventType.SERVER_EVENT, serverEvent);

		this.sendEvent(gameEvent);
	}

	private sendEvent(event: GameEvent) {
		sendEvent(event).catch(error => {
			logger.error(error);
		});
	}
}

