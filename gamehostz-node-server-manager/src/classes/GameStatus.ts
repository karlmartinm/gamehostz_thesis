import { GameStatuses } from '../entities/GameStatuses.enum';
import { LocalStorage } from 'node-persist';
import { GAME_STATUS_FILE } from '../constants/constants';

export class GameStatus {

	private _status: GameStatuses;

	constructor(private storage: LocalStorage) {
		this._status = GameStatuses.Init;
	}

	get status(): GameStatuses {
		return this._status;
	}

	async setStatus(value: GameStatuses) {
		this._status = value;
		await this.saveValue();
	}

	isGameStatus(value: GameStatuses): boolean {
		return (this._status === value);
	}

	private async saveValue(): Promise<void> {
		// probably fine to ignore async result
		await this.storage.setItem(GAME_STATUS_FILE, this._status);
	}
}
