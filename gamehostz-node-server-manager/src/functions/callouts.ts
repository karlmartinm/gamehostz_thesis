import { GameSettings, Gslt } from '../entities/GameSettings.interface';
import { get, post, del } from 'request-promise';
import config from '../config/config';
import { runTimeConfig } from '../config/runTimeConfig';
import { GameEvent, ServerEventType } from '../entities/GameEvent.interface';

export async function getGameSettings(): Promise<GameSettings> {
	return get(`http://${config.java.host}:${config.java.port}/api/internal/gameservers/game-config/${config.app.uuid}`, {
		headers: {
			Authorization: config.java.authKey
		},
		json: true
	});
}

export async function postInitiateShutdown(eventType: ServerEventType) {
	return post('http://127.0.0.1:3005', {
		body: {
			lgsm_name: runTimeConfig.gameConfig.lgsm_name,
			event_type: eventType
		},
		json: true
	});
}

export async function sendEvent(event: GameEvent): Promise<void> {
	return post(`http://${config.java.host}:${config.java.port}/api/internal/event`, {
		headers: {
			Authorization: config.java.authKey
		},
		body: { ...event, additionalJson: JSON.stringify(event.additionalJson) },
		json: true
	});
}

export async function getGSLT(): Promise<Gslt> {
	return post(`http://${config.java.host}:${config.java.port}/api/internal/gslt`, {
		headers: {
			Authorization: config.java.authKey
		},
		body: config.app.uuid,
		json: true
	});
}

export async function informJavaServerStop(uuid: string, shutdownEventType: ServerEventType): Promise<void> {
	return del(`http://${config.java.host}:${config.java.port}/api/internal/gameservers/${uuid}/${shutdownEventType}`, {
		headers: {
			Authorization: config.java.authKey
		},
		resolveWithFullResponse: true
	});
}
