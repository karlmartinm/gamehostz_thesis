import * as graylog2 from 'graylog2';

import config from '../config/config';

export default function logger(): graylog2.graylog | DevLogger {
	if (!config.graylog.host || !config.graylog.port) {
		throw new Error('No graylog configuration set');
	}
	// if (!config.app.prod) {
	return new DevLogger();
	// }
	// return new graylog2.graylog({
	// 	servers: [
	// 		{ host: config.graylog.host, port: +config.graylog.port }
	// 	],
	// 	hostname: config.graylog.hostname,
	// 	facility: config.app.uuid,
	// 	bufferSize: 1350
	// });
}

// Dev Mock graylog
class DevLogger {

	private static cl(message: any): void {
		console.log(message);
	}

	log(message: any): void {
		DevLogger.cl(message);
	}

	emergency(message: any): void {
		DevLogger.cl(message);
	}

	alert(message: any): void {
		DevLogger.cl(message);
	}

	critical(message: any): void {
		DevLogger.cl(message);
	}

	error(message: any): void {
		DevLogger.cl(message);
	}

	warning(message: any): void {
		DevLogger.cl(message);
	}

	notice(message: any): void {
		DevLogger.cl(message);
	}

	info(message: any): void {
		DevLogger.cl(message);
	}

	debug(message: any): void {
		DevLogger.cl(message);
	}

	close() {
	}
}
