import { informJavaServerStop, postInitiateShutdown } from './callouts';
import graylog from './graylog';
import { ServerEventType } from '../entities/GameEvent.interface';
import Signals = NodeJS.Signals;
import config from '../config/config';

const logger = graylog();

export const shutdownSignal = (signals: Signals) => {
	console.log('Shutdown with signal', signals);
	shutdown(null as any);
};

export const shutdown = async (eventType: ServerEventType) => {
	logger.close(function () {
		console.log('Graylog closed');
	});
	try {
		// TODO (not easy way to solve or even too necessary) gracefully close connections and stop socketio and http server
		console.log('shutdown actions');
		await informJavaServerStop(config.app.uuid, eventType);
		setTimeout(async () => {
			const shutdownResult = await postInitiateShutdown(eventType);
			console.log(shutdownResult);
			process.exit(0);
		}, 5000);
	} catch (e) {
		process.exit(0);
	}
};
