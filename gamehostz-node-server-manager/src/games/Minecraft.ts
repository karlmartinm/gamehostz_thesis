import { GameStatus } from '../classes/GameStatus';
import { GameStatuses } from '../entities/GameStatuses.enum';
import graylog from '../functions/graylog';
import { MinecraftServerProperties } from './game-settings/MinecraftServerProperties';
import { BaseGame } from './BaseGame';
import { GameInterface } from './GameInterface';
import config from '../config/config';

const logger = graylog();

export default class Minecraft extends BaseGame implements GameInterface {

	private minecraftServerProperties: MinecraftServerProperties;

	constructor(private gameStatus: GameStatus) {
		super();
		this.minecraftServerProperties = new MinecraftServerProperties();
	}

	async serverInit(): Promise<void> {
		logger.log('Minecraft.setup');
		await this.gameStatus.setStatus(GameStatuses.GameInitialize);
		try {
			await this.writeInitialGameSettings();
		} catch (e) {
			logger.error(e.message);
		}
		logger.log('done');
	}

	private async writeInitialGameSettings() {
		logger.log('Minecraft.writeGameConfig');
		await this.waitServerFolderCreation();
		if (config.app.isLoadGame) {
			await this.minecraftServerProperties.updateSettings();
		} else {
			await this.minecraftServerProperties.setInitialSettings();
		}
	}
}
