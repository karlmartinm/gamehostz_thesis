import {GameStatus} from '../classes/GameStatus';
import {runTimeConfig} from '../config/runTimeConfig';
import {Gslt, Settings} from '../entities/GameSettings.interface';
import {GameStatuses} from '../entities/GameStatuses.enum';
import {getGSLT} from '../functions/callouts';
import graylog from '../functions/graylog';
import {ensure} from '../util/util';
import {BaseGame} from './BaseGame';
import {ConfigSaver} from './game-settings/ConfigSaver';
import {GameInterface} from './GameInterface';
import * as ip from 'ip';

const logger = graylog();

export default class Csgo extends BaseGame implements GameInterface {
	private csgoGameModes: CsgoGamemode[] = [];
	private tickrate: String = "128";
	private hostname: String = "free GamehostZ.com CSGO server";
	private hostPlayersShow: String = "2";

	constructor(private gameStatus: GameStatus) {
		super();
		this.initCsgoGameModeTable();
	}

	private initCsgoGameModeTable() {
		this.csgoGameModes.push(new CsgoGamemode("Arms Race", "1", "0"));
		this.csgoGameModes.push(new CsgoGamemode("Classic Casual", "0", "0"));
		this.csgoGameModes.push(new CsgoGamemode("Classic Competitive", "0", "1"));
		this.csgoGameModes.push(new CsgoGamemode("Custom", "3", "0"));
		this.csgoGameModes.push(new CsgoGamemode("Deathmatch", "1", "2"));
		this.csgoGameModes.push(new CsgoGamemode("Demolition", "1", "1"));
		this.csgoGameModes.push(new CsgoGamemode("Wingman", "0", "2"));
		this.csgoGameModes.push(new CsgoGamemode("Danger Zone", "6", "0"));
	}

	async serverInit(): Promise<void> {
		logger.log('CSGO.get free GSLT');
		let gslt = await getGSLT();
		logger.log("got gslt" + gslt.gslt)
		logger.log('CSGO.setup');
		await this.gameStatus.setStatus(GameStatuses.GameInitialize);
		await this.waitServerFolderCreation();
		await this.saveCSGOCommonConfig(gslt);
		await this.saveCSGOServerConfig();
		logger.log('done');
	}

	async saveCSGOCommonConfig(gslt: Gslt) {
		const maxplayers = ensure(runTimeConfig.gameSettings.find(value => value.name == "max-players")).value;
		const gamemodeSetting = ensure(runTimeConfig.gameSettings.find(value => value.name == "game-mode"));
		const gamemode: CsgoGamemode = ensure(this.csgoGameModes.find(value => value.name==gamemodeSetting.value));

		const settings: Map<String, String> = new Map();
		settings.set("gametype", gamemode.gametype);
		settings.set("gamemode", gamemode.gamemode);
		settings.set("maxplayers", maxplayers);
		settings.set("gslt", gslt.gslt);
		settings.set("ip", ip.address());
		settings.set("tickrate", this.tickrate);
		const configSaver: ConfigSaver = new ConfigSaver(settings, '{key}="{value}"');

		await configSaver.saveToServerFiles(`/home/client/sm/csgo/lgsm/config-lgsm/csgoserver/common.cfg`);
	}

	async saveCSGOServerConfig() {
		const rconPassword = ensure(runTimeConfig.gameSettings.find(value => value.name == "rcon-password")).value;
		const serverPassword = ensure(runTimeConfig.gameSettings.find(value => value.name == "server-password")).value;

		const settings: Map<String, String> = new Map();
		settings.set("rcon_password", rconPassword);
		settings.set("hostname", this.hostname);
		settings.set("sv_password", serverPassword);
		settings.set("host_players_show", this.hostPlayersShow);
		const configSaver: ConfigSaver = new ConfigSaver(settings, '{key} "{value}"');

		await configSaver.saveToServerFiles(`/home/client/sm/csgo/serverfiles/csgo/cfg/csgoserver.cfg`);
	}
}

class CsgoGamemode {
	public name: String;
	public gametype: String;
	public gamemode: String;

	constructor(name: String, gametype: String, gamemode: String) {
		this.name = name;
		this.gametype = gametype;
		this.gamemode = gamemode;
	}
}
