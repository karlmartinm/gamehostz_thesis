import {BaseGame} from "./BaseGame";
import {GameInterface} from "./GameInterface";
import graylog from "../functions/graylog";
import {GameStatus} from "../classes/GameStatus";
import {GameStatuses} from "../entities/GameStatuses.enum";
import * as ip from "ip";
import {ConfigSaver} from "./game-settings/ConfigSaver";
import {UEIniProperties} from "./game-settings/unreal/UEIniProperties";
import {ensure} from "../util/util";
import {runTimeConfig} from "../config/runTimeConfig";

const logger = graylog();

export default class InsurgencySandstorm extends BaseGame implements GameInterface {

	private ueIniProperties: UEIniProperties;

	constructor(private gameStatus: GameStatus) {
		super();
		this.ueIniProperties = new UEIniProperties();
	}

	async serverInit(): Promise<void> {
		logger.log('IS.setup');
		await this.gameStatus.setStatus(GameStatuses.GameInitialize);
		try {
			await this.writeInitialGameSettings();
		} catch (e) {
			logger.error(e.message);
		}

		logger.log('done');
	}
	private async writeInitialGameSettings() {
		await this.waitServerFolderCreation();
		await this.ueIniProperties.save();
		await this.saveISCommonConfig();
	}

	async saveISCommonConfig() {
		const port: string = "27102";
		const queryPort: string = "27131";
		const mapNamesMap: any = {
			"Crossing": "Canyon",
			"Farmhouse": "Farmhouse",
			"Hideout": "Town",
			"Hillside": "Sinjar",
			"Ministry": "Ministry",
			"Outskirts": "Compound",
			"Precinct": "Precinct",
			"Refinery": "Oilfield",
			"Summit": "Mountain",
			"PowerPlant": "PowerPlant",
			"Tideway": "Buhriz",
		}
		const serverPassword: string = ensure(runTimeConfig.gameSettings.find(value => value.name == "password")).value;
		const serverPasswordStr: string = serverPassword && serverPassword !== "" ? `?password=${serverPassword}` : '';
		const scenario: string = ensure(runTimeConfig.gameSettings.find(value => value.name == "scenario")).value;
		const map: string = mapNamesMap[scenario.split('_')[1]];

		// Since currently the default LinuxGSM launch parameters are overwritten, it is not really needed to set these properties
		// separately in the cfg file. This is just so when running the "details" command for the server, the "correct" values are displayed
		const settings: Map<String, String> = new Map();
		settings.set("ip", ip.address());
		settings.set("port", port);
		settings.set("queryport", queryPort);
		if (serverPasswordStr !== '') settings.set("serverpassword", serverPassword);
		settings.set("defaultmap", map);
		settings.set("defaultscenario", scenario);
		const configSaver: ConfigSaver = new ConfigSaver(settings, '{key}="{value}"');

		// By default LinuxGSM puts ports as travel parameters ('?').
		// Official Sandstorm dedi server guide puts them as cmd line arguments ('-')
		const startParamsSettings: Map<String, String> = new Map();
		startParamsSettings.set("parms", `${map}?Scenario=${scenario}${serverPasswordStr} -Port=${port} -QueryPort=${queryPort} -log`)
		const configSaverForFn: ConfigSaver = new ConfigSaver(startParamsSettings, 'fn_parms(){\n{key}="{value}"\n}');

		await configSaver.saveToServerFiles(`/home/client/sm/insurgencysandstorm/lgsm/config-lgsm/inssserver/common.cfg`);
		await configSaverForFn.saveToServerFiles(`/home/client/sm/insurgencysandstorm/lgsm/config-lgsm/inssserver/common.cfg`);
	}
}
