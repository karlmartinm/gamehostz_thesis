import * as fs from "fs";
import config from '../config/config';
import {runTimeConfig} from '../config/runTimeConfig';
import graylog from '../functions/graylog';
import {sleep} from '../util/util';
const logger = graylog();

export class BaseGame {
	private WAIT_FOR_SERVER_FOLDER_CREATION_SEC = 2;
	protected CONFIG_PATH: string = `${config.app.basePath}/${runTimeConfig.gameConfig.folder}/serverfiles`;

	protected async waitServerFolderCreation() {
		let tries = 10;
		while (tries > 0) {
			if (!this.isGameFolderExisting()) {
				logger.log(`no serverFiles folder, waiting ${this.WAIT_FOR_SERVER_FOLDER_CREATION_SEC} sec for serverFiles folder creation`);
				await sleep(this.WAIT_FOR_SERVER_FOLDER_CREATION_SEC);
				tries--;
			} else {
				return;
			}
		}
		throw new Error(`waited ${this.WAIT_FOR_SERVER_FOLDER_CREATION_SEC * tries} seconds but serverFiles folder was not created`);
	}

	private isGameFolderExisting(): boolean {
		return fs.existsSync(this.CONFIG_PATH);
	}
}
