import * as fs from "fs";

export class ConfigSaver {
	constructor(private settings: Map<String, String>,
				private settingsFormat: string) {
	}

	constructSettings() {
		let output: String = ""
		this.settings.forEach((value, key) =>
			output += `${this.settingsFormat
				.replace("{key}", key+"")
				.replace("{value}", value+"")}\n`
		)
		return output
	}

	saveToServerFiles(path: string) {
		if (path.length != 0) {
			let data = this.constructSettings();
			fs.writeFileSync(path, data, 'utf-8');
		}
	}
}
