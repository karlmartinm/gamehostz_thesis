import {runTimeConfig} from "../../../config/runTimeConfig";
import fs from "fs-extra";
import config from "../../../config/config";
import ini from 'ini';

export class UEIniProperties {

	private readonly ueSettings: any;

	constructor() {
		this.ueSettings = this.parseGameSettings();
	}

	private parseGameSettings() {
		const settingsObj: any = {};
		runTimeConfig.gameSettings.forEach(gameSetting => {
			if (gameSetting.format !== 'ue_ini_format' || !gameSetting.file) return;
			// If no filepath key in settingsObj, create an object
			if (!settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`]) {
				settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`] = {};
			}
			// Loop through locations of a gameSetting, because one setting can be in multiple sections
			gameSetting.location.forEach(gameSettingLocation => {
				// If no section in file obj, create an object
				if (!settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]]) {
					settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]] = {};
				}
				// If no section class in section obj, create an object
				if (!settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]][gameSettingLocation[1]]) {
					settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]][gameSettingLocation[1]] = {};
				}
				// If no variable in section class obj, assign a value for a variable
				if (!settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]][gameSettingLocation[1]][gameSettingLocation[2]]) {
					let value: any = gameSetting.value;
					if (gameSetting.value === 'true' || gameSetting.value === 'false') {
						value = gameSetting.value.charAt(0).toUpperCase() + gameSetting.value.slice(1);
					}/* else if (isNaN(Number(gameSetting.value))) {
						value = `"${gameSetting.value}"`;
					}*/
					settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]][gameSettingLocation[1]][gameSettingLocation[2]] = value;
				} else if (Array.isArray(settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]][gameSettingLocation[1]][gameSettingLocation[2]])) {
					settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]][gameSettingLocation[1]][gameSettingLocation[2]].push(gameSetting.value);
				} else {
					settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]][gameSettingLocation[1]][gameSettingLocation[2]] = [settingsObj[`${config.app.basePath}/${runTimeConfig.gameConfig.folder}${gameSetting.directory}${gameSetting.file}`][gameSettingLocation[0]][gameSettingLocation[1]][gameSettingLocation[2]]];
				}
			})
		});
		return settingsObj;
	}

	save() {
		Object.keys(this.ueSettings).forEach(filePath => {
			const data = ini.encode(this.ueSettings[filePath]);
			fs.outputFileSync(filePath, data, 'utf-8');
		});
	}
}
