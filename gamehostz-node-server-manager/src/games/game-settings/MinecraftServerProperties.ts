import * as ip from 'ip';

import { runTimeConfig } from '../../config/runTimeConfig';
import * as fs from "fs";
import config from '../../config/config';
import { Settings } from '../../entities/GameSettings.interface';

interface MinecraftSettings {
	[name: string]: string;
}

export class MinecraftServerProperties {

	private readonly filePath: string;
	private readonly ip: string;
	private readonly port: string;
	private readonly enableQuery: string;
	private readonly queryPort: string;

	constructor() {
		this.filePath = `${config.app.basePath}/${runTimeConfig.gameConfig.folder}/serverfiles/server.properties`;
		this.ip = ip.address();
		this.port = runTimeConfig.gameConfig.default_port;
		this.enableQuery = 'true';
		this.queryPort = runTimeConfig.gameConfig.query_port;
	}

	setInitialSettings() {
		const initialSettings = { ...this.convertSettings(runTimeConfig.gameSettings), ...this.getRequiredSettings() };
		this.writeSettingsToFile(initialSettings);
	}

	updateSettings() {
		console.log('save game updateSettings');
		const currentSettings = this.readFile();
		const newSettings = {
			...currentSettings,
			// ...this.convertSettings(runTimeConfig.gameSettings),
			...this.getRequiredSettings()
		};
		console.log('newSettings', JSON.stringify(newSettings));
		this.writeSettingsToFile(newSettings);
	}

	private getRequiredSettings() {
		return {
			'server-ip': this.ip,
			'server-port': this.port,
			'enable-query': this.enableQuery,
			'query.port': this.queryPort
		}
	}

	private toFileFormat(list: MinecraftSettings) {
		let output = '';
		for (let key of Object.keys(list)) {
			output += `${key}=${list[key]}\n`;
		}
		return output;
	}

	private convertSettings(toConvert: Settings[]): MinecraftSettings {
		const result = {} as MinecraftSettings;
		for (let setting of toConvert) {
			result[setting.name] = setting.value;
		}
		return result;
	}

	private writeSettingsToFile(settings: MinecraftSettings) {
		const toFile = this.toFileFormat(settings);
		fs.writeFileSync(this.filePath, toFile, 'utf-8');
	}

	private readFile(): MinecraftSettings {
		if (fs.existsSync(this.filePath)) {
			const file = fs.readFileSync(this.filePath, 'utf-8');
			const lineList = file.split('\n');
			const obj = {} as MinecraftSettings;
			let key: string, value: string;

			for (let line of lineList) {
				if (line.length === 0 || line.charAt(0) === '#') continue;
				[key, value] = line.split('=');
				obj[key] = value;
			}
			return obj;
		}
		return {};
	}
}
