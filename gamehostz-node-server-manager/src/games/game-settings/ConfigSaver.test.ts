import {GameDig} from '../../classes/GameDig';
import {GameStatus} from '../../classes/GameStatus';
import {runTimeConfig} from '../../config/runTimeConfig';
import {Gslt} from '../../entities/GameSettings.interface';
import Csgo from '../Csgo';
import {ConfigSaver} from './ConfigSaver';
import * as fs from "fs";
import * as ip from 'ip';

jest.mock('fs');
jest.mock('../../functions/graylog');
jest.mock('ip', () => ({
	...jest.requireActual('ip'),
	address: jest.fn().mockReturnValue("0.0.0.0")
}));


describe('ConfigSaver when given different game configs and formats', () => {

	test('when making empty settings then should return nothing', () => {
		const settings: Map<String, String> = new Map();
		const settingsFormat = "{key}:{value}";
		const configSaver = new ConfigSaver(settings, settingsFormat);

		expect(configSaver.constructSettings()).toBe("");
	});

	test('when making with one entry with format, should return something', () => {
		const settings: Map<String, String> = new Map();
		settings.set("some", "thing");
		const settingsFormat = "{key}:{value}";
		const configSaver = new ConfigSaver(settings, settingsFormat);

		expect(configSaver.constructSettings()).toBe("some:thing\n");
	});

	test('when making with multiple entries with different format, should return something', () => {
		const settings: Map<String, String> = new Map();
		settings.set("some", "thing");
		settings.set("other", "thing");
		const settingsFormat = '{key}="{value}"';
		const configSaver = new ConfigSaver(settings, settingsFormat);

		expect(configSaver.constructSettings()).toBe('some="thing"\nother="thing"\n');
	});

});

describe('Given Path to save then should save a file', () => {

	test('when given path, then should save', () => {
		const settings: Map<String, String> = new Map();
		const settingsFormat = "{key}:{value}";
		const configSaver = new ConfigSaver(settings, settingsFormat);

		configSaver.saveToServerFiles("common.cfg");

		expect(fs.writeFileSync).toHaveBeenCalledWith("common.cfg", "", "utf-8");
	});

});

describe('CSGO data lets see what happens', () => {
	let storage;
	let gameStatus: GameStatus;
	let csgo: Csgo;
	let expectedUrl: String;
	let gslt: Gslt = { gslt: "0CB8A1DC722FE319F8533A6A943767EC"};

	beforeEach(() => {
		storage = require('node-persist');
		gameStatus = new GameStatus(storage);
		csgo = new Csgo(gameStatus);
		expectedUrl = "/home/client/sm/csgo/lgsm/config-lgsm/csgoserver/common.cfg";
	});

	test('when trying to save config', () => {
		runTimeConfig.gameSettings = [{"name": "game-mode", "value": "Classic Competitive"},
			{"name": "max-players", "value": "12"},
			{"name": "rcon-password", "value": "default"},
			{"name": "server-password", "value": ""}];
		const expected = 'gametype="0"\ngamemode="1"\nmaxplayers="12"\ngslt="0CB8A1DC722FE319F8533A6A943767EC"\nip="0.0.0.0"\ntickrate="128"\n';

		csgo.saveCSGOCommonConfig(gslt);

		expect(fs.writeFileSync).toHaveBeenCalledWith(expectedUrl, expected, "utf-8");
	});

	test('when trying to save config with some other gamemode', () => {
		runTimeConfig.gameSettings = [{"name": "game-mode", "value": "Deathmatch"},
			{"name": "max-players", "value": "12"},
			{"name": "rcon-password", "value": "default"},
			{"name": "server-password", "value": ""}];
		const expected = 'gametype="1"\ngamemode="2"\nmaxplayers="12"\ngslt="0CB8A1DC722FE319F8533A6A943767EC"\nip="0.0.0.0"\ntickrate="128"\n';

		csgo.saveCSGOCommonConfig(gslt);

		expect(fs.writeFileSync).toHaveBeenCalledWith(expectedUrl, expected, "utf-8");
	});

	test('when trying to save cs server config', () => {
		runTimeConfig.gameSettings = [{"name": "game-mode", "value": "Deathmatch"},
			{"name": "max-players", "value": "12"},
			{"name": "rcon-password", "value": "default"},
			{"name": "server-password", "value": "password"}];
		const expected = 'rcon_password "default"\nhostname "free GamehostZ.com CSGO server"\nsv_password "password"\nhost_players_show "2"\n';
		expectedUrl = "/home/client/sm/csgo/serverfiles/csgo/cfg/csgoserver.cfg";

		csgo.saveCSGOServerConfig();

		expect(fs.writeFileSync).toHaveBeenCalledWith(expectedUrl, expected, "utf-8");
	});

	test('when trying to save cs server config', () => {
		runTimeConfig.gameSettings = [{"name": "game-mode", "value": "Deathmatch"},
			{"name": "max-players", "value": "12"},
			{"name": "rcon-password", "value": "123"},
			{"name": "server-password", "value": ""}];
		const expected = 'rcon_password "123"\nhostname "free GamehostZ.com CSGO server"\nsv_password ""\nhost_players_show "2"\n';
		expectedUrl = "/home/client/sm/csgo/serverfiles/csgo/cfg/csgoserver.cfg";

		csgo.saveCSGOServerConfig();

		expect(fs.writeFileSync).toHaveBeenCalledWith(expectedUrl, expected, "utf-8");
	});

	//TODO maybe needed redundancy tests

	// test('what happens if data is missing common cfg',   () => {
	// 	runTimeConfig.gameSettings = []
	// 	const expected = 'gametype="0"\ngamemode="0"\nmaxplayers="12"\ngslt="0CB8A1DC722FE319F8533A6A943767EC"\nip="0.0.0.0"\ntickrate="128"\n'
	//
	// 	csgo.saveCSGOCommonConfig()
	//
	// 	expect(fs.writeFileSync).toHaveBeenCalledWith(expectedUrl, expected, "utf-8")
	// });
	//
	// test('what happens if data is missing server cfg',   () => {
	// 	runTimeConfig.gameSettings = []
	// 	const expected = 'rcon_password "default"\nhostname "free GamehostZ.com CSGO server"\nsv_password ""\nhost_players_show "2"\n'
	// 	expectedUrl = "/home/client/sm/csgo/serverfiles/csgo/cfg/csgoserver.cfg";
	//
	// 	csgo.saveCSGOCommonConfig()
	//
	// 	expect(fs.writeFileSync).toHaveBeenCalledWith(expectedUrl, expected, "utf-8")
	// });

});


