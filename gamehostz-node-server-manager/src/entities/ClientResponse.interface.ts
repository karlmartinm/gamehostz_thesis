import { QueryResult } from 'gamedig';

export enum SOCKET_EVENT {
	DATA = 'DATA'
}

export enum CLIENT_RESPONSE_EVENT {
	CONSOLE_EVENT = 'CONSOLE_EVENT',
	STATUS_EVENT = 'STATUS_EVENT',
	MESSAGE_EVENT = 'MESSAGE_EVENT'
}

export enum MESSAGE_EVENT_TYPE {
	SHUTDOWN_TIMER = 'SHUTDOWN_TIMER',
	SHUTDOWN = 'SHUTDOWN'
}

export interface MessageStatusEvent {
	type: MESSAGE_EVENT_TYPE;
	message?: string;
	value?: string | number | Date;
}

export interface ClientResponse {
	type: CLIENT_RESPONSE_EVENT;
	data: string | QueryResult | MessageStatusEvent;
}
