export interface GameSettings {
	serverId: string;
	gameConfig: SettingsList;
	gameName: string;
	serverEmptyCloseTime: number;
}

export interface SettingsList {
	settings: Array<Settings>;
}

export interface Settings {
	name: string;
	value: string;
	format: string;
	file: string;
	directory: string;
	location: string[][];
}

export interface Gslt {
	gslt: string;
}
