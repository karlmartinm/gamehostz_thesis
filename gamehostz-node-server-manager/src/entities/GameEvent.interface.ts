export enum GameEventType {
	USER_COMMAND = 'USER_COMMAND',
	SERVER_EVENT = 'SERVER_EVENT'
}

export enum GameEventSource {
	SM = 'SM'
}

export enum ServerEventType {
	SHUTDOWN_USER = 'SHUTDOWN_USER',
	SHUTDOWN_EMPTY = 'SHUTDOWN_EMPTY',
	SHUTDOWN_NEVER_STARTED = 'SHUTDOWN_NEVER_STARTED',

	CLIENT_CONNECT = 'CLIENT_CONNECT',
	CLIENT_DISCONNECT = 'CLIENT_DISCONNECT',
	SERVER_EMPTY = 'SERVER_EMPTY',
	PLAYER_COUNT = 'PLAYER_COUNT'
}

export type GameEventJsonValueType = string | number | boolean;

export interface GameEventJson {
	value?: GameEventJsonValueType,
}

export interface GameEventUserCommand extends GameEventJson {
}

export interface GameEventServerEvent extends GameEventJson {
	type: ServerEventType
}

export interface GameEvent {
	serverId: string,
	userId: string,
	source: GameEventSource,
	type: GameEventType
	additionalJson: GameEventUserCommand | GameEventServerEvent
}
