export interface Commands {
	[name: string]: Command;
}

export interface Command {
	cmd: string;
	cwd?: string;
}
