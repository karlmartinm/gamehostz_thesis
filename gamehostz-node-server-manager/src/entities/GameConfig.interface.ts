export interface GameConfig {
	name: string;
	class_file: string;
	folder: string;
	lgsm_name: string;
	gamedig_name: string;
	default_port: string;
	query_port: string;
	close_when_empty_for_sec: number;
	empty_server_check_interval_sec: number;
	max_wait_for_server_initial_start_sec: number;
}
