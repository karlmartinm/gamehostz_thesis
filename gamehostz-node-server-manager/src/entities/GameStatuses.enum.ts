export enum GameStatuses {
	Init,
	DownloadingLgsm,
	DownloadingGame,
	GameInitialize,
	StartingGame,
	RestartingGame,
	GameRunning,
	GameStopped,
	GameError,
	VPSKill
}
