import { GameStatuses } from '../entities/GameStatuses.enum';

export const isOkayToResumeFromCrash = (status: GameStatuses): boolean => {
	return status === GameStatuses.StartingGame ||
		status === GameStatuses.RestartingGame ||
		status === GameStatuses.GameRunning ||
		status === GameStatuses.GameStopped;
};

export const sleep = (sec: number) => {
	const sleepTime = Math.ceil(sec * 1000);
	return new Promise(resolve => setTimeout(resolve, sleepTime));
};

export const ensure = <T>(argument: T | undefined | null, message: string = 'This value was promised to be there.'): T => {
	if (argument === undefined || argument === null) {
		throw new TypeError(message);
	}
	return argument;
};


export const parseBoolean = (boolVal: any) => {
	if (typeof boolVal === 'boolean') {
		return boolVal;
	}
	if (typeof boolVal === 'string') {
		return boolVal === 'true' || boolVal === '1';
	}
	if (typeof boolVal === 'number') {
		return boolVal === 1;
	}
	return false;
};
