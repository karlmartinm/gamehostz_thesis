import { GameStatuses } from './entities/GameStatuses.enum';
import * as bodyParser from 'body-parser';
import express, { Application, Request, Response } from 'express';
import * as fs from 'fs';
import socketIO from 'socket.io';
import { GameStatus } from './classes/GameStatus';
import cors from 'cors';
import { GameManager } from './classes/GameManager';
import config from './config/config';
import { GAME_STATUS_FILE } from './constants/constants';
import { GameConfig } from './entities/GameConfig.interface';
import { isOkayToResumeFromCrash } from './util/util';
import { runTimeConfig } from './config/runTimeConfig';
import { shutdownSignal } from './functions/shutdown';
import graylog from './functions/graylog';
import { getGameSettings } from './functions/callouts';

// TODO actually replace this
const storage = require('node-persist'); // TODO import es6 way - not sure why "import * as storage from 'node-persist'" does not work

const storagePath = config.app.basePath + config.app.storagePath;
const logger = graylog();
let io: socketIO.Server;
let gameSetup: GameManager;
let gameStatus: GameStatus;


const initStorage = async () => {
	if (!fs.existsSync(storagePath)) {
		fs.mkdirSync(storagePath);
	}
	await storage.init({
		dir: storagePath
	});
	await Promise.all([
		new Promise(async (resolve) => {
			storage.keys().then((keys: any) => {
				if (keys.includes(GAME_STATUS_FILE) && !config.app.prod) {
					storage.getItem(GAME_STATUS_FILE).then((gs: GameStatuses) => {
						gameStatus = new GameStatus(storage);
						if (!isOkayToResumeFromCrash(gs)) {
							throw new Error('Nodejs restart before game could install and run');
						}
						// TODO should die when failed on setup and not while game already runs?
						gameStatus.setStatus(gs).then(() => {
							return resolve();
						});
					});
				} else {
					gameStatus = new GameStatus(storage);
					return resolve();
				}
			});
		})
	]);
};

const fetchGameSettings = async () => {
	const res = await getGameSettings();

	if (!config.app.supportsSave) {
		runTimeConfig.gameSettings = res.gameConfig.settings;
		if (!runTimeConfig.gameSettings) {
			throw new Error('no gameSettings from api/storage');
		}
	}

	runTimeConfig.gameConfig = config.supported_games.find((x) => x.name === res.gameName) as GameConfig;
	if (!runTimeConfig.gameConfig) {
		throw new Error('no gameConfig from api/storage');
	}
	logger.log('runTimeConfig.gameSettings', runTimeConfig.gameSettings);
	logger.log('runTimeConfig.gameConfig', runTimeConfig.gameConfig);
};

const startWebServices = async () => {
	return new Promise(resolve => {
		const app: Application = express();
		const jsonParser = bodyParser.json();
		const server = app.listen(config.app.port, () => {
			logger.log(`Express running at: ${config.app.port}`);
			resolve();
		});
		app.use(cors());

		io = socketIO(server);

		app.get('/ping', (req: Request, res: Response) => {
			return res.status(200).send('pong');
		});

		// used for internal/admin actions
		app.put('/control/{action}', jsonParser, (req: Request, res: Response) => {
			const requestIp = req.connection.remoteAddress;
			if (requestIp !== config.java.host) return res.status(401).send();
			// TODO parse, validate action
		});
	});
};

const startGameManager = async () => {
	gameSetup = new GameManager(gameStatus, storage, io);
};

(async () => {
	try {
		await initStorage();
		await fetchGameSettings();
		await startWebServices();

		// noinspection ES6MissingAwait
		startGameManager(); // async handoff
	} catch (e) {
		logger.error(e);
	}
})();

process.on('SIGINT', shutdownSignal);
process.on('SIGTERM', shutdownSignal);
process.on('SIGHUP', shutdownSignal);
