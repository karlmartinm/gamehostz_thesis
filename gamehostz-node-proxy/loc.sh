#!/bin/bash

npm run build
docker stop gh-np
docker rm gh-np
docker build -t gh-np .
docker run --name gh-np -a stdin -a stdout -e "APP_ENV=prod" gh-np
