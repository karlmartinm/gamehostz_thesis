const server = require('http').createServer();
const socketIo = require('socket.io');
const port = 3002;

const io = socketIo(server);

io.on('connection', (client) => {
	console.log('io.on connection');

	client.on('proxy_server', (data) => {
		console.log('proxy_server', data);
		setTimeout(() => {
			client.emit('server_proxy', { data: 'response from server2' });
		}, 2000);
	});
	client.on('disconnect', () => {
		console.log('disconnect');
	});
});
console.log(`server2 started on port ${port}`);
server.listen(port);
