const server = require('http').createServer();
const socketIo = require('socket.io');
const port = 3001;

const io = socketIo(server);

io.on('connection', (client) => {
	console.log('io.on connection');

	client.on('proxy_server', (data) => {
		console.log('proxy_server', data);
		setTimeout(() => {
			client.emit('server_proxy', { data: 'response from server1' });
		}, 2000);
	});
	client.on('disconnect', () => {
		console.log('disconnect');
	});
});
console.log(`server1 started on port ${port}`);
server.listen(port);
