const server = require('http').createServer();
const socketIo = require('socket.io');
const socketIoClient = require('socket.io-client');
const port = 3000;

const io = socketIo(server);

io.on('connection', (client) => {
	const userId = client.handshake.query.userId;
	console.log(`io.on connection. userId = ${userId}`);

	const clientConnection = socketIoClient.connect(`http://localhost:${userId}`);
	clientConnection.on('connect', () => {
		console.log('connected to destination');
	});
	clientConnection.on('server_proxy', (data) => {
		console.log('server_proxy', data);
		client.emit('proxy_client', data);
	});
	clientConnection.on('disconnect', () => {
		console.log('disconnect');
	});

	client.on('client_proxy', (data) => {
		console.log('client_proxy', data);
		if (clientConnection.connected) {
			clientConnection.emit('proxy_server', data);
		} else {
			console.log('no connection to server');
		}
	});
	client.on('disconnect', () => {
		console.log('disconnect');
	});
});
console.log(`proxy started on port ${port}`);
server.listen(port);
