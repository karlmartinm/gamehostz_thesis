#!/bin/bash

apt-get upgrade && apt-get update
apt-get install openssh-client -y
apt-get install sshpass -y

sshpass -p $4 ssh -o StrictHostKeyChecking=no $1@$2 docker login -u Alvar195 -p $3 docker.pkg.github.com
sshpass -p $4 ssh -o StrictHostKeyChecking=no $1@$2 docker pull docker.pkg.github.com/alvar195/gamehostz-node-proxy/gh-np:latest
sshpass -p $4 ssh -o StrictHostKeyChecking=no $1@$2 "docker container stop gh-np && docker container rm gh-np || true"
sshpass -p $4 ssh -o StrictHostKeyChecking=no $1@$2 docker run -e "APP_ENV=prod" -d --restart unless-stopped --network host --name gh-np -p 8066:8066 -d docker.pkg.github.com/alvar195/gamehostz-node-proxy/gh-np:latest
