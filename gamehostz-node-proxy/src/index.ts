import { listen, Socket } from "socket.io";
import { createServer } from 'http';
import * as socketIoClient from 'socket.io-client';

import config from './config/config';
import { validateSMAccess } from './functions/callouts';
import { GameInfo } from './entities/GameInfo.interface';

const server = createServer(function (req, res) {
	res.writeHead(200, { 'Content-Type': 'text/plain' });
	res.end('node-proxy');
});

const io = listen(server);

io.on('connection', async (client: Socket) => {
	console.log('io.on connection');
	const { serverId, userId } = client.handshake.query;
	console.log('serverId', serverId, 'userId', userId);

	try {
		const response = await validateSMAccess(serverId, userId);
		console.log('validateSMAccess response', response.body);
		if (response.statusCode !== 200) {
			console.log('statuscode not 200');
			invalidConnection(client);
		}
		validateSuccess(client, response.body);
	} catch (e) {
		console.log('error', e.message);
		invalidConnection(client);
	}


});

const validateSuccess = (client: Socket, response: GameInfo) => {
	console.log('validateSuccess');
	let SMConnection = socketIoClient.connect(`http://${response.address}:${config.app.sm_port}`);
	// TODO not necessary to listen (server startup takes time)
	SMConnection.on('connect_error', function () {
		console.log('Connection Failed');
	});
	SMConnection.on('connect', () => {
		console.log('connected to destination');
	});
	SMConnection.on('DATA', (data: any) => {
		console.log('DATA', data);
		client.emit('DATA', data);
	});
	SMConnection.on('disconnect', () => {
		console.log('disconnect');
	});

	client.on('action', (data: any) => {
		console.log('action', data);
		SMConnection.emit('action', data);
	});
	client.on('disconnect', function () {
		clientDisconnect(SMConnection);
	});

};

const invalidConnection = (client: Socket) => {
	client.emit('INVALID_CONNECTION');
	client.disconnect(true);
};

const clientDisconnect = (SMConnection: SocketIOClient.Socket) => {
	let socket = SMConnection.disconnect();
	socket.disconnect();
	SMConnection = null as any;
	socket = null as any;
	console.log('client disconnect');
};

server.listen(config.app.port);
console.log(`node-proxy running on port ${config.app.port}`);
