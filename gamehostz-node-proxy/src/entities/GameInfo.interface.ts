export interface GameInfo {
	id: string;
	externalId: string;
	address: string;
	gameAbbreviation: string;
	gameConfig: object;
}
