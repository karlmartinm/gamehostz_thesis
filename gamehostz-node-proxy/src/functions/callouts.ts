import { get } from 'request-promise';

import config from '../config/config';

export async function validateSMAccess(serverId: string, userId: string) {
	console.log('request to ', `http://${config.java.host}:${config.java.port}/api/internal/gameservers/${serverId}/${userId}`);
	return get(`http://${config.java.host}:${config.java.port}/api/internal/gameservers/${serverId}/${userId}`, {
		json: true,
		resolveWithFullResponse: true
	});
}
