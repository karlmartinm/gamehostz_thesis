import { join } from 'path';

console.log(process.env.APP_ENV)
const prod = process.env.APP_ENV === 'prod';

require('dotenv').config({ path: join(__dirname, prod ? '../.env' : '../../.env.development') });
console.log(process.env.JAVA_HOST)

export default {
	app: {
		port: process.env.APP_PORT || 8066,
		prod,
		sm_port: process.env.APP_SM_PORT
	},
	java: {
		host: process.env.JAVA_HOST,
		port: process.env.JAVA_PORT || 80
	},
	graylog: {
		host: process.env.GL_HOST,
		port: process.env.GL_PORT,
		hostname: process.env.GL_HOSTNAME
	}
}
