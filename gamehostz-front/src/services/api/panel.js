import axios from 'axios';

export default {
  getPanel(id) {
    return axios.get(`/api/portal/gameservers/${id}`);
  },
};
