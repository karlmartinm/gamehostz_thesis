import axios from 'axios';

export default {
  getRunningServers() {
    return axios.get(`/api/portal/gameservers/running`);
  },
};
