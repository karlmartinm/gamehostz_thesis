import axios from 'axios';

export default {
  getSaveGames() {
    return axios.get(`/api/portal/gameservers/save-games`);
  },
  loadGame(id) {
    return axios.get(`/api/portal/gameservers/load/${id}`);
  },
  deleteSave(id) {
    return axios.delete(`/api/portal/gameservers/save/${id}`)
  }
};
