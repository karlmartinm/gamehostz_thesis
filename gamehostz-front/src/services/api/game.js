import axios from 'axios';

export default {
  startServer(gameName, region, config) {
    return axios.post(`/api/portal/gameservers/start/${region}/${gameName}`, config);
  },
  getGameConfigs() {
    return axios.get('/api/portal/gameconfigs', {
    });
  },
  getRegions() {
    return axios.get('/api/portal/gameconfigs/regions');
  },
  getServerPrice(gameName, playerCount) {
    return axios.get(`/api/portal/pricing/${gameName}/${playerCount}`, {
    });
  },
  getServerPriceBySlug(memorySlug) {
    return axios.get(`/api/portal/pricing/slug/${memorySlug}`, {
    });
  },
  stopServer(serverId) {
    return axios.delete(`/api/portal/gameservers/stop/${serverId}`);
  },
};
