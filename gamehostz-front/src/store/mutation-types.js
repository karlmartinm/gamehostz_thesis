// TODO Remove unused stuff
// app
export const SET_APP_VERSION = 'SET_APP_VERSION';
// locale
export const SET_LOCALE = 'SET_LOCALE';
// error
export const SHOW_ERROR = 'SHOW_ERROR';
export const ERROR = 'ERROR';
// success
export const SHOW_SUCCESS = 'SHOW_SUCCESS';
export const SUCCESS = 'SUCCESS';
// loading
export const SHOW_LOADING = 'SHOW_LOADING';
// auth
export const IS_LOGGED_IN = 'IS_LOGGED_IN';
export const SAVE_TOKEN = 'SAVE_TOKEN';
export const SAVE_USER = 'SAVE_USER';
export const LOGOUT = 'LOGOUT';
export const EMAIL_VERIFIED = 'EMAIL_VERIFIED';
export const RESET_EMAIL_SENT = 'RESET_EMAIL_SENT';
export const SHOW_CHANGE_PASSWORD_INPUTS = 'SHOW_CHANGE_PASSWORD_INPUTS';
export const SET_RESET_USERNAME = 'SET_RESET_USERNAME';
export const LOGIN = 'LOGIN';
// profile
export const FILL_PROFILE = 'FILL_PROFILE';
export const ADD_PROFILE_DATA = 'ADD_PROFILE_DATA';
// cities
export const FILL_ALL_CITIES = 'FILL_ALL_CITIES';
// Admin - Cities
export const CITIES = 'CITIES';
export const TOTAL_CITIES = 'TOTAL_CITIES';
// Admin - Users
export const USERS = 'USERS';
export const TOTAL_USERS = 'TOTAL_USERS';
// minecraft
export const MINECRAFT = 'MINECRAFT';
export const ADD_MC_DATA = 'ADD_MC_DATA';
export const FILL_MC_DATA = 'FILL_MC_DATA';
// panel
export const PANEL = 'PANEL';
export const FILL_PANEL_DATA = 'FILL_PANEL_DATA';
export const RESET_PANEL_DATA = 'RESET_PANEL_DATA';
// billing
export const BALANCE = 'BALANCE';
export const FILL_BALANCE_DATA = 'FILL_BALANCE_DATA';
export const RESET_BALANCE_DATA = 'RESET_BALANCE_DATA';
