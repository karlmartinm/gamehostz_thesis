import * as types from '@/store/mutation-types';
import api from '@/services/api/panel';
import { buildSuccess, handleError } from '@/utils/utils.js';
import router from '@/router';

const retryGetPanel = (commit, dispatch, serverId) => {
  return new Promise((resolve, reject) => {
    api.getPanel(serverId)
      .then((response) => {
        if (response.status === 200) {
          commit(types.FILL_PANEL_DATA, response.data);
          buildSuccess(null, commit, resolve);
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.errorKey === 'NO_IP_AVAILABLE') {
          setTimeout(() => {
            return retryGetPanel(commit, dispatch, serverId).then(resolve, reject);
          }, 10000);
        } else if (error.response && error.response.data.errorKey === 'NO_RUNNING_SERVER') {
          console.log('NO_RUNNING_SERVER redirected back to /games');
          router.push('/games');
        } else {
          handleError(error, commit, reject);
        }
      });
  });
};

const getters = {
  panel: (state) => state.panel,
};

const actions = {
  resetPanel({commit}) {
    commit(types.RESET_PANEL_DATA);
  },
  getPanel({commit, dispatch}, serverId) {
    return retryGetPanel(commit, dispatch, serverId);
  },
};

const mutations = {
  [types.FILL_PANEL_DATA](state, data) {
    state.panel.serverIPWithPort = `${data.serverAddress}:${data.serverPort}`;
    state.panel.serverIP = data.serverAddress;
    state.panel.serverId = data.serverId;
    state.panel.gameAbbreviation = data.gameAbbreviation;
    state.panel.serverStartedAt = data.serverStartedAt;
    state.panel.secondsToStart = data.secondsToStart;
    state.panel.serverPrice = data.serverPrice;
  },
  [types.RESET_PANEL_DATA](state) {
    state.panel.serverIPWithPort = '';
    state.panel.serverIP = '';
    state.panel.serverId = '';
    state.panel.gameAbbreviation = '';
    state.panel.serverStartedAt = '';
    state.panel.secondsToStart = '';
    state.panel.serverPrice = '';
  },
};

const state = {
  panel: {
    serverIPWithPort: '',
    serverIP: '',
    serverId: '',
    gameAbbreviation: '',
    serverStartedAt: '',
    secondsToStart: '',
    serverPrice: '',
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
