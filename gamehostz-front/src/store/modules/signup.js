import * as types from '@/store/mutation-types';
import router from '@/router';
import { buildSuccess, handleError } from '@/utils/utils.js';

// TODO Remove unneeded stuff and merge with auth file, or sth else?

const actions = {
  userSignUp({ commit, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true);
      dispatch('cognito/registerUser', payload, { root: true })
        .then((response) => {
          if (response.userConfirmed) {
            buildSuccess(null, commit, resolve,
              router.push({
                name: 'games',
              }),
            );
          } else {
            buildSuccess(null, commit, resolve,
              router.push({
                name: 'confirmUser',
              }),
            );
          }
        })
        .catch((error) => {
          handleError(error, commit, reject);
        });
    });
  },
};

export default {
  actions,
};
