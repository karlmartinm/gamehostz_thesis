import * as types from '@/store/mutation-types';
import api from '@/services/api/profile';
import {handleError} from '@/utils/utils.js';


const getters = {
    balance: (state) => state.balance,
};

const actions = {
    fetchBalance({commit}) {
        return new Promise((resolve, reject) => {
            api.getBalance()
                .then((response) => {
                    commit(types.FILL_BALANCE_DATA, response.data);
                })
                .catch((error) => {
                    handleError(error, commit, reject);
                });
        });
    },
};

const mutations = {
    [types.FILL_BALANCE_DATA](state, balance) {
        state.balance.amount = balance;
    },
};

const state = {
    balance: {
        amount: 0,
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
};
