import * as types from '@/store/mutation-types';
import router from '@/router';
import { buildSuccess, handleError } from '@/utils/utils.js';

// TODO Remove unneeded stuff and merge with auth file?

const getters = {
  showChangePasswordInputs: (state) => state.showChangePasswordInputs,
  resetUsername: (state) => state.resetUsername,
};

const actions = {
  setResetUsername({ commit }, payload) {
    commit(types.SET_RESET_USERNAME, payload);
  },
  resetPassword({ commit, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true);
      dispatch('cognito/changePassword', payload, { root: true })
        .then((response) => {
          // commit(types.SHOW_CHANGE_PASSWORD_INPUTS, false);
          commit(types.SET_RESET_USERNAME, null);
          buildSuccess(
            {
              msg: 'resetPassword.PASSWORD_CHANGED',
            },
            commit,
            resolve,
            router.push({
              name: 'login',
            }),
          );
        })
        .catch((error) => {
          // commit(types.SHOW_CHANGE_PASSWORD_INPUTS, false);
          handleError(error, commit, reject);
        });
    });
  },
};

const mutations = {
  [types.SHOW_CHANGE_PASSWORD_INPUTS](state, value) {
    state.showChangePasswordInputs = value;
  },
  [types.SET_RESET_USERNAME](state, value) {
    state.resetUsername = value;
  },
};

const state = {
  showChangePasswordInputs: true,
  resetUsername: null,
};

export default {
  state,
  getters,
  actions,
  mutations,
};
