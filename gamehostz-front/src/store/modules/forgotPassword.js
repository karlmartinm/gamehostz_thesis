import * as types from '@/store/mutation-types';
import router from '@/router';
import { buildSuccess, handleError } from '@/utils/utils.js';

// TODO Remove unneeded stuff and merge with auth file?

const getters = {
  resetEmailSent: (state) => state.resetEmailSent,
};

const actions = {
  forgotPassword({ commit, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true);
      dispatch('cognito/forgotPassword', payload, { root: true })
        .then((response) => {
          commit(types.RESET_EMAIL_SENT, true);
          commit(types.SET_RESET_USERNAME, payload.username);
          buildSuccess({
              msg: 'forgotPassword.RESET_EMAIL_SENT',
              params: [payload.username],
              timeout: 0,
            },
            commit,
            resolve,
            router.push({
              name: 'resetPassword',
            }),
          );
        })
        .catch((error) => {
          handleError(error, commit, reject);
        });
    });
  },
};

const mutations = {
  [types.RESET_EMAIL_SENT](state, value) {
    state.resetEmailSent = value;
  },
};

const state = {
  resetEmailSent: false,
};

export default {
  state,
  getters,
  actions,
  mutations,
};
