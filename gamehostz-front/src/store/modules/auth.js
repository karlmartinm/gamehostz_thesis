import * as types from '@/store/mutation-types';
import i18n from '@/plugins/i18n';
import router from '@/router';
import { buildSuccess, handleError } from '@/utils/utils.js';
import { addMinutes, format } from 'date-fns';

// TODO Work through this and remove unneeded stuff

const MINUTES_TO_CHECK_FOR_TOKEN_REFRESH = 1440;

const getters = {
  user: (state) => state.user,
  token: (state) => state.token,
  isTokenSet: (state) => state.isTokenSet,
  loginUsername: (state) => state.loginUsername,
  isUserLoggedIn: (state) => state.isUserLoggedIn,
};

const actions = {
  setIsUserLoggedIn({ commit }, payload) {
    commit(types.IS_LOGGED_IN, payload);
  },
  facebookLogin({ commit }, payload) {
    commit('setUser', payload)
    commit(types.LOGIN, payload.username);
    commit(types.SAVE_TOKEN, '');
    commit(types.SAVE_USER, payload);
    router.push({
      name: 'games',
    })
  },
   userLogin({ commit, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true);
      commit(types.LOGIN, payload.username);
      dispatch('cognito/signInUser', payload, { root: true })
        .then((response) => {
          // not DRY because I do not understand actions
          if (response.signInUserSession.accessToken.payload["cognito:groups"] != undefined &&
              response.signInUserSession.accessToken.payload["cognito:groups"].find(a => a == "Admin") != undefined) {
            response.role = "admin"
          }
          commit(types.SAVE_USER, response);

          //  window.localStorage.setItem('user', JSON.stringify(response.data.user));
          // window.localStorage.setItem('token', JSON.stringify(response.data.token));
          // window.localStorage.setItem(
          //   'tokenExpiration',
          //   JSON.stringify(format(addMinutes(new Date(), MINUTES_TO_CHECK_FOR_TOKEN_REFRESH), 'X')),
          // );
          // commit(types.SAVE_USER, response.data.user);
          commit(types.SAVE_TOKEN, '');
          // commit(types.EMAIL_VERIFIED, response.data.user.verified);
          buildSuccess(null, commit, resolve,
            router.push({
              name: 'games',
            }),
          );
        })
        .catch((error) => {
          handleError(error, commit, reject);
        });
    });
  },
  userVerification({ commit, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true);
      dispatch('cognito/confirmUser', payload, { root: true })
        .then((response) => {
            buildSuccess(null, commit, resolve,
              router.push({
                name: 'home',
              }),
            );
        })
        .catch((error) => {
          handleError(error, commit, reject);
        });
    });
  },
  userResendCode({ commit, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true);
      dispatch('cognito/resendConfirmation', payload, { root: true })
        .then((response) => {
          buildSuccess(i18n.t('confirmUser.RESEND_SUCCESS'), commit, resolve);
        })
        .catch((error) => {
          handleError(error, commit, reject);
        });
    });
  },
  /* refreshToken({ commit }) {
    return new Promise((resolve, reject) => {
      api
        .refreshToken()
        .then((response) => {
          if (response.status === 200) {
            window.localStorage.setItem('token', JSON.stringify(response.data.token));
            window.localStorage.setItem(
              'tokenExpiration',
              JSON.stringify(format(addMinutes(new Date(), MINUTES_TO_CHECK_FOR_TOKEN_REFRESH), 'X')),
            );
            commit(types.SAVE_TOKEN, response.data.token);
            resolve();
          }
        })
        .catch((error) => {
          handleError(error, commit, reject);
        });
    });
  }, */
  autoLogin({ commit }) {
    const user = JSON.parse(localStorage.getItem('user'));
    commit(types.SAVE_USER, user);
    commit(types.SAVE_TOKEN, JSON.parse(localStorage.getItem('token')));
    commit(types.SET_LOCALE, JSON.parse(localStorage.getItem('locale')));
    commit(types.EMAIL_VERIFIED, user.verified);
  },
  userLogout({ commit }) {
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('tokenExpiration');
    window.localStorage.removeItem('user');
    commit(types.LOGOUT);
    router.push({
      name: 'login',
    });
  }
};

const mutations = {
  [types.SAVE_TOKEN](state, token) {
    state.token = token;
    state.isTokenSet = true;
  },
  [types.LOGOUT](state) {
    state.user = null;
    state.token = null;
    state.isTokenSet = false;
  },
  [types.SAVE_USER](state, user) {
    state.user = user;
  },
  [types.LOGIN](state, loginUsername) {
    state.loginUsername = loginUsername;
  },
  [types.IS_LOGGED_IN](state, isUserLoggedIn) {
    state.isUserLoggedIn = isUserLoggedIn;
  },
};

const state = {
  user: null,
  token: JSON.parse(!!localStorage.getItem('token')) || null,
  isTokenSet: !!localStorage.getItem('token'),
  loginUsername: null,
  isUserLoggedIn: false,
};

export default {
  state,
  getters,
  actions,
  mutations,
};
