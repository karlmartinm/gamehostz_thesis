import Vue from 'vue';
import '@/plugins/axios';
import vuetify from './plugins/vuetify';
import '@/plugins/veevalidate';
import '@/plugins/common';
import '@/plugins/googleAnalytics';
import i18n from '@/plugins/i18n';
import App from '@/App.vue';
import router from '@/router';
import { store } from '@/store';
import VueClipboard from 'vue-clipboard2';
import VuetifyConfirm from 'vuetify-confirm';
import attachCognitoModule from '@vuetify/vuex-cognito-module';
import VueFbCustomerChat from 'vue-fb-customer-chat';
import VuetifyDialog from 'vuetify-dialog'
import 'vuetify-dialog/dist/vuetify-dialog.css'

Vue.use(VuetifyDialog, {
  context: {
    vuetify
  }
})

Vue.use(VueFbCustomerChat, {
  page_id: 110306837057814, //  change 'null' to your Facebook Page ID,
  theme_color: '#333333', // theme color in HEX
  locale: 'en_US', // default 'en_US'
});
Vue.use(VueClipboard);
Vue.use(VuetifyConfirm, {
  vuetify,
});

Vue.config.productionTip = false;

attachCognitoModule(store, {
  userPoolId: process.env.VUE_APP_USER_POOL_ID,
  identityPoolId: process.env.VUE_APP_IDENTITY_POOL_ID,
  userPoolWebClientId: process.env.VUE_APP_CLIENT_ID,
  region: process.env.VUE_APP_REGION,
}, 'cognito');

const app = new Vue({
  router,
  store,
  i18n,
  vuetify,
  created() {
    store.dispatch('setLocale', store.getters.locale);
    if (store.getters.isUserLoggedIn) {
      store.dispatch('autoLogin');
    }
  },
  render: (h) => h(App),
}).$mount('#app');

if (window.Cypress) {
  // Only available during E2E tests
  window.app = app;
}
