import Vue from 'vue';
import VeeValidate from 'vee-validate';
import en from 'vee-validate/dist/locale/en';

const veeValidateConfig = {
  locale: JSON.parse(localStorage.getItem('locale')) || 'en',
  dictionary: {
    en,
  },
};

Vue.use(VeeValidate, veeValidateConfig);
