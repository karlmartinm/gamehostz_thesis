import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors';
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/src/styles/main.sass';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: true,
    themes: {
      light: {
        primary: '#009688',
        secondary: '#00897B',
        accent: '#3b8a9b',
      },
      dark: {
        primary: colors.blue.lighten3,
        secondary: colors.shades.black,
        error: colors.red.darken1,
      },
    },
    notificationLocation: 'top-right'
  },
  icons: {
    iconfont: 'mdi',
  },
});
