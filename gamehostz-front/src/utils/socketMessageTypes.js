// enum SOCKET_DATA
export const DATA = 'DATA'; // TODO Yeet if not needed
// enum CLIENT_RESPONSE_EVENT
export const CONSOLE_EVENT = 'CONSOLE_EVENT';
export const STATUS_EVENT = 'STATUS_EVENT';
export const MESSAGE_EVENT = 'MESSAGE_EVENT';
// enum MESSAGE_EVENT_TYPE
export const SHUTDOWN_TIMER = 'SHUTDOWN_TIMER';
export const SHUTDOWN = 'SHUTDOWN';
