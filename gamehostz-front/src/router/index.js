import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';
import routes from '@/router/routes';
import { store } from '@/store';
import { checkIfTokenNeedsRefresh } from '@/utils/utils.js';
import { checkForUpdates } from '@/utils/updates.js';
import * as types from '@/store/mutation-types';

Vue.use(Router);
Vue.use(Meta);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [...routes],
});

// TODO Make this mess prettier
router.beforeEach((to, from, next) => {
  checkForUpdates();
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  store.dispatch('cognito/fetchSession').then((payload) => {
    const isLoggedIn = store.getters['cognito/isLoggedIn'];
    store.dispatch('setIsUserLoggedIn', isLoggedIn);
    const isUnauthorized = requiresAuth && !isLoggedIn;
    const hasNoUsernameForVerification = to.name === 'confirmUser' && !store.getters.loginUsername && !store.getters['cognito/username'];
    if (to.name === 'login' && store.getters['cognito/isLoggedIn']) {
      return next('/games');
    }
    if (isUnauthorized || hasNoUsernameForVerification) {
      return next('/login');
    }
    checkIfTokenNeedsRefresh();
    store.commit(types.SUCCESS, null);
    store.commit(types.ERROR, null);
    return next();
  }).catch(() => {
    store.dispatch('setIsUserLoggedIn', false);
    if (requiresAuth) {
      return next('/login');
    }
    return next();
  });
});

export default router;
