export default [
  {
    path: '/terms',
    name: 'terms',
    meta: {
      requiresAuth: false,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/Terms.vue'),
  },
];
