export default [
  {
    path: '/payfail',
    name: 'payfail',
    meta: {
      requiresAuth: true,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/PayFail.vue'),
  },
];
