export default [
  {
    path: '/panel/:serverId',
    name: 'panel',
    meta: {
      requiresAuth: true,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/Panel.vue'),
  },
];
