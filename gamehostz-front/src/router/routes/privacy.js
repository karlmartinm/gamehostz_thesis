export default [
  {
    path: '/privacy',
    name: 'privacy',
    meta: {
      requiresAuth: false,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/Privacy.vue'),
  },
];
