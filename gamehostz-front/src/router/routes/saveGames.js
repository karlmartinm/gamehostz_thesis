export default [
  {
    path: '/save-games',
    name: 'save-games',
    meta: {
      requiresAuth: true,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/SaveGames.vue'),
  },
];
