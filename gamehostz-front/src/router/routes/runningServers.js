export default [
  {
    path: '/running-servers',
    name: 'running-servers',
    meta: {
      requiresAuth: true,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/RunningServers.vue'),
  },
];
