export default [
  {
    path: '/games',
    name: 'games',
    meta: {
      requiresAuth: true,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/Games.vue'),
  },
];
