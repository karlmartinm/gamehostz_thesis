export default [
  {
    path: '/checkout',
    name: 'checkout',
    meta: {
      requiresAuth: true,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/Checkout.vue'),
  },
];
