export default [
  {
    path: '/paysuccess',
    name: 'paysuccess',
    meta: {
      requiresAuth: true,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/components/PaySuccess.vue'),
  },
];
