#!/usr/bin/env bash
echo "userdata start"
adduser --disabled-password --gecos "" client
printf '[default]\naccess_key = null\nsecret_key = null\nhost_base = fra1.digitaloceanspaces.com\nhost_bucket = gh-sm.fra1.digitaloceanspaces.com' >> /home/client/.s3cfg_sm
printf '[default]\naccess_key = null\nsecret_key = null\nhost_base = fra1.digitaloceanspaces.com\nhost_bucket = gh-sm.fra1.digitaloceanspaces.com' >> /root/.s3cfg
ufw allow 25565
ufw allow  from null to any port 3000
ufw allow from null to any port 3000
ufw reload
pm2 startup
s3cmd get s3://gh-sm/sms.zip /root/sms.zip
mkdir /root/sms
yes|tr y n|unzip -q /root/sms.zip -d /root/sms
yes|tr y n|mv -i /root/sms/dist/* /root/sms
su - client -c "s3cmd -c .s3cfg_sm get s3://gh-sm/sm.zip /home/client/sm.zip --force"
rm /home/client/.s3cfg_sm
mkdir /home/client/sm
yes|tr y n|unzip -q /home/client/sm.zip -d /home/client/sm
yes|tr y n|mv -i /home/client/sm/dist/* /home/client/sm
printf 'APP_PORT=3005\nAPP_ENV=prod\nAPP_UUID=7a3c362c-4ea2-4f57-a008-021681fca96d\nAPP_PROVIDER=DIGITAL_OCEAN\nAPP_PROVIDER_API_KEY=null\nJAVA_HOST=null\nJAVA_PORT=null\nGL_HOST=null\nGL_PORT=null\nGL_HOSTNAME=sms\nS3_KEY=null\nS3_SECRET=null\nS3_ENDPOINT=fra1.digitaloceanspaces.com\nS3_BUCKET=gamesave' >> /root/sms/.env
pm2 start /root/sms/index.js --name "sms"
printf 'APP_PORT=3000\nAPP_ENV=prod\nAPP_UUID=7a3c362c-4ea2-4f57-a008-021681fca96d\nAPP_STORAGE_PATH=/storage\nAPP_BASE_PATH=/home/client/sm\nJAVA_HOST=null\nJAVA_PORT=null\nGL_HOST=null\nGL_PORT=null\nGL_HOSTNAME=sm' >> /home/client/sm/.env
mkdir /home/client/sm/minecraft
wget -O /home/client/sm/minecraft/linuxgsm.sh https://linuxgsm.sh
chmod +x /home/client/sm/minecraft/linuxgsm.sh
chown -R client:client /home/client
su - client -c "cd /home/client/sm/minecraft && ./linuxgsm.sh mcserver"
su - client -c "/home/client/sm/minecraft/mcserver auto-install"
chown -R client:client /home/client
su - client -c "pm2 start /home/client/sm/index.js --name "sm""
ls /home/client/sm/minecraft
echo "userdata end"
