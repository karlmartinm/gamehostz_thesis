package com.gamehostz.main.rest.gameserver;

import com.gamehostz.main.MainApplication;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(classes = MainApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GameServerControllerPortalIntTest {

	@Autowired
	private MockMvc mvc;

	@Test
	@DisplayName("Should receive 3 games")
	@WithMockUser
	void gameConfigs() throws Exception {
		mvc.perform(get("/api/portal/gameconfigs").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
		//			.andExpect(jsonPath("$", hasSize(3)))
		//			.andExpect(jsonPath("$[0].abbreviation").value("CSGO"));
		//			.andExpect(jsonPath("$[0]").value("Minecraft"))
		//			.andExpect(jsonPath("$[1]").value("CSGO"))
		//			.andExpect(jsonPath("$[2]").value("Insurgency"));
	}
}
