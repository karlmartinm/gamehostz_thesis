package com.gamehostz.main.rest.gameserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamehostz.main.MainApplication;
import com.gamehostz.main.domain.event.EventType;
import com.gamehostz.main.rest.event.EventRequestModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(classes = MainApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EventControllerIntTest {

	@Autowired
	private MockMvc mvc;
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	@DisplayName("Given event request should save request to database")
	@WithMockUser
	void gameConfigs() throws Exception {
		EventRequestModel model = EventRequestModel.newBuilder()
			.userId(UUID.randomUUID())
			.serverId(UUID.randomUUID())
			.type(EventType.LOGIN)
			.additionalJson("{'userAgent': 'Chrome'}")
			.build();

		mvc.perform(MockMvcRequestBuilders.post("/api/portal/event")
			.contentType(MediaType.APPLICATION_JSON)
			.content(objectMapper.writeValueAsBytes(model)))
			.andExpect(status().isOk());
	}
}
