package com.gamehostz.main.usecases.getgameconfigs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class GetGameConfigsTest {

	private GetGameConfigs getGameConfigs;

	@BeforeEach
	void setUp() {
		getGameConfigs = new GetGameConfigs();
	}

	@Test
	void getGamePrices() throws IOException {
		assertThat(getGameConfigs.getGamePrices()).isNotEmpty();
	}
}
