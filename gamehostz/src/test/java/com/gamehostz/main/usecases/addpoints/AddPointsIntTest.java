package com.gamehostz.main.usecases.addpoints;

import com.gamehostz.main.MainApplication;
import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.transactions.CurrencyType;
import com.gamehostz.main.domain.transactions.Transaction;
import com.gamehostz.main.helpers.CognitoUtils;
import com.gamehostz.main.usecases.usepoints.UsePoints;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(classes = MainApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AddPointsIntTest {

	static final long EXPIRATIONTIME = 864_000_000; // 10 days
	static final String SECRET = "ThisIsASecretThisIsASecretThisIsASecretThisIsASecretThisIsASecretThisIsASecretThisIsASecretThisIsASecretThisIsASecretThisIsASecretThisIsASecretThisIsASecret";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";

	@Autowired
	private MockMvc mvc;
	@Autowired
	private AddPoints addPoints;
	@Autowired
	private UsePoints usePoints;
	@Autowired
	private CustomerRepository customerRepository;
	@MockBean
	private CognitoUtils cognitoUtils;

	//TODO: split test into smaller tests
	@Test
	@DisplayName("Given add points functionality, adding points directly, should add rows to the db")
	@WithMockUser
	void execute() throws Exception {
		UUID authId = UUID.randomUUID();
		when(cognitoUtils.getCognitoUserUUIDFromRequest()).thenReturn(authId);
		mvc.perform(MockMvcRequestBuilders.post("/api/portal/customer"))
			.andExpect(status().isOk());

		Optional<Customer> customer = customerRepository.findByAuthId(authId);
		List<Transaction> transactions = addPoints.execute(AddPoints.Request.of(customer.get().getAuthId(), BigDecimal.TEN, CurrencyType.EUR));
		assertThat(transactions.size()).isEqualTo(4);

		mvc.perform(MockMvcRequestBuilders.get("/api/portal/customer/balance"))
			.andExpect(status().isOk())
		.andExpect(content().string("1200.00"));

		usePoints.execute(UsePoints.Request.of(customer.get().getId(), BigDecimal.valueOf(200)));

		mvc.perform(MockMvcRequestBuilders.get("/api/portal/customer/balance"))
			.andExpect(status().isOk())
			.andExpect(content().string("1000.00"));
	}

}
