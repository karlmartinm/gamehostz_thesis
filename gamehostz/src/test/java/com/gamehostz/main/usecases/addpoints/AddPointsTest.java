package com.gamehostz.main.usecases.addpoints;

import com.gamehostz.main.domain.client.UserException;
import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.transactions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestPropertySource(properties = {
	"gamehostz.account.id=20354d7a-e4fe-47af-8ff6-187bca92f3f9",
})
class AddPointsTest {

	private AddPoints addPoints;
	@Mock
	TransactionRepository transactionRepository;
	@Mock
	CustomerRepository customerRepository;
	@Value("${gamehostz.account.id}")
	String ghAccountId;
	@Value("${gamehostz.account.eur.id}")
	String ghEURAccountId;
	@Value("${gamehostz.account.usd.id}")
	String ghUSDAccountId;
	@Value("${gamehostz.account.points.id}")
	String ghPOINTSAccountId;

	@Configuration
	static class Config {

		@Bean
		public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
			return new PropertySourcesPlaceholderConfigurer();
		}

	}

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		addPoints = new AddPoints(ghAccountId, transactionRepository, customerRepository);
	}

	@Test
	@DisplayName("Given points transformation use case, when adding 10 EUR, then the user should have 1200 points")
	void happyPath() {
		UUID authId = UUID.randomUUID();
		mockRepositories(authId);

		List<Transaction> transactions = addPoints.execute(AddPoints.Request.of(authId, BigDecimal.TEN, CurrencyType.EUR));
		Optional<BigDecimal> sum = transactions.stream()
			.filter(transaction -> transaction.getCurrencyAccount().getCurrencyType().equals(CurrencyType.POINT))
			.map(transaction -> transaction.getAmount().multiply(transaction.getDirection().equals(Direction.DEBIT) ? BigDecimal.ONE : BigDecimal.valueOf(-1)))
			.reduce(BigDecimal::add);

		assertThat(sum.get()).isEqualTo(BigDecimal.valueOf(1200));
	}

	@Test
	@DisplayName("Given points transformation use case, when adding 10 EUR, then the GH should have 10 EUR")
	void happyPathForGH() {
		UUID authId = UUID.randomUUID();
		mockRepositories(authId);

		List<Transaction> transactions = addPoints.execute(AddPoints.Request.of(authId, BigDecimal.TEN, CurrencyType.EUR));
		Optional<BigDecimal> sum = transactions.stream()
			.filter(transaction -> transaction.getCurrencyAccount().getId() != null)
			.filter(transaction -> transaction.getCurrencyAccount().getId().equals(UUID.fromString(ghEURAccountId)))
			.filter(transaction -> transaction.getCurrencyAccount().getCurrencyType().equals(CurrencyType.EUR))
			.map(transaction -> transaction.getAmount().multiply(transaction.getDirection().equals(Direction.DEBIT) ? BigDecimal.ONE : BigDecimal.valueOf(-1)))
			.reduce(BigDecimal::add);

		assertThat(sum.get()).isEqualTo(BigDecimal.valueOf(10));
	}

	@Test
	@DisplayName("Given points transformation use case, when adding 0 EUR, then the system should return an error")
	void errorOnZero() {
		UUID authId = UUID.randomUUID();
		mockRepositories(authId);

		assertThatThrownBy(() -> {
			addPoints.execute(AddPoints.Request.of(authId, BigDecimal.ZERO, CurrencyType.EUR));
		}).isInstanceOf(UserException.class)
			.hasMessageContaining("TRANSACTION_AMOUNT_TOO_LOW");
	}

	@Test
	@DisplayName("Given points transformation use case, when adding 1 USD, then the user should have 110 points")
	void smallHappyPath() {
		UUID authId = UUID.randomUUID();
		mockRepositories(authId);

		List<Transaction> transactions = addPoints.execute(AddPoints.Request.of(authId, BigDecimal.ONE, CurrencyType.USD));
		Optional<BigDecimal> sum = transactions.stream()
			.filter(transaction -> transaction.getCurrencyAccount().getCurrencyType().equals(CurrencyType.POINT))
			.map(transaction -> transaction.getAmount().multiply(transaction.getDirection().equals(Direction.DEBIT) ? BigDecimal.ONE : BigDecimal.valueOf(-1)))
			.reduce(BigDecimal::add);

		assertThat(sum.get()).isEqualTo(BigDecimal.valueOf(110));
	}

	@Test
	@DisplayName("Given points transformation use case, when adding -10 USD, then the system should return an error")
	void errorOnNegative() {
		UUID authId = UUID.randomUUID();
		mockRepositories(authId);

		assertThatThrownBy(() -> {
			addPoints.execute(AddPoints.Request.of(authId, new BigDecimal("-10"), CurrencyType.USD));
		}).isInstanceOf(UserException.class)
			.hasMessageContaining("TRANSACTION_AMOUNT_TOO_LOW");
	}

	@Test
	@DisplayName("Given points transformation use case, when adding biggest amount USD, then the system should not fail")
	void biggestHappyPath() {
		UUID authId = UUID.randomUUID();
		mockRepositories(authId);

		List<Transaction> transactions = addPoints.execute(AddPoints.Request.of(authId, new BigDecimal(Integer.MAX_VALUE), CurrencyType.USD));
		Optional<BigDecimal> sum = transactions.stream()
			.filter(transaction -> transaction.getCurrencyAccount().getCurrencyType().equals(CurrencyType.POINT))
			.map(transaction -> transaction.getAmount().multiply(transaction.getDirection().equals(Direction.DEBIT) ? BigDecimal.ONE : BigDecimal.valueOf(-1)))
			.reduce(BigDecimal::add);

		assertThat(sum.get()).isEqualTo(BigDecimal.valueOf(236223201170l));
	}

	private void mockRepositories(UUID authId) {
		when(transactionRepository.saveAll(any())).thenReturn(any());
		Customer customer = createCustomer();
		when(customerRepository.findByAuthId(authId)).thenReturn(Optional.of(customer));
		Customer ghCustomerAccount = createGHCustomer();
		when(customerRepository.findById(UUID.fromString(ghAccountId))).thenReturn(Optional.of(ghCustomerAccount));
	}

	private Customer createCustomer() {
		Set<CurrencyAccount> currencyAccounts = new HashSet<>();
		for (CurrencyType value : CurrencyType.values()) {
			currencyAccounts.add(new CurrencyAccount(value));
		}
		return Customer.CustomerBuilder.aCustomer()
			.withAuthId(UUID.randomUUID())
			.withCurrencyAccounts(currencyAccounts)
			.build();
	}

	private Customer createGHCustomer() {
		Set<CurrencyAccount> currencyAccounts = new HashSet<>();
		currencyAccounts.add(new CurrencyAccount(UUID.fromString(ghEURAccountId), CurrencyType.EUR));
		currencyAccounts.add(new CurrencyAccount(UUID.fromString(ghUSDAccountId), CurrencyType.USD));
		currencyAccounts.add(new CurrencyAccount(UUID.fromString(ghPOINTSAccountId), CurrencyType.POINT));
		return Customer.CustomerBuilder.aCustomer()
			.withAuthId(UUID.randomUUID())
			.withCurrencyAccounts(currencyAccounts)
			.build();
	}
}
