package com.gamehostz.main.usecases.createcustomer;

import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.transactions.CurrencyType;
import com.gamehostz.main.usecases.addpoints.AddPoints;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CreateCustomerTest {

	private CreateCustomer createCustomer;
	@Mock
	CustomerRepository customerRepository;
	@Mock
	AddPoints addPoints;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		createCustomer = new CreateCustomer(customerRepository, addPoints);
	}

	@Test
	@DisplayName("Given customer logic, when creating a new customer, then customer should exist")
	void execute() {
		UUID authId = UUID.randomUUID();
		when(customerRepository.save(any(Customer.class))).thenReturn(any());
		Customer customer = createCustomer.execute(CreateCustomer.Request.of(authId));
		assertThat(customer).isNotNull();
	}

	@Test
	@DisplayName("Given customer logic, when creating a new customer, then customer should have currency accounts")
	void executeCheckForCurrencies() {
		UUID authId = UUID.randomUUID();
		Customer customer = createCustomer.execute(CreateCustomer.Request.of(authId));
		assertThat(customer.getCurrencyAccounts())
			.extracting("currencyType")
			.contains(CurrencyType.EUR, CurrencyType.USD, CurrencyType.POINT);
	}
}
