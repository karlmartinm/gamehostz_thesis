package com.gamehostz.main.usecases.generateuserdata;

import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.serverprovider.ServerProviderName;
import com.gamehostz.main.usecases.getgameconfigs.GetGameConfigs;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import static org.apache.commons.codec.Charsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

class GenerateUserDataTest {


	private final GenerateUserData generateUserData;
	private final GetGameConfigs getGameConfigs;
	private UUID serverId;

	public GenerateUserDataTest() {
		this.generateUserData = new GenerateUserData();
		this.getGameConfigs = new GetGameConfigs();
	}

	@Test
	void execute() throws IOException {
		GameConfig internalGameConfig = getGameConfigs.getGameConfigs().get("mc");
		serverId = UUID.fromString("7a3c362c-4ea2-4f57-a008-021681fca96d");

		String executeScript = generateUserData.execute(serverId, ServerProviderName.DIGITAL_OCEAN, internalGameConfig, null);
		System.out.println(executeScript);
		assertThat(FileUtils.readFileToString(new File("src/test/resources/expected_minecraft_script.sh"), UTF_8)).isEqualTo(executeScript);
	}
}
