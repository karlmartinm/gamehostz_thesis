package com.gamehostz.main.security;

import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import java.util.Collections;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String[] AUTH_WHITELIST = {
		"/v2/api-docs",
		"/configuration/**",
		"/swagger*/**",
		"/webjars/**"
	};

	@Autowired
	private ServerInfoRepository serverInfoRepository;

	@Bean
	public FilterRegistrationBean<JwtAuthFilter> authTokenFilterBean() throws Exception {
		FilterRegistrationBean<JwtAuthFilter> registration = new FilterRegistrationBean<>();
		registration.setFilter(new JwtAuthFilter());
		registration.setUrlPatterns(Collections.singletonList("/api/portal/*"));
		return registration;
	}

	@Bean
	public FilterRegistrationBean<ServerManagerFilter> authServerManagerFilterBean() throws Exception {
		FilterRegistrationBean<ServerManagerFilter> registration = new FilterRegistrationBean<>();
		registration.setFilter(new ServerManagerFilter(serverInfoRepository));
		registration.setUrlPatterns(Collections.singletonList("/api/internal/*"));
		return registration;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable() // TODO enable csrf !/?
			.authorizeRequests()
			.antMatchers(AUTH_WHITELIST).permitAll()
			.antMatchers("/api/internal/**").permitAll()
			.antMatchers("/api/portal/**").permitAll()
			.anyRequest().denyAll()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
}
