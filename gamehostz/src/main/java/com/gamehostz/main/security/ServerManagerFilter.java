package com.gamehostz.main.security;

import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ServerManagerFilter extends OncePerRequestFilter {

	private Logger logger = LoggerFactory.getLogger(ServerManagerFilter.class);
	private ServerInfoRepository serverInfoRepository;

	ServerManagerFilter(ServerInfoRepository serverInfoRepository) {
		this.serverInfoRepository = serverInfoRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
		throws ServletException, IOException {
		// TODO Currently just checks if there is a active server in the DB with the same IP as request.
		// TODO Mby figure out sth better in the future

		// Uncomment this to disable the auth
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SERVER_MANAGER"));
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
			UUID.randomUUID(),
			null,
			authorities
		);
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);

		// Uncomment this to enable the auth
		/*final String ip = req.getRemoteAddr();
		serverInfoRepository.findAllByAddressAndClosedAtIsNullAndClosedByIsNull(ip).stream()
			.findFirst()
			.ifPresentOrElse(
				serverInfo -> {
					List<GrantedAuthority> authorities = new ArrayList<>();
					authorities.add(new SimpleGrantedAuthority("ROLE_SERVER_MANAGER"));
					UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
						serverInfo.getId(),
						null,
						authorities
					);
					SecurityContextHolder.getContext().setAuthentication(authenticationToken);
					logger.info("SM authorized, IP: {}", ip);
				},
				() -> logger.error("SM unauthorized, IP: {}", ip)
			);*/

		chain.doFilter(req, res);
	}
}
