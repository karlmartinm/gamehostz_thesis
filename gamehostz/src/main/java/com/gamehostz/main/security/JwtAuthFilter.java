package com.gamehostz.main.security;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class JwtAuthFilter extends OncePerRequestFilter {

	private Logger logger = LoggerFactory.getLogger(JwtAuthFilter.class);

	private static final String AUTH_HEADER_STRING = "Authorization";
	private static final String AUTH_BEARER_STRING = "Bearer";

	private String ISSUER = "https://cognito-idp.eu-central-1.amazonaws.com/eu-central-1_brm7fd0Jc";
	private String KEY_STORE_PATH = "/.well-known/jwks.json";

	// should cache keys
	private RemoteJWKSet remoteJWKSet;

	JwtAuthFilter() throws MalformedURLException {
		URL JWKUrl = new URL(ISSUER + KEY_STORE_PATH);
		this.remoteJWKSet = new RemoteJWKSet(JWKUrl);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
		throws IOException, ServletException {

		if (req.getHeader(AUTH_HEADER_STRING) != null) {
			try {
				String header = req.getHeader(AUTH_HEADER_STRING).replace(AUTH_BEARER_STRING, "");
				logger.info("Request header: {}", header);
				JWT jwt = JWTParser.parse(header);

				String issuer = jwt.getJWTClaimsSet().getIssuer();
				logger.info("Got JWT issuer: {}", issuer);

				// Check if issuer in our user pool
				if (isIssuerInUserPool(issuer)) {
					JWSKeySelector<SecurityContext> keySelector = new JWSVerificationKeySelector<SecurityContext>(JWSAlgorithm.RS256, remoteJWKSet);
					ConfigurableJWTProcessor<SecurityContext> jwtProcessor = new DefaultJWTProcessor<>();
					jwtProcessor.setJWSKeySelector(keySelector);

					// check token
					JWTClaimsSet claimsSet = jwtProcessor.process(jwt, null);
					List<GrantedAuthority> authorities = new ArrayList<>();
					authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
					UsernamePasswordAuthenticationToken authenticationToken =
						new UsernamePasswordAuthenticationToken(claimsSet, null, authorities);
					SecurityContextHolder.getContext().setAuthentication(authenticationToken);
				}
			} catch (Exception e) {
				logger.error("JWT Auth error: ", e);
			}
		}
		chain.doFilter(req, res);
	}

	private boolean isIssuerInUserPool(String issuer) {
		return ISSUER.equals(issuer);
	}
}
