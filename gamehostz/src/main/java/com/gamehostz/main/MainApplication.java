package com.gamehostz.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.gameserver.Regions;
import com.gamehostz.main.domain.gameserver.ServerPricing;
import com.gamehostz.main.usecases.getgameconfigs.GetGameConfigs;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.zalando.problem.ProblemModule;
import org.zalando.problem.spring.web.advice.ProblemHandling;
import org.zalando.problem.violations.ConstraintViolationProblemModule;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.time.Clock;
import java.util.*;

@SpringBootApplication
@EnableScheduling
@EnableSwagger2
public class MainApplication {
	public static Map<String, GameConfig> gameConfigs = new HashMap<>();
	public static List<ServerPricing> serverPrices = new ArrayList<>();
	public static Regions regions;
	public static UUID serviceAuthKey;

	public static void main(String[] args) throws IOException {
		GetGameConfigs getGameConfigs = new GetGameConfigs();
		gameConfigs = getGameConfigs.getGameConfigs();
		serverPrices = getGameConfigs.getGamePrices();
		regions = getGameConfigs.getRegions();

		serviceAuthKey = UUID.randomUUID();

		SpringApplication.run(MainApplication.class, args);
	}

	@Bean
	public ApplicationRunner initialize() {
		return new BootStrap();
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper().registerModules(
			new ProblemModule(),
			new ConstraintViolationProblemModule());
	}

	@ControllerAdvice
	public class ExceptionHandler implements ProblemHandling {
	}

	@Bean
	Clock clock() {
		return Clock.system(Constants.OFFSET);
	}

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
			.apis(RequestHandlerSelectors.any())
			.paths(PathSelectors.any())
			.build();
	}
}
