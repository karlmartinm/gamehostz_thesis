package com.gamehostz.main.usecases.validateuserstartserver;

import com.gamehostz.main.domain.client.UserException;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.usecases.getbalance.GetBalance;
import com.gamehostz.main.usecases.getbillingamount.GetBillingAmount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.UUID;

import static com.gamehostz.main.domain.client.UserError.NOT_ENOUGH_POINTS;
import static com.gamehostz.main.domain.client.UserError.SERVER_ALREADY_RUNNING;

@Component
public class ValidateUserStartServer {

	@Autowired
	private ServerInfoRepository serverInfoRepository;

	@Autowired
	private GetBalance getBalance;

	private GetBillingAmount getBillingAmount;

	public ValidateUserStartServer() {
		this.getBillingAmount = new GetBillingAmount();
	}

	public void execute(Request request) {
		List<ServerInfo> runningUserServers = serverInfoRepository.findAllByCreatedByAndClosedAtIsNull(request.userId);
		if (runningUserServers.size() != 0) {
			throw new UserException(SERVER_ALREADY_RUNNING);
		}

		BigDecimal balance = getBalance.executeByAuth(GetBalance.Request.of(request.userId));
		BigDecimal billingAmount = getBillingAmount.execute(GetBillingAmount.Request.ofSlug(request.ramAmountSlug));
		// Note that prices should always be dividable by 4
		BigDecimal quarterlyBillingAmount = billingAmount.divide(BigDecimal.valueOf(4), RoundingMode.HALF_EVEN);
		if (balance.compareTo(quarterlyBillingAmount) < 0) {
			throw new UserException(NOT_ENOUGH_POINTS);
		}
	}

	public static class Request {
		private final UUID userId;
		private final String ramAmountSlug;

		Request(UUID userId, String ramAmountSlug) {
			this.userId = userId;
			this.ramAmountSlug = ramAmountSlug;
		}

		public static Request of(UUID userId, String ramAmountSlug) {
			return new Request(userId, ramAmountSlug);
		}
	}
}
