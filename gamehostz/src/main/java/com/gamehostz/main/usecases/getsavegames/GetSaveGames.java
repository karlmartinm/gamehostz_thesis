package com.gamehostz.main.usecases.getsavegames;

import com.gamehostz.main.domain.savegame.SaveGameRepository;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.usecases.startserver.StartServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class GetSaveGames {
	private Logger log = LoggerFactory.getLogger(StartServer.class);
	private ServerInfoRepository serverInfoRepository;
	private SaveGameRepository saveGameRepository;
	private final Clock clock;

	public GetSaveGames(ServerInfoRepository serverInfoRepository, SaveGameRepository saveGameRepository, Clock clock) {
		this.serverInfoRepository = serverInfoRepository;
		this.saveGameRepository = saveGameRepository;
		this.clock = clock;
	}

	public List<SaveGameDTO> execute(UUID userId) {
		// TODO bug with multiple SAVING status
		return saveGameRepository.findSaveGamesWithUserId(userId).stream()
			.filter(distinctByKey(SaveGameDTO::getSaveGameId))
			.collect(Collectors.toList());
	}

	private static <T> Predicate<T> distinctByKey(
		Function<? super T, ?> keyExtractor) {

		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}
}

