package com.gamehostz.main.usecases.stopserver;

import com.gamehostz.main.domain.savegame.SaveGame;
import com.gamehostz.main.domain.savegame.SaveGameException;
import com.gamehostz.main.domain.savegame.SaveGameRepository;
import com.gamehostz.main.domain.savegame.SaveGameState;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.domain.serverprovider.ServerProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.domain.savegame.SaveGameError.SAVING_FAILED;
import static com.gamehostz.main.domain.serverprovider.ProviderError.NO_RUNNING_SERVER;

@Component
public class SavingDone {
	private final Logger log = LoggerFactory.getLogger(SavingDone.class);
	private final ServerInfoRepository serverInfoRepository;
	private final SaveGameRepository saveGameRepository;

	public SavingDone(ServerInfoRepository serverInfoRepository, SaveGameRepository saveGameRepository) {
		this.serverInfoRepository = serverInfoRepository;
		this.saveGameRepository = saveGameRepository;
	}

	@Transactional
	public void execute(SavingDone.Request request) {
		log.info("saving done: {}", request.serverId);
		ServerInfo serverInfo = serverInfoRepository.findById(request.serverId)
			.orElseThrow(() -> new ServerProviderException(NO_RUNNING_SERVER));

		handleSaveGame(serverInfo, request.saveSize);
	}

	private void handleSaveGame(ServerInfo serverInfo, long saveSize) {
		if (!gameConfigs.get(serverInfo.getGameAbbreviation()).getSupportsSave()) {
			return;
		}

		SaveGame saveGame = saveGameRepository
			.findOneByIdOrderByCreatedAtDesc(serverInfo.getSaveGameId())
			.orElseThrow(() -> new SaveGameException(SAVING_FAILED));

		saveGame.setState(SaveGameState.ACTIVE);
		saveGame.setSaveSize(saveSize);
		saveGameRepository.save(saveGame);
	}

	public static class Request {
		private final UUID serverId;
		private final long saveSize;

		Request(UUID serverId, long saveSize) {
			this.serverId = serverId;
			this.saveSize = saveSize;
		}

		public static SavingDone.Request of(UUID serverId, long saveSize) {
			return new SavingDone.Request(serverId, saveSize);
		}
	}
}
