package com.gamehostz.main.usecases.lambda.getsaves;

import com.gamehostz.main.domain.savegame.SaveGameRepository;
import com.gamehostz.main.domain.savegame.SaveGameState;
import com.gamehostz.main.usecases.getsavegames.SaveGameDTO;
import com.gamehostz.main.usecases.loadserver.LoadServer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GetSaves {

	private final Logger log = LoggerFactory.getLogger(LoadServer.class);
	private final SaveGameRepository saveGameRepository;

	public GetSaves(SaveGameRepository saveGameRepository) {
		this.saveGameRepository = saveGameRepository;
	}

	public List<LambdaSaveGame> execute() {
		log.info("lambda get all saves");
		List<SaveGameDTO> allDeleteRequests = saveGameRepository.findAllDeleteRequests();
		List<LambdaSaveGame> lambdaSaveGames = new ArrayList<>();

		lambdaSaveGames.addAll(getOverwriteSaveDTOs());
		// add all delete request saves
		lambdaSaveGames.addAll(allDeleteRequests.stream()
			.map(saveGameDTO -> convert(saveGameDTO, SaveGameState.DELETE_REQUEST))
			.collect(Collectors.toList())
		);
		return lambdaSaveGames;
	}

	private List<LambdaSaveGame> getOverwriteSaveDTOs() {
		List<LambdaSaveGame> overwriteLambdaDTOs = new ArrayList<>();
		List<SaveGameDTO> allActive = saveGameRepository.findAllActive();
		Map<UUID, List<SaveGameDTO>> saveGameDTOMap = new HashMap<>();
		for (SaveGameDTO save : allActive) {
			saveGameDTOMap.computeIfAbsent(save.getSaveGameId(), s -> new ArrayList<>()).add(save);
		}
		for (UUID uuid : saveGameDTOMap.keySet()) {
			int i = 0;
			for (SaveGameDTO saveGameDTO : saveGameDTOMap.getOrDefault(uuid, Collections.emptyList())) {
				if (i > 0) {
					overwriteLambdaDTOs.add(convert(saveGameDTO, SaveGameState.OVERWRITTEN));
				}
				i++;
			}
		}
		return overwriteLambdaDTOs;
	}

	private LambdaSaveGame convert(SaveGameDTO saveGameDTO, SaveGameState saveGameState) {
		LambdaSaveGame lambdaSaveGame = new LambdaSaveGame();
		lambdaSaveGame.setSaveGameId(saveGameDTO.getSaveGameId());
		lambdaSaveGame.setServerId(saveGameDTO.getServerId());
		lambdaSaveGame.setSaveGameState(saveGameState);
		return lambdaSaveGame;
	}
}
