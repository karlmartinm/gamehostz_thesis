package com.gamehostz.main.usecases.addpoints;

import com.gamehostz.main.domain.client.UserException;
import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.transactions.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static com.gamehostz.main.domain.client.UserError.*;

@Component
public class AddPoints {
	private final TransactionRepository transactionRepository;
	private final CustomerRepository customerRepository;

	private String ghAccountId;

	public AddPoints(@Value("${gamehostz.account.id}") String ghAccountId,
					 TransactionRepository transactionRepository,
					 CustomerRepository customerRepository) {
		this.transactionRepository = transactionRepository;
		this.customerRepository = customerRepository;
		this.ghAccountId = ghAccountId;
	}

	@Transactional
	public List<Transaction> execute(Request request) {
		if (request.amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new UserException(TRANSACTION_AMOUNT_TOO_LOW);
		}

		Customer customer = customerRepository.findByAuthId(request.authId)
			.orElseThrow(() -> new UserException(CUSTOMER_NOT_FOUND));

		// Cash transaction of debit from user
		Transaction depositDebit = Transaction.TransactionBuilder.aTransaction()
			.withAmount(request.amount)
			.withCurrencyAccount(customer.getCurrencyAccounts().stream()
				.filter(currencyAccount -> currencyAccount.getCurrencyType().equals(request.currencyType))
				.findAny().orElseThrow(() -> new UserException(TRANSACTION_ACCOUNT_NOT_FOUND)))
			.withDirection(Direction.DEBIT)
			.withTransactionType(TransactionType.DEPOSIT)
			.build();

		// Cash transaction to credit the user
		Transaction conversionCredit = Transaction.TransactionBuilder.aTransaction()
			.withAmount(request.amount)
			.withCurrencyAccount(customer.getCurrencyAccounts().stream()
				.filter(currencyAccount -> currencyAccount.getCurrencyType().equals(request.currencyType))
				.findAny().orElseThrow(() -> new UserException(TRANSACTION_ACCOUNT_NOT_FOUND)))
			.withDirection(Direction.CREDIT)
			.withTransactionType(TransactionType.CONVERSION)
			.build();

		// POINTS added to customer, uses multiplier
		Transaction conversionDebitPoints = Transaction.TransactionBuilder.aTransaction()
			.withAmount(request.amount.multiply(request.currencyType.getMultiplier()))
			.withCurrencyAccount(customer.getCurrencyAccounts().stream()
				.filter(currencyAccount -> currencyAccount.getCurrencyType().equals(CurrencyType.POINT))
				.findAny().orElseThrow(() -> new UserException(POINTS_ACCOUNT_NOT_FOUND)))
			.withDirection(Direction.DEBIT)
			.withTransactionType(TransactionType.CONVERSION)
			.build();

		Customer ghAccount = customerRepository.findById(UUID.fromString(ghAccountId))
			.orElseThrow(() -> new UserException(GH_ACCOUNT_NOT_FOUND));

		// Cash transaction to GameHostZ account for accounting purposes
		// If transaction is add points with currency POINTS then we should subtract GH POINTS
		// this happens when user signs up and gets free points
		Transaction ghDebitPayment = Transaction.TransactionBuilder.aTransaction()
			.withAmount(request.amount)
			.withCurrencyAccount(ghAccount.getCurrencyAccounts().stream()
				.filter(currencyAccount -> currencyAccount.getCurrencyType().equals(request.currencyType))
				.findAny().orElseThrow(() -> new UserException(GH_ACCOUNT_NOT_FOUND)))
			.withDirection(request.currencyType == CurrencyType.POINT ? Direction.CREDIT : Direction.DEBIT)
			.withTransactionType(request.currencyType == CurrencyType.POINT ? TransactionType.WITHDRAW : TransactionType.PAYMENT)
			.build();

		List<Transaction> transactions = List.of(depositDebit, conversionCredit, conversionDebitPoints, ghDebitPayment);
		transactionRepository.saveAll(transactions);
		return transactions;
	}

	public static class Request {
		private final UUID authId;
		private final BigDecimal amount;
		private final CurrencyType currencyType;


		Request(UUID authId, BigDecimal amount, CurrencyType currencyType) {
			this.authId = authId;
			this.amount = amount;
			this.currencyType = currencyType;
		}

		public static Request of(UUID authId, BigDecimal amount, CurrencyType currencyType) {
			return new Request(authId, amount, currencyType);
		}
	}
}
