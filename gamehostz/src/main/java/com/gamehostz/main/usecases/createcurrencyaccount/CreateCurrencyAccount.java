package com.gamehostz.main.usecases.createcurrencyaccount;

import com.gamehostz.main.domain.transactions.CurrencyAccount;
import com.gamehostz.main.domain.transactions.CurrencyType;

public class CreateCurrencyAccount {
	public CurrencyAccount execute(Request request) {
		return new CurrencyAccount(request.currencyType);
	}

	public static class Request {
		private final CurrencyType currencyType;

		Request(CurrencyType currencyType) {
			this.currencyType = currencyType;
		}

		public static Request of(CurrencyType currencyType) {
			return new Request(currencyType);
		}
	}
}
