package com.gamehostz.main.usecases.getramsettings;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.gameserver.RamSettings;
import com.gamehostz.main.domain.validator.ConfigValidationException;

import java.util.Comparator;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.domain.validator.ValidationError.NO_RAM_FOUND;

public class GetRamSettings {

	public RamSettings execute(Request request) {
		return gameConfigs.get(request.gameName).getGameConfigFiles().stream()
			.filter(gameConfigFile -> gameConfigFile.getRam() != null)
			.flatMap(gameConfigFile -> gameConfigFile.getRam().stream())
			.sorted(Comparator.comparingLong(RamSettings::getMaxPlayers))
			.filter(ramSettings -> isSettingBiggerThan(request, ramSettings))
			.findFirst()
			.orElseThrow(() -> new ConfigValidationException(NO_RAM_FOUND));
	}

	private boolean isSettingBiggerThan(Request request, RamSettings ramSettings) {
		return ramSettings.getMaxPlayers() >= (request.playerCount == null ? getValueFromClientConfig(request) : request.playerCount);
	}

	private long getValueFromClientConfig(Request request) {
		return Long.parseLong(request.clientConfig.getClientConfigSetting().stream()
			.filter(clientConfigSetting -> clientConfigSetting.getName()
				.equals("max-players"))
			.findAny()
			.orElseThrow(() -> new ConfigValidationException(NO_RAM_FOUND))
			.getValue());
	}

	public static class Request {
		private final String gameName;
		private final Integer playerCount;
		private final ClientConfig clientConfig;

		Request(String gameName, Integer playerCount, ClientConfig clientConfig) {
			if ((playerCount == null && clientConfig == null) || gameName == null)
				throw new ConfigValidationException(NO_RAM_FOUND);
			this.gameName = gameName;
			this.playerCount = playerCount;
			this.clientConfig = clientConfig;
		}

		public static Request of(String gameName, ClientConfig clientConfig) {
			return new Request(gameName, null, clientConfig);
		}

		public static Request of(String gameName, Integer playerCount) {
			return new Request(gameName, playerCount, null);
		}
	}
}
