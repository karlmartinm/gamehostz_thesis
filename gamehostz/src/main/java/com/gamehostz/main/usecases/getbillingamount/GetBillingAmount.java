package com.gamehostz.main.usecases.getbillingamount;

import com.gamehostz.main.domain.client.UserException;

import java.math.BigDecimal;

import static com.gamehostz.main.MainApplication.serverPrices;
import static com.gamehostz.main.domain.client.UserError.PRICING_NOT_FOUND;

public class GetBillingAmount {

	public BigDecimal execute(Request request) {
		return serverPrices.stream()
			.filter(serverPricing -> serverPricing.getSlug().contains(request.slug))
			.map(serverPricing -> new BigDecimal(serverPricing.getCostPerHour()))
			.findFirst().orElseThrow(() -> new UserException(PRICING_NOT_FOUND));
	}

	public static class Request {
		private final String slug;

		Request(String slug) {
			this.slug = slug;
		}

		public static Request ofSlug(String slug) {
			return new Request(slug);
		}

	}
}
