package com.gamehostz.main.usecases.stopserver;

import com.gamehostz.main.domain.savegame.SaveGame;
import com.gamehostz.main.domain.savegame.SaveGameLocation;
import com.gamehostz.main.domain.savegame.SaveGameRepository;
import com.gamehostz.main.domain.savegame.SaveGameState;
import com.gamehostz.main.domain.serverprovider.*;
import com.gamehostz.main.usecases.getprovider.GetProvider;
import com.gamehostz.main.usecases.opengslt.OpenGslt;
import com.gamehostz.main.usecases.startserver.StartServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.util.Optional;
import java.util.UUID;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.domain.serverprovider.ProviderError.NO_RUNNING_SERVER;

@Component
public class DeleteServer {

	private final Logger log = LoggerFactory.getLogger(StartServer.class);

	private final Clock clock;

	private final ServerInfoRepository serverInfoRepository;

	private final SaveGameRepository saveGameRepository;

	private final OpenGslt openGslt;

	private final ServerProvider serverProvider;

	public DeleteServer(Clock clock, ServerInfoRepository serverInfoRepository, SaveGameRepository saveGameRepository, OpenGslt openGslt, GetProvider provider) {
		this.clock = clock;
		this.serverInfoRepository = serverInfoRepository;
		this.saveGameRepository = saveGameRepository;
		this.openGslt = openGslt;
		this.serverProvider = provider.execute();
	}

	public void execute(Request request) {
		log.info("stopping server: {} from source {}", request.serverId, request.shutdownEventType);
		ServerInfo serverInfo = serverInfoRepository.findById(request.serverId)
			.orElseThrow(() -> new ServerProviderException(NO_RUNNING_SERVER));

		handleSaveGame(serverInfo);
		openGslt.execute(OpenGslt.Request.of(serverInfo));

		serverInfo.endServer(clock, request.shutdownEventType);
		serverInfoRepository.save(serverInfo);
		// this now deletes droplet, saving games might be broken, add 5 minute timeout here maybe? easy solution
		serverProvider.stopServer(request.serverId);
	}

	private void handleSaveGame(ServerInfo serverInfo) {
		if (!gameConfigs.get(serverInfo.getGameAbbreviation()).getSupportsSave()) {
			return;
		}

		if (serverInfo.getSaveGameId() == null) {
			UUID saveGameId = UUID.randomUUID();
			SaveGame saveGame =
				SaveGame.createSaveGame(saveGameId, clock, SaveGameLocation.DIGITAL_OCEAN, SaveGameState.SAVING);
			saveGameRepository.save(saveGame);
			serverInfo.setSaveGameId(saveGameId);
			return;
		}

		Optional<SaveGame> saveGameOptional = saveGameRepository
			.findOneByIdOrderByCreatedAtDesc(serverInfo.getSaveGameId());

		if (saveGameOptional.isPresent()) {
			SaveGame saveGame = saveGameOptional.get();
			saveGame.setState(SaveGameState.SAVING);
			saveGameRepository.save(saveGame);
		} else {
			log.info("Save game corrupted or not found {}", serverInfo.getId());
		}
	}

	public static class Request {
		private final UUID serverId;
		private final ShutdownEventType shutdownEventType;

		Request(UUID serverId, ShutdownEventType shutdownEventType) {
			this.serverId = serverId;
			this.shutdownEventType = shutdownEventType;
		}

		public static Request of(UUID serverId, ShutdownEventType shutdownEventType) {
			return new Request(serverId, shutdownEventType);
		}
	}
}
