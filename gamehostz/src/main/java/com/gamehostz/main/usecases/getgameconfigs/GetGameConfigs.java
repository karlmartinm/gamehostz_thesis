package com.gamehostz.main.usecases.getgameconfigs;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.gameserver.Region;
import com.gamehostz.main.domain.gameserver.Regions;
import com.gamehostz.main.domain.gameserver.ServerPricing;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.gamehostz.main.helpers.StreamExceptionHandler.handleException;

public class GetGameConfigs {
	public Map<String, GameConfig> getGameConfigs() {
		return FileUtils.listFiles(new File("json/games"), new String[] { "json" }, true)
			.stream()
			.map(handleException(file -> (GameConfig) new ObjectMapper().readValue(file, GameConfig.class)))
			.collect(Collectors.toMap(gameConfig -> gameConfig.getAbbreviation().toLowerCase(), gameConfig -> gameConfig));
	}

	public List<ServerPricing> getGamePrices() throws IOException {
		File json = FileUtils.getFile(new File("json"), "servers.json");
		return new ObjectMapper().readValue(json, new TypeReference<List<ServerPricing>>() {
		});
	}

	public Regions getRegions() throws IOException {
		File json = FileUtils.getFile(new File("json"), "regions.json");
		return new ObjectMapper().readValue(json, new TypeReference<Regions>() {
		});
	}
}
