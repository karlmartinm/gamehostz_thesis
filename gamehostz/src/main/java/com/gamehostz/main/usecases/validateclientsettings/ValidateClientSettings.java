package com.gamehostz.main.usecases.validateclientsettings;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.client.ClientConfigSetting;
import com.gamehostz.main.domain.gameserver.FieldType;
import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.gameserver.GameConfigFileSetting;
import com.gamehostz.main.domain.serverprovider.digitalocean.DigitalOceanImagesJob;
import com.gamehostz.main.domain.validator.*;
import com.gamehostz.main.helpers.Utils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.domain.gameserver.FieldType.*;
import static com.gamehostz.main.domain.validator.ValidationError.*;

@Component
public class ValidateClientSettings {
	private final Map<FieldType, Validator> validationRules;
	private GameConfig gameConfig;

	public ValidateClientSettings() {
		validationRules = new HashMap<>();
		validationRules.put(ENUM, new EnumValidator());
		validationRules.put(BOOLEAN, new BooleanValidator());
		validationRules.put(RANGE, new RangeValidator());
		validationRules.put(STRING, new StringValidator());
	}

	private void validateSetting(ClientConfigSetting clientConfigSetting) {
		GameConfigFileSetting setting =
			getGameConfigFileFromSettingName(gameConfig, clientConfigSetting.getName());
		validationRules.get(setting.getType()).validate(setting, clientConfigSetting.getValue());
	}

	public void validateGame(ClientConfig clientConfig, String game) {
		if (clientConfig == null || clientConfig.getClientConfigSetting() == null) {
			throw new ConfigValidationException(NULL_CONFIG);
		}
		gameConfig = Utils.getGameConfigByGameName(game);

		clientConfig.getClientConfigSetting().forEach(this::validateSetting);
	}

	public void validateRegion(String region) {
		Set<String> availableRegions = DigitalOceanImagesJob.availableImages
			.values()
			.stream()
			.flatMap(image -> image.getRegions().stream())
			.collect(Collectors.toSet());
		if (!availableRegions.contains(region)) {
			throw new ConfigValidationException(NO_REGION_FOUND);
		}
	}

	private GameConfigFileSetting getGameConfigFileFromSettingName(GameConfig gameConfig, String name) {
		return gameConfig.getGameConfigFiles().stream()
			.flatMap(gameConfigFile -> gameConfigFile.getGameConfigFileSetting().stream())
			.filter(gameConfigFileSetting -> gameConfigFileSetting.getName().equals(name))
			.findFirst()
			.orElseThrow(() -> new ConfigValidationException(SETTING_TYPE_MISSING));
	}
}
