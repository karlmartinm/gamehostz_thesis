package com.gamehostz.main.usecases.usepoints;

import com.gamehostz.main.domain.client.UserException;
import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.transactions.*;
import com.gamehostz.main.usecases.getbalance.GetBalance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static com.gamehostz.main.domain.client.UserError.*;

@Component
public class UsePoints {
	private final CustomerRepository customerRepository;
	private final TransactionRepository transactionRepository;
	private final GetBalance getBalance;

	private String ghAccountId;

	public UsePoints(@Value("${gamehostz.account.id}") String ghAccountId, CustomerRepository customerRepository, TransactionRepository transactionRepository, GetBalance getBalance) {
		this.customerRepository = customerRepository;
		this.ghAccountId = ghAccountId;
		this.transactionRepository = transactionRepository;
		this.getBalance = getBalance;
	}

	@Transactional
	public Transaction execute(Request request) {
		Customer customer = customerRepository.findById(request.customerId)
			.orElseThrow(() -> new UserException(CUSTOMER_NOT_FOUND));

		BigDecimal balance = getBalance.executeByCustomer(GetBalance.Request.of(customer.getId()));
		if (balance.compareTo(request.amount) < 0) {
			throw new UserException(NOT_ENOUGH_POINTS);
		}

		Transaction usedPoints = Transaction.TransactionBuilder.aTransaction()
			.withAmount(request.amount)
			.withCurrencyAccount(customer.getCurrencyAccounts().stream()
				.filter(currencyAccount -> currencyAccount.getCurrencyType().equals(CurrencyType.POINT))
				.findAny().orElseThrow(() -> new UserException(POINTS_ACCOUNT_NOT_FOUND)))
			.withDirection(Direction.CREDIT)
			.withTransactionType(TransactionType.PAYMENT)
			.build();

		Customer ghAccount = customerRepository.findById(UUID.fromString(ghAccountId))
			.orElseThrow(() -> new UserException(GH_ACCOUNT_NOT_FOUND));

		Transaction ghDebitPayment = Transaction.TransactionBuilder.aTransaction()
			.withAmount(request.amount)
			.withCurrencyAccount(ghAccount.getCurrencyAccounts().stream()
				.filter(currencyAccount -> currencyAccount.getCurrencyType().equals(CurrencyType.POINT))
				.findAny().orElseThrow(() -> new UserException(GH_ACCOUNT_NOT_FOUND)))
			.withDirection(Direction.DEBIT)
			.withTransactionType(TransactionType.PAYMENT)
			.build();

		transactionRepository.saveAll(List.of(usedPoints, ghDebitPayment));
		return usedPoints;
	}

	public static class Request {
		private final UUID customerId;
		private final BigDecimal amount;


		Request(UUID customerId, BigDecimal amount) {
			this.customerId = customerId;
			this.amount = amount;
		}

		public static Request of(UUID customerId, BigDecimal amount) {
			return new Request(customerId, amount);
		}
	}
}
