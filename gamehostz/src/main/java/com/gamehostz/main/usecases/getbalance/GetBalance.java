package com.gamehostz.main.usecases.getbalance;

import com.gamehostz.main.domain.client.UserException;
import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.transactions.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static com.gamehostz.main.domain.client.UserError.*;

@Component
public class GetBalance {
	private final TransactionRepository transactionRepository;
	private final CustomerRepository customerRepository;

	public GetBalance(TransactionRepository transactionRepository, CustomerRepository customerRepository) {
		this.transactionRepository = transactionRepository;
		this.customerRepository = customerRepository;
	}

	public BigDecimal executeByCustomer(Request request) {
		Customer customer = customerRepository.findById(request.id)
			.orElseThrow(() -> new UserException(CUSTOMER_NOT_FOUND));
		return execute(customer);
	}

	public BigDecimal executeByAuth(Request request) {
		Customer customer = customerRepository.findByAuthId(request.id)
			.orElseThrow(() -> new UserException(CUSTOMER_NOT_FOUND));
		return execute(customer);
	}

	private BigDecimal execute(Customer customer) {
		CurrencyAccount pointsAccount = customer.getCurrencyAccounts().stream().filter(currencyAccount -> currencyAccount.getCurrencyType().equals(CurrencyType.POINT)).findFirst().orElseThrow(() -> new UserException(POINTS_ACCOUNT_NOT_FOUND));
		List<Transaction> transactions = transactionRepository.findAllByCurrencyAccountId(pointsAccount.getId());
		return transactions.stream()
			.map(transaction -> transaction.getAmount().multiply(transaction.getDirection().equals(Direction.DEBIT) ? BigDecimal.ONE : BigDecimal.valueOf(-1)))
			.reduce(BigDecimal::add).orElseThrow(() -> new UserException(BALANCE_CALCULATION_ERROR));
	}

	public static class Request {
		private final UUID id;

		Request(UUID id) {
			this.id = id;
		}

		public static Request of(UUID id) {
			return new Request(id);
		}
	}
}
