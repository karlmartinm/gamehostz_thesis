package com.gamehostz.main.usecases.getserverinfo;

import com.gamehostz.main.domain.serverprovider.ServerProvider;
import com.gamehostz.main.domain.serverprovider.digitalocean.DigitalOceanProvider;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.docker.DockerProvider;
import com.gamehostz.main.rest.gameserver.responsemodel.ServerInfoResponse;
import com.gamehostz.main.usecases.getbillingamount.GetBillingAmount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Consumer;

import static com.gamehostz.main.MainApplication.gameConfigs;

@Component
public class GetServerInfo {
	private Logger log = LoggerFactory.getLogger(GetServerInfo.class);

	private final ServerProvider provider;
	private final DigitalOceanProvider digitalOceanProvider;
	private final DockerProvider dockerProvider;
	private final GetBillingAmount getBillingAmount;

	public GetServerInfo(@Value("${provider.env}") String environment, DigitalOceanProvider digitalOceanProvider, DockerProvider dockerProvider) {
		this.digitalOceanProvider = digitalOceanProvider;
		this.dockerProvider = dockerProvider;
		this.provider = environment.equals("local") ? this.dockerProvider : this.digitalOceanProvider;
		this.getBillingAmount = new GetBillingAmount();
	}

	public void execute(Request request, Consumer<ServerInfoResponse> presenter) {
		log.info("getting server info {} server", request.serverId);
		ServerInfo serverInfo = provider.getServerInfo(request.serverId);
		String port = String.valueOf(gameConfigs.get(serverInfo.getGameAbbreviation()).getPrimaryPort());

		ServerInfoResponse response = ServerInfoResponse.Builder.aServerInfoResponse()
			.serverAddress(serverInfo.getAddress())
			.serverId(serverInfo.getId())
			.serverPort(port)
			.gameAbbreviation(serverInfo.getGameAbbreviation())
			.secondsToStart(gameConfigs.get(serverInfo.getGameAbbreviation()).getSecondsToStart())
			.serverStartedAt(serverInfo.getServerStartTime())
			.serverPrice(getBillingAmount.execute(GetBillingAmount.Request.ofSlug(serverInfo.getSlug())))
			.build();
		presenter.accept(response);
	}

	public static class Request {
		private final UUID serverId;

		Request(UUID serverId) {
			this.serverId = serverId;
		}

		public static Request of(UUID serverId) {
			return new Request(serverId);
		}
	}
}
