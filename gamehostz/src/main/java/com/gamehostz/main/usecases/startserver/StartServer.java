package com.gamehostz.main.usecases.startserver;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.savegame.SaveGameRepository;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerProvider;
import com.gamehostz.main.rest.gameserver.responsemodel.ServerInfoResponse;
import com.gamehostz.main.usecases.getprovider.GetProvider;
import com.gamehostz.main.usecases.getramsettings.GetRamSettings;
import com.gamehostz.main.usecases.validateclientsettings.ValidateClientSettings;
import com.gamehostz.main.usecases.validateuserstartserver.ValidateUserStartServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Consumer;

@Component
public class StartServer {
	private final ValidateClientSettings validateClientSettings;
	private final ValidateUserStartServer validateUserStartServer;
	private final ServerProvider provider;
	private final GetRamSettings getRamSettings;
	SaveGameRepository saveGameRepository;
	private Logger log = LoggerFactory.getLogger(StartServer.class);

	public StartServer(ValidateUserStartServer validateUserStartServer,
	                   GetProvider provider,
	                   SaveGameRepository saveGameRepository) {
		this.validateUserStartServer = validateUserStartServer;
		this.validateClientSettings = new ValidateClientSettings();
		this.getRamSettings = new GetRamSettings();
		this.saveGameRepository = saveGameRepository;
		this.provider = provider.execute();
	}

	public void execute(Request request, Consumer<ServerInfoResponse> presenter) {
		log.info("starting a new {} server", request.gameName);

		validateClientSettings.validateGame(request.clientConfig, request.gameName);
		validateClientSettings.validateRegion(request.region);

		String ramAmountSlug = this.getRamSettings.execute(GetRamSettings.Request.of(request.gameName, request.clientConfig)).getRamAmount();
		validateUserStartServer.execute(ValidateUserStartServer.Request.of(request.userId, ramAmountSlug));

		ServerInfo serverInfo = provider.startServer(
			request.region,
			ramAmountSlug,
			request.gameName,
			request.clientConfig,
			request.userId,
			null,
			null);

		ServerInfoResponse response = ServerInfoResponse.Builder.aServerInfoResponse()
			.serverId(serverInfo.getId())
			.build();
		presenter.accept(response);
	}

	public static class Request {
		private final ClientConfig clientConfig;
		private final String gameName;
		private final String region;
		private final UUID userId;

		Request(ClientConfig clientConfig, String gameName, String region, UUID userId) {
			this.clientConfig = clientConfig;
			this.gameName = gameName;
			this.region = region;
			this.userId = userId;
		}

		public static Request of(ClientConfig clientConfig, String gameName, String region, UUID userId) {
			return new Request(clientConfig, gameName, region, userId);
		}
	}
}
