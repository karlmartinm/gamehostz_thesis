package com.gamehostz.main.usecases.lambda.getsaves;

import com.gamehostz.main.domain.savegame.SaveGameState;
import java.util.UUID;
import org.hibernate.annotations.Type;

public class LambdaSaveGame {

	@Type(type = "pg-uuid")
	private UUID serverId;
	private UUID saveGameId;
	private SaveGameState saveGameState;

	public UUID getServerId() {
		return serverId;
	}

	public void setServerId(UUID serverId) {
		this.serverId = serverId;
	}

	public UUID getSaveGameId() {
		return saveGameId;
	}

	public void setSaveGameId(UUID saveGameId) {
		this.saveGameId = saveGameId;
	}

	public SaveGameState getSaveGameState() {
		return saveGameState;
	}

	public void setSaveGameState(SaveGameState saveGameState) {
		this.saveGameState = saveGameState;
	}
}
