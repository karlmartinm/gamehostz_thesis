package com.gamehostz.main.usecases.getsavegames;

import com.gamehostz.main.domain.savegame.SaveGameState;
import org.hibernate.annotations.Type;

import java.time.Instant;
import java.util.UUID;

public class SaveGameDTO {

	@Type(type = "pg-uuid")
	private UUID serverId;
	private UUID saveGameId;
	private UUID createdBy;
	private String gameName;
	private Instant createdAt;
	private SaveGameState saveGameState;
	private String regionCode;
	private long saveSize;

	public SaveGameDTO(UUID serverId, UUID saveGameId, String gameName, Instant createdAt, SaveGameState saveGameState,
		String regionCode, long saveSize, UUID createdBy) {
		this.serverId = serverId;
		this.saveGameId = saveGameId;
		this.gameName = gameName;
		this.createdAt = createdAt;
		this.saveGameState = saveGameState;
		this.regionCode = regionCode;
		this.saveSize = saveSize;
		this.createdBy = createdBy;
	}

	public UUID getServerId() {
		return serverId;
	}

	public void setServerId(UUID serverId) {
		this.serverId = serverId;
	}

	public UUID getSaveGameId() {
		return saveGameId;
	}

	public void setSaveGameId(UUID saveGameId) {
		this.saveGameId = saveGameId;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public Instant getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Instant createdAt) {
		this.createdAt = createdAt;
	}

	public SaveGameState getSaveGameState() {
		return saveGameState;
	}

	public void setSaveGameState(SaveGameState saveGameState) {
		this.saveGameState = saveGameState;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public long getSaveSize() {
		return saveSize;
	}

	public void setSaveSize(long saveSize) {
		this.saveSize = saveSize;
	}

	public UUID getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}
}
