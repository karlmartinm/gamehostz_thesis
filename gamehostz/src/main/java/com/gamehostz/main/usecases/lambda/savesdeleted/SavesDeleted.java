package com.gamehostz.main.usecases.lambda.savesdeleted;

import com.gamehostz.main.domain.savegame.SaveGame;
import com.gamehostz.main.domain.savegame.SaveGameRepository;
import com.gamehostz.main.domain.savegame.SaveGameState;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.usecases.lambda.getsaves.LambdaSaveGame;
import java.time.Clock;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.stereotype.Component;

@Component
public class SavesDeleted {

	private final ServerInfoRepository serverInfoRepository;
	private final SaveGameRepository saveGameRepository;

	public SavesDeleted(ServerInfoRepository serverInfoRepository,
		SaveGameRepository saveGameRepository) {
		this.serverInfoRepository = serverInfoRepository;
		this.saveGameRepository = saveGameRepository;
	}

	public void execute(SavesDeleted.Request request) {
		List<UUID> saveGameIds = request.deletedSaveGames.stream().map(LambdaSaveGame::getSaveGameId).collect(Collectors.toList());
		List<UUID> serverIds = request.deletedSaveGames.stream().map(LambdaSaveGame::getServerId).collect(Collectors.toList());

		Iterable<SaveGame> saveGameList = () ->
			StreamSupport.stream(saveGameRepository.findAllById(saveGameIds).spliterator(), false)
				.peek(saveGame -> saveGame.setState(SaveGameState.DELETED))
				.iterator();
		Iterable<ServerInfo> serverIdList = () ->
			StreamSupport.stream(serverInfoRepository.findAllById(serverIds).spliterator(), false)
				.peek(saveGame -> saveGame.setSaveDeleted(true))
				.iterator();

		saveGameRepository.saveAll(saveGameList);
		serverInfoRepository.saveAll(serverIdList);
	}

	public static class Request {

		private final List<LambdaSaveGame> deletedSaveGames;

		Request(List<LambdaSaveGame> deletedSaveGames) {
			this.deletedSaveGames = deletedSaveGames;
		}

		public static SavesDeleted.Request of(List<LambdaSaveGame> deletedSaveGames) {
			return new SavesDeleted.Request(deletedSaveGames);
		}
	}
}
