package com.gamehostz.main.usecases.savedelete;

import com.gamehostz.main.domain.event.Event;
import com.gamehostz.main.domain.event.EventRepository;
import com.gamehostz.main.domain.event.EventSource;
import com.gamehostz.main.domain.event.EventType;
import com.gamehostz.main.domain.savegame.*;
import com.gamehostz.main.usecases.loadserver.LoadServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.util.UUID;

@Component
public class SaveDelete {

	private final Logger log = LoggerFactory.getLogger(LoadServer.class);
	private final SaveGameRepository saveGameRepository;
	private final EventRepository eventRepository;
	private final Clock clock;

	public SaveDelete(SaveGameRepository saveGameRepository, EventRepository eventRepository, Clock clock) {
		this.saveGameRepository = saveGameRepository;
		this.eventRepository = eventRepository;
		this.clock = clock;
	}

	@Transactional
	public void execute(Request request) {
		log.info("Deleting save game with id {}", request.saveGameId);
		saveGameRepository.findUserSaveGames(request.saveGameId, request.userId)
			.stream()
			.findAny()
			.orElseThrow(() -> new SaveGameException(SaveGameError.DELETE_REQUEST_FAILED)); // Trying to delete someone else's save

		// TODO kinda same query as previous but needed for SaveGame obj save(?)
		SaveGame saveGame = saveGameRepository
			.getOne(request.saveGameId)
			.orElseThrow(() -> new SaveGameException(SaveGameError.DELETE_REQUEST_FAILED));

		if (!SaveGameState.ACTIVE.equals(saveGame.getState())) {
			throw new SaveGameException(SaveGameError.DELETE_REQUEST_FAILED);
		}

		saveGame.setState(SaveGameState.DELETE_REQUEST);
		saveGameRepository.save(saveGame);

		createEvent(request);
	}

	public static class Request {

		private final UUID saveGameId;
		private final UUID userId;

		Request(UUID saveGameId, UUID userId) {
			this.saveGameId = saveGameId;
			this.userId = userId;
		}

		public static SaveDelete.Request of(UUID saveGameId, UUID userId) {
			return new SaveDelete.Request(saveGameId, userId);
		}
	}

	private void createEvent(Request request) {
		Event event = Event.EventBuilder.anEvent(clock)
			.id(UUID.randomUUID())
			.userId(request.userId)
			.serverId(null)
			.source(EventSource.PORTAL)
			.type(EventType.SAVE_DELETE)
			.additionalJson(new Event.SaveDeleteTypeJson(request.saveGameId))
			.build();
		eventRepository.save(event);
	}
}
