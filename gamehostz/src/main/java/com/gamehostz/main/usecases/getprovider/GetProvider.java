package com.gamehostz.main.usecases.getprovider;

import com.gamehostz.main.domain.serverprovider.ServerProvider;
import com.gamehostz.main.domain.serverprovider.digitalocean.DigitalOceanProvider;
import com.gamehostz.main.domain.serverprovider.docker.DockerProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GetProvider {

	private final ServerProvider provider;

	public GetProvider(@Value("${provider.env}") String environment,
	                   DigitalOceanProvider digitalOceanProvider,
	                   DockerProvider dockerProvider) {
		this.provider = environment.equals("local") ? dockerProvider : digitalOceanProvider; // TODO add capability for other vps providers
	}

	public ServerProvider execute() {
		return this.provider;
	}
}
