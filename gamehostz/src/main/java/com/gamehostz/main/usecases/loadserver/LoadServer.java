package com.gamehostz.main.usecases.loadserver;

import com.gamehostz.main.domain.savegame.*;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.domain.serverprovider.ServerProvider;
import com.gamehostz.main.rest.gameserver.responsemodel.ServerInfoResponse;
import com.gamehostz.main.usecases.getprovider.GetProvider;
import com.gamehostz.main.usecases.validateclientsettings.ValidateClientSettings;
import com.gamehostz.main.usecases.validateuserstartserver.ValidateUserStartServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.UUID;
import java.util.function.Consumer;

@Component
public class LoadServer {

	private final ValidateClientSettings validateClientSettings;
	private final ValidateUserStartServer validateUserStartServer;
	private final ServerProvider provider;

	private final Logger log = LoggerFactory.getLogger(LoadServer.class);
	private final SaveGameRepository saveGameRepository;
	private final ServerInfoRepository serverInfoRepository;

	public LoadServer(ValidateUserStartServer validateUserStartServer,
	                  GetProvider provider,
	                  SaveGameRepository saveGameRepository,
	                  ServerInfoRepository serverInfoRepository) {
		this.validateUserStartServer = validateUserStartServer;
		this.validateClientSettings = new ValidateClientSettings();
		this.saveGameRepository = saveGameRepository;
		this.serverInfoRepository = serverInfoRepository;
		this.provider = provider.execute();
	}

	@Transactional
	public void execute(Request request, Consumer<ServerInfoResponse> presenter) {
		log.info("loading a new {} server", request.saveGameId);

		ServerInfo previousServerInfo = serverInfoRepository
			.findFirstBySaveGameIdOrderByClosedAtDesc(request.saveGameId)
			.orElseThrow(() -> new SaveGameException(SaveGameError.LOAD_FAILED));

		SaveGame saveGame = saveGameRepository
			.getOne(previousServerInfo.getSaveGameId())
			.orElseThrow(() -> new SaveGameException(SaveGameError.LOAD_FAILED));

		if (SaveGameState.SAVING.equals(saveGame.getState())) {
			throw new SaveGameException(SaveGameError.LOAD_FAILED);
		}

		String ramAmountSlug = previousServerInfo.getSlug();

		validateClientSettings.validateRegion(previousServerInfo.getRegionCode());
		validateUserStartServer.execute(ValidateUserStartServer.Request.of(request.userId, ramAmountSlug));

		ServerInfo serverInfo = provider
			.startServer(previousServerInfo.getRegionCode(),
				ramAmountSlug,
				previousServerInfo.getGameAbbreviation(),
			             null, // null because save files should already contain (updated) config
			             request.userId,
			             previousServerInfo.getId(),
			             previousServerInfo.getSaveGameId());

		saveGame.setState(SaveGameState.RUNNING);
		saveGameRepository.save(saveGame);

		ServerInfoResponse response = ServerInfoResponse.Builder.aServerInfoResponse()
			.serverId(serverInfo.getId())
			.build();
		presenter.accept(response);
	}

	public static class Request {
		private final UUID saveGameId;
		private final UUID userId;

		Request(UUID saveGameId, UUID userId) {
			this.saveGameId = saveGameId;
			this.userId = userId;
		}

		public static Request of(UUID saveGameId, UUID userId) {
			return new Request(saveGameId, userId);
		}
	}
}

