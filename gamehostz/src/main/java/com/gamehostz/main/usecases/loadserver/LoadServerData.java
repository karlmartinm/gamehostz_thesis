package com.gamehostz.main.usecases.loadserver;

import com.gamehostz.main.domain.savegame.SaveGameLocation;
import com.gamehostz.main.domain.serverprovider.ServerProviderName;

import java.util.UUID;

public class LoadServerData {

	private UUID saveGameId;
	private UUID serverInfoId;
	private ServerProviderName serverProvider;
	private SaveGameLocation saveGameLocation;
	private String gameAbbreviation;
	private String slug;
	private String regionCode;

	public LoadServerData(UUID saveGameId,
	                      UUID serverInfoId,
	                      ServerProviderName serverProvider,
	                      SaveGameLocation saveGameLocation,
	                      String gameAbbreviation,
	                      String slug,
	                      String regionCode) {
		this.saveGameId = saveGameId;
		this.serverInfoId = serverInfoId;
		this.serverProvider = serverProvider;
		this.saveGameLocation = saveGameLocation;
		this.gameAbbreviation = gameAbbreviation;
		this.slug = slug;
		this.regionCode = regionCode;
	}

	public UUID getSaveGameId() {
		return saveGameId;
	}

	public void setSaveGameId(UUID saveGameId) {
		this.saveGameId = saveGameId;
	}

	public UUID getServerInfoId() {
		return serverInfoId;
	}

	public void setServerInfoId(UUID serverInfoId) {
		this.serverInfoId = serverInfoId;
	}

	public ServerProviderName getServerProvider() {
		return serverProvider;
	}

	public void setServerProvider(ServerProviderName serverProvider) {
		this.serverProvider = serverProvider;
	}

	public SaveGameLocation getSaveGameLocation() {
		return saveGameLocation;
	}

	public void setSaveGameLocation(SaveGameLocation saveGameLocation) {
		this.saveGameLocation = saveGameLocation;
	}

	public String getGameAbbreviation() {
		return gameAbbreviation;
	}

	public void setGameAbbreviation(String gameAbbreviation) {
		this.gameAbbreviation = gameAbbreviation;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	@Override
	public String toString() {
		return "LoadServerData{" +
			"saveGameId=" + saveGameId +
			", serverInfoId=" + serverInfoId +
			", serverProvider=" + serverProvider +
			", saveGameLocation='" + saveGameLocation + '\'' +
			", gameAbbreviation='" + gameAbbreviation + '\'' +
			", slug='" + slug + '\'' +
			", region='" + regionCode + '\'' +
			'}';
	}
}
