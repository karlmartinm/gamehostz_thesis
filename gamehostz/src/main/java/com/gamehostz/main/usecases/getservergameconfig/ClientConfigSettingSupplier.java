package com.gamehostz.main.usecases.getservergameconfig;

import com.gamehostz.main.domain.client.ClientConfigSetting;
import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.gameserver.GameConfigFile;
import com.gamehostz.main.helpers.Utils;
import com.gamehostz.main.usecases.getservergameconfig.createclientconfigsettings.CreateClientConfigSetting;
import com.gamehostz.main.usecases.getservergameconfig.createclientconfigsettings.CreateUEClientConfigSetting;

import java.util.*;
import java.util.function.Supplier;

public class ClientConfigSettingSupplier {

	// Just in case explicitly put csgo and mc in this map although they currently don't need any extra fields for SM
	private static final Map<String, Supplier<CreateClientConfigSetting>> CLIENT_CONFIG_SETTING_SUPPLIER = Map.of(
		"csgo_server_properties_format", CreateStandardClientConfig::new,
		"mc_server_properties_format", CreateStandardClientConfig::new,
		"ue_ini_format", CreateUEClientConfigSetting::new
		);

	public static ClientConfigSetting supplyClientConfigSetting(GameConfig gameConfig, ClientConfigSetting setting) {

		GameConfigFile gameConfigFile = Utils.getGameConfigFileFromSettingName(gameConfig, setting.getName());
		String format = gameConfigFile.getFormat();

		// If format is not in the supplier map or there is no format then just pass setting like they are in DB.
		Supplier<CreateClientConfigSetting> clientConfigSettingSupplier =
			format == null ? CreateStandardClientConfig::new : CLIENT_CONFIG_SETTING_SUPPLIER.get(format);

		return Optional.of(clientConfigSettingSupplier.get()).get().create(gameConfigFile, setting);
	}

	// Class for when extra values besides name and value are not needed to pass on to SM
	private static class CreateStandardClientConfig implements CreateClientConfigSetting {

		public CreateStandardClientConfig() {
		}

		@Override
		public ClientConfigSetting create(GameConfigFile gameConfigFile, ClientConfigSetting setting) {
			return setting;
		}
	}
}
