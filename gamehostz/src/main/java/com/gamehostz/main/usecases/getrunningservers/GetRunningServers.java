package com.gamehostz.main.usecases.getrunningservers;

import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.rest.gameserver.responsemodel.ServerInfoResponse;
import com.gamehostz.main.usecases.getserverinfo.GetServerInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.gamehostz.main.MainApplication.gameConfigs;

@Component
public class GetRunningServers {
	private Logger log = LoggerFactory.getLogger(GetServerInfo.class);

	private final ServerInfoRepository serverInfoRepository;

	public GetRunningServers(ServerInfoRepository serverInfoRepository) {
		this.serverInfoRepository = serverInfoRepository;
	}

	public List<ServerInfo> execute() {
		log.info("getting running servers");
		return serverInfoRepository.findAllByClosedAtIsNull();
	}

	public List<ServerInfoResponse> execute(UUID userId) {
		log.info("getting running servers of user {}", userId);
		List<ServerInfo> serverInfos = serverInfoRepository.findAllByCreatedByAndClosedAtIsNull(userId);
		return serverInfos.stream().map(serverInfo -> {
			String port = String.valueOf(gameConfigs.get(serverInfo.getGameAbbreviation()).getPrimaryPort());
			return ServerInfoResponse.Builder.aServerInfoResponse()
				.serverId(serverInfo.getId())
				.serverAddress(serverInfo.getAddress())
				.serverPort(port)
				.clientConfig(serverInfo.getGameConfig())
				.gameAbbreviation(serverInfo.getGameAbbreviation())
				.build();
		})
		.collect(Collectors.toList());
	}
}
