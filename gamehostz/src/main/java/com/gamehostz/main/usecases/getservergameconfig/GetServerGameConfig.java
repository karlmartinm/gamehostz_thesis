package com.gamehostz.main.usecases.getservergameconfig;

import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerProvider;
import com.gamehostz.main.domain.serverprovider.digitalocean.DigitalOceanProvider;
import com.gamehostz.main.domain.serverprovider.docker.DockerProvider;
import com.gamehostz.main.rest.gameserver.responsemodel.ServerGameConfigResponse;
import com.gamehostz.main.usecases.getserverinfo.GetServerInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Consumer;

@Component
public class GetServerGameConfig {
	private final ServerProvider provider;
	private final DigitalOceanProvider digitalOceanProvider;
	private final DockerProvider dockerProvider;
	private Logger log = LoggerFactory.getLogger(GetServerInfo.class);

	public GetServerGameConfig(@Value("${provider.env}") String environment, DigitalOceanProvider digitalOceanProvider, DockerProvider dockerProvider) {
		this.digitalOceanProvider = digitalOceanProvider;
		this.dockerProvider = dockerProvider;
		this.provider = environment.equals("local") ? this.dockerProvider : this.digitalOceanProvider;
	}

	public void execute(Request request, Consumer<ServerGameConfigResponse> presenter) {
		log.info("getting server game config {} server", request.serverId);

		ServerInfo serverInfo = provider.getServerInfo(request.serverId);

		ServerGameConfigResponse response = ServerGameConfigResponse.newBuilder()
			.serverId(serverInfo.getId())
			.gameConfig(serverInfo.getGameConfig())
			.gameName(serverInfo.getGameAbbreviation())
			.serverEmptyCloseTime(120L) // TODO Hardcoded for now. NOT USED FROM HERE?
			.build();
		presenter.accept(response);
	}

	public static class Request {
		private final UUID serverId;

		Request(UUID serverId) {
			this.serverId = serverId;
		}

		public static Request of(UUID serverId) {
			return new Request(serverId);
		}
	}
}
