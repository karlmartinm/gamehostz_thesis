package com.gamehostz.main.usecases.generateuserdata;

import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.serverprovider.ServerProviderName;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@PropertySource("classpath:userdata.properties")
public class GenerateUserData {
	@Value("${digitalocean.spaceKey}")
	private String spaceKey;
	@Value("${digitalocean.spaceSecret}")
	private String spaceSecret;
	@Value("${digitalocean.appEnv}")
	private String appEnv;
	@Value("${digitalocean.javaIp}")
	private String javaIp;
	@Value("${digitalocean.javaPort}")
	private String javaPort;
	@Value("${digitalocean.nodeProxyIp}")
	private String nodeProxyIp;
	@Value("${digitalocean.graylogIp}")
	private String graylogIp;
	@Value("${digitalocean.graylogPort}")
	private String graylogPort;

	private String spaceHostBase = "fra1.digitaloceanspaces.com";
	private String spaceHostBucket = "gh-sm.fra1.digitaloceanspaces.com";
	private String spaceSaveGameHostBucket = "gamesave.fra1.digitaloceanspaces.com";
	//private String javaAuthKey = serviceAuthKey.toString(); // TODO java app auth key, generated on every startup (?)

	private String SMPort = "3000";
	private String SMPm2Name = "sm";

	private String SMSPort = "3005";
	private String SMSPm2Name = "sms";
	@Value("${digitalocean.auth}")
	private String DOApiKey;
	@Value("${provider.env}")
	private String providerEnv; // TODO enum?

	private StringBuilder sb;

	public GenerateUserData() {
	}

	public String execute(UUID serverId, ServerProviderName provider, GameConfig gameConfig, UUID saveFileId) {
		appEnv = "prod"; // ???

		generateShScript(serverId, provider, gameConfig, saveFileId);
		return this.sb.toString();
	}

	private void generateShScript(UUID serverId, ServerProviderName provider, GameConfig gameConfig, UUID saveFileId) {
		shScriptStart();
		createClientUser();

		smS3Config(saveFileId);
		smsS3Config();
		ufwFirewallSetup(gameConfig);

		smsFilesFetch();
		smFilesFetch();
		saveFilesFetch(saveFileId);
		smsEnvVariables(serverId, provider, gameConfig);

		initializePm2AndStartSms();
		smEnvVariables(serverId, gameConfig, saveFileId);
		initializeSm(gameConfig, saveFileId);
		initializeFilebrowser(gameConfig);
		shScriptEnd(gameConfig);
	}

	private void initializeFilebrowser(GameConfig gameConfig) {
		this.sb.append("su - client -c \"mkdir /home/client/filebrowser\"").append("\n")
			.append("curl -fsSL https://filebrowser.org/get.sh | bash").append("\n")
			.append(
				"su - client -c \"cd /home/client/filebrowser && filebrowser config init --port=8888 --address=0.0.0.0 --signup=0 --viewMode=list --viewMode=list")
			// TODO test proxy auth
			.append(" --branding.name='Gamehostz' --branding.disableExternal=1 --log=/home/client/filebrowser/log.log --auth.method=noauth")
			.append(" --root=/home/client/sm/").append(gameConfig.getFolder()).append("/serverfiles/\"").append("\n")
			.append("su - client -c \"cd /home/client/filebrowser && filebrowser config set --shell='bash -c' --commands='unzip'\"")
			.append("\n")
			.append("su - client -c \"cd /home/client/filebrowser && filebrowser users add client client\"").append("\n")
			// --commands='unzip' might not be secure in terms of SM source code access
			.append(
				"su - client -c \"cd /home/client/filebrowser && filebrowser users update client --lockPassword=1 --perm.share=0 --commands='unzip' --scope='/home/client/sm/")
			.append(gameConfig.getFolder()).append("/serverfiles'\"").append("\n")
			.append(
				"su - client -c \"cd /home/client/filebrowser && tmux new-session -d -s 'filebrowser' 'filebrowser -d /home/client/filebrowser/filebrowser.db'\"")
			.append("\n")
			.append("echo \"filebrowser done\"").append("\n");
	}

	private void shScriptStart() {
		this.sb = new StringBuilder("#!/usr/bin/env bash").append("\n")
			.append("echo \"userdata start\"").append("\n");
	}

	private void createClientUser() {
		this.sb.append("adduser --disabled-password --gecos \"\" client").append("\n");
	}

	private void initializePm2AndStartSms() {
		this.sb.append("pm2 startup").append("\n")
			.append("pm2 start /root/sms/index.js --name \"").append(SMSPm2Name).append("\"").append("\n");
	}

	private void initializeSm(GameConfig gameConfig, UUID saveFileId) {
		this.sb.append("mkdir /home/client/sm/").append(gameConfig.getFolder()).append("\n")

			.append("wget -O /home/client/sm/").append(gameConfig.getFolder())
			.append("/linuxgsm.sh https://linuxgsm.sh").append("\n")

			.append("chmod +x /home/client/sm/").append(gameConfig.getFolder()).append("/linuxgsm.sh").append("\n")

			.append("chown -R client:client /home/client").append("\n")

			.append("su - client -c \"cd /home/client/sm/").append(gameConfig.getFolder())
			.append(" && ./linuxgsm.sh ").append(gameConfig.getLgsmName()).append("\"").append("\n")

			.append("su - client -c \"/home/client/sm/").append(gameConfig.getFolder()).append("/")
			.append(gameConfig.getLgsmName()).append(" auto-install").append("\"\n");

		// TODO move to another method
		if (saveFileId != null) {
			this.sb.append("rm -rf /home/client/sm/").append(gameConfig.getFolder()).append("/serverfiles").append("\n")
				.append("unzip /home/client/save.zip -d \"/home/client/sm/").append(gameConfig.getFolder()).append("/serverfiles\"")
				.append("\n")
				.append("rm /home/client/save.zip").append("\n");
		}

		this.sb.append("chown -R client:client /home/client").append("\n")

			.append("su - client -c \"pm2 start /home/client/sm/index.js --name \"").append(SMPm2Name).append("\"\"")
			.append("\n");
	}

	private void shScriptEnd(GameConfig gameConfig) {
		this.sb.append("ls /home/client/sm/").append(gameConfig.getFolder()).append("\n")

			.append("echo \"userdata end\"").append("\n")
			.append("while true; do sleep 30; done;").append("\n")
			.append("echo \"while end\"").append("\n");
	}

	private void saveFilesFetch(UUID saveFileId) {
		if (saveFileId == null) {
			return;
		}
		this.sb.append("su - client -c \"s3cmd -c .s3cfg_sm get s3://gamesave/")
			.append(saveFileId)
			.append(".zip /home/client/save.zip --force\"")
			.append("\n")
			.append("rm /home/client/.s3cfg_save").append("\n");
	}

	private void smFilesFetch() {
		if (providerEnv.equals("local")) {
			this.sb.append("echo \"copying sm files, takes a while: (mv /root/build/sm /home/client/sm)\"").append("\n");
			this.sb.append("mv /root/build/sm /home/client/sm").append("\n");
			// this.sb.append("echo \"unzip /home/client/sm/insurgencysandstorm.zip -d /home/client/sm\"").append("\n");
			// this.sb.append("unzip -q /home/client/sm/insurgencysandstorm.zip -d /home/client/sm").append("\n");
			return;
		}
		this.sb.append("su - client -c \"s3cmd -c .s3cfg_sm get s3://gh-sm/sm.zip /home/client/sm.zip --force\"").append("\n")
			.append("rm /home/client/.s3cfg_sm").append("\n");

		this.sb.append("mkdir /home/client/sm").append("\n")
			.append("yes|tr y n|unzip -q /home/client/sm.zip -d /home/client/sm").append("\n")
			//			.append("rm /home/client/sm.zip").append("\n")
			.append("yes|tr y n|mv -i /home/client/sm/dist/* /home/client/sm").append("\n");
		//			.append("rm -rf /home/client/sm/dist").append("\n");
	}

	private void smsFilesFetch() {
		if (providerEnv.equals("local")) {
			//			this.sb.append("mv /root/build/js-dist/sms /root/sms").append("\n");
			return;
		}
		this.sb.append("s3cmd get s3://gh-sm/sms.zip /root/sms.zip").append("\n")
			.append("mkdir /root/sms").append("\n")
			.append("yes|tr y n|unzip -q /root/sms.zip -d /root/sms").append("\n")
			//			.append("rm /root/sms.zip").append("\n")
			.append("yes|tr y n|mv -i /root/sms/dist/* /root/sms").append("\n");
		//			.append("rm -rf /root/sms/dist").append("\n")
	}

	private void smEnvVariables(UUID serverId, GameConfig gameConfig, UUID saveFileId) {
		this.sb.append("printf 'APP_PORT=").append(SMPort)
			.append("\\nAPP_ENV=").append(appEnv)
			.append("\\nAPP_UUID=").append(serverId)
			.append("\\nAPP_STORAGE_PATH=").append("/storage")
			.append("\\nAPP_BASE_PATH=").append("/home/client/sm")
			.append("\\nAPP_SUPPORTS_SAVE=").append(gameConfig.getSupportsSave())
			.append("\\nAPP_IS_LOAD_GAME=").append((saveFileId != null) ? "true" : "false")
			.append("\\nJAVA_HOST=").append(javaIp)
			.append("\\nJAVA_PORT=").append(javaPort)
			//			.append("\\nJAVA_AUTHKEY=").append(javaAuthKey)
			.append("\\nGL_HOST=").append(graylogIp)
			.append("\\nGL_PORT=").append(graylogPort)
			.append("\\nGL_HOSTNAME=").append("sm")
			.append("' >> /home/client/sm/.env").append("\n");
	}

	private void smsEnvVariables(UUID serverId, ServerProviderName provider, GameConfig gameConfig) {
		this.sb.append("printf 'APP_PORT=").append(SMSPort)
			.append("\\nAPP_ENV=").append(appEnv)
			.append("\\nAPP_UUID=").append(serverId)
			.append("\\nAPP_PROVIDER=").append(provider)
			.append("\\nAPP_PROVIDER_API_KEY=").append(DOApiKey) // TODO check provider first and then set api key
			.append("\\nAPP_SUPPORTS_SAVE=").append(gameConfig.getSupportsSave())
			.append("\\nJAVA_HOST=").append(javaIp)
			.append("\\nJAVA_PORT=").append(javaPort)
			//			.append("\\nJAVA_AUTHKEY=").append(javaAuthKey)
			.append("\\nGL_HOST=").append(graylogIp)
			.append("\\nGL_PORT=").append(graylogPort)
			.append("\\nGL_HOSTNAME=").append("sms")
			.append("\\nS3_KEY=").append(spaceKey)
			.append("\\nS3_SECRET=").append(spaceSecret)
			.append("\\nS3_ENDPOINT=").append(spaceHostBase)
			.append("\\nS3_BUCKET=").append("gamesave")
			.append("' >> /root/sms/.env").append("\n");
	}

	private void ufwFirewallSetup(GameConfig gameConfig) {
		if (providerEnv.equals("local")) {
			return;
		}
		for (Integer port : gameConfig.getPorts()) {
			this.sb.append("ufw allow ").append(port).append("\n");
		}
		if (!appEnv.equals("dev"))
			this.sb.append("ufw allow from ").append(javaIp).append(" to any port ").append(SMPort)
				.append("\n");

		if (!appEnv.equals("dev"))
			this.sb.append("ufw allow from ").append(nodeProxyIp).append(" to any port ").append(SMPort)
				.append("\n");

		this.sb.append("ufw reload").append("\n");
	}

	private void smsS3Config() {
		this.sb.append("printf '[default]\\naccess_key = ").append(spaceKey)
			.append("\\nsecret_key = ").append(spaceSecret)
			.append("\\nhost_base = ").append(spaceHostBase)
			.append("\\nhost_bucket = ").append(spaceHostBucket).append("' >> /root/.s3cfg").append("\n");
	}

	private void smS3Config(UUID saveFileId) {
		this.sb
			//sm
			.append("printf '[default]\\naccess_key = ").append(spaceKey)
			.append("\\nsecret_key = ").append(spaceSecret)
			.append("\\nhost_base = ").append(spaceHostBase)
			.append("\\nhost_bucket = ").append(spaceHostBucket).append("' >> /home/client/.s3cfg_sm").append("\n");

		if (saveFileId != null) {
			this.sb.append("printf '[default]\\naccess_key = ").append(spaceKey)
				.append("\\nsecret_key = ").append(spaceSecret)
				.append("\\nhost_base = ").append(spaceHostBase)
				.append("\\nhost_bucket = ").append(spaceSaveGameHostBucket).append("' >> /home/client/.s3cfg_save").append("\n");
		}
	}
}
