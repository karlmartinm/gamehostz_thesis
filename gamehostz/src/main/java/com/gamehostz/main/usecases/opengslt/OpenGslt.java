package com.gamehostz.main.usecases.opengslt;


import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.steam.GsltToken;
import com.gamehostz.main.domain.steam.GsltTokenRepository;
import org.springframework.stereotype.Component;

@Component
public class OpenGslt {
	private final GsltTokenRepository gsltTokenRepository;

	public OpenGslt(GsltTokenRepository gsltTokenRepository) {
		this.gsltTokenRepository = gsltTokenRepository;
	}

	public void execute(Request request) {
		gsltTokenRepository.findById(request.server.getId())
			.ifPresent(this::openTokenAndSave);
	}

	private void openTokenAndSave(GsltToken gsltToken) {
		gsltToken.openToken();
		gsltTokenRepository.save(gsltToken);
	}

	public static class Request {
		private final ServerInfo server;

		Request(ServerInfo server) {
			this.server = server;
		}

		public static Request of(ServerInfo server) {
			return new Request(server);
		}
	}
}
