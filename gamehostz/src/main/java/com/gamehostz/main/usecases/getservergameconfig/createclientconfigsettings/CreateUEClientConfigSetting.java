package com.gamehostz.main.usecases.getservergameconfig.createclientconfigsettings;

import com.gamehostz.main.domain.client.ClientConfigSetting;
import com.gamehostz.main.domain.gameserver.GameConfigFile;
import com.gamehostz.main.usecases.getservergameconfig.clientconfigsettings.UEClientConfigSetting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateUEClientConfigSetting implements CreateClientConfigSetting {

	public CreateUEClientConfigSetting() {
	}

	public ClientConfigSetting create(GameConfigFile gameConfigFile, ClientConfigSetting setting) {

		Map<String, List<List<String>>> locationMap = new HashMap<>();
		gameConfigFile.getGameConfigFileSetting().forEach(s -> locationMap.put(s.getName(), s.getLocation()));

		UEClientConfigSetting ueClientConfigSetting = new UEClientConfigSetting();
		ueClientConfigSetting.setName(setting.getName());
		ueClientConfigSetting.setValue(setting.getValue());
		ueClientConfigSetting.setLocation(locationMap.get(setting.getName()));
		ueClientConfigSetting.setFile(gameConfigFile.getFile());
		ueClientConfigSetting.setDirectory(gameConfigFile.getDirectory());
		ueClientConfigSetting.setFormat(gameConfigFile.getFormat());
		return ueClientConfigSetting;
	}
}
