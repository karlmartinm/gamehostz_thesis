package com.gamehostz.main.usecases.closegslt;

import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.domain.serverprovider.ServerProviderException;
import com.gamehostz.main.domain.steam.GsltException;
import com.gamehostz.main.domain.steam.GsltToken;
import com.gamehostz.main.domain.steam.GsltTokenRepository;
import com.gamehostz.main.rest.gslt.responsemodel.GsltResponse;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import java.util.function.Consumer;

import static com.gamehostz.main.domain.serverprovider.ProviderError.NO_RUNNING_SERVER;
import static com.gamehostz.main.domain.steam.GsltError.OUT_OF_TOKENS;

@Component
public class CloseGslt {
	private final GsltTokenRepository gsltTokenRepository;
	private final ServerInfoRepository serverInfoRepository;

	public CloseGslt(GsltTokenRepository gsltTokenRepository, ServerInfoRepository serverInfoRepository) {
		this.gsltTokenRepository = gsltTokenRepository;
		this.serverInfoRepository = serverInfoRepository;
	}

	// update from https://steamcommunity.com/dev/managegameservers
	// needed to run csgo servers publiclypublic
	@Transactional
	public void execute(Request request, Consumer<GsltResponse> presenter) {
		GsltToken gslt = getGsltToken();
		ServerInfo server = getServerInfo(request);

		server.setGslt(gslt);
		serverInfoRepository.save(server);
		gslt.closeToken();
		gsltTokenRepository.save(gslt);

		GsltResponse response = GsltResponse.of(gslt.getToken());
		presenter.accept(response);
	}

	private GsltToken getGsltToken() {
		return gsltTokenRepository.findAllNotInUse().stream()
			.findFirst()
			.orElseThrow(() -> new GsltException(OUT_OF_TOKENS));
	}

	private ServerInfo getServerInfo(Request request) {
		return serverInfoRepository.findOneByIdAndClosedAtIsNull(request.serverId)
			.orElseThrow(() -> new ServerProviderException(NO_RUNNING_SERVER));
	}

	public static class Request {
		private final UUID serverId;

		Request(UUID serverId) {
			this.serverId = serverId;
		}

		public static Request of(UUID serverId) {
			return new Request(serverId);
		}
	}
}
