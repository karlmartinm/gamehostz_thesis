package com.gamehostz.main.usecases.createcustomer;

import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.transactions.CurrencyAccount;
import com.gamehostz.main.domain.transactions.CurrencyType;
import com.gamehostz.main.rest.customer.responsemodel.CustomerResponse;
import com.gamehostz.main.usecases.addpoints.AddPoints;
import com.gamehostz.main.usecases.createcurrencyaccount.CreateCurrencyAccount;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

@Component
@Transactional
public class CreateCustomer {
	private final CustomerRepository customerRepository;
	private final AddPoints addPoints;
	private BigDecimal POINTS_TO_ADD = BigDecimal.valueOf(150);

	public CreateCustomer(CustomerRepository customerRepository, AddPoints addPoints) {
		this.customerRepository = customerRepository;
		this.addPoints = addPoints;
	}

	public void execute(Request request, Consumer<CustomerResponse> presenter) {
		if (customerRepository.findByAuthId(request.authId).isPresent()) {
			presenter.accept(CustomerResponse.Builder.aCustomerResponse().newCustomer(false).pointsAdded(BigDecimal.ZERO).build());
		} else {
			customerRepository.findByAuthId(request.authId).orElseGet(() -> createCustomer(request));
			presenter.accept(CustomerResponse.Builder.aCustomerResponse().newCustomer(true).pointsAdded(POINTS_TO_ADD).build());
		}
	}

	private Customer createCustomer(Request request) {
		Set<CurrencyAccount> currencyAccounts = createCurrencyAccounts();
		Customer customer = saveCustomer(request, currencyAccounts);
		addPoints.execute(AddPoints.Request.of(request.authId, POINTS_TO_ADD, CurrencyType.POINT));
		return customer;
	}

	private Customer saveCustomer(Request request, Set<CurrencyAccount> currencyAccounts) {
		Customer customer = Customer.CustomerBuilder
			.aCustomer()
			.withAuthId(request.authId)
			.withCurrencyAccounts(currencyAccounts)
			.build();
		this.customerRepository.save(customer);
		return customer;
	}

	private Set<CurrencyAccount> createCurrencyAccounts() {
		CreateCurrencyAccount createCurrencyAccount = new CreateCurrencyAccount();
		Set<CurrencyAccount> currencyAccounts = new HashSet<>();
		for (CurrencyType value : CurrencyType.values()) {
			currencyAccounts.add(createCurrencyAccount.execute(CreateCurrencyAccount.Request.of(value)));
		}
		return currencyAccounts;
	}

	public static class Request {
		private final UUID authId;

		Request(UUID authId) {
			this.authId = authId;
		}

		public static Request of(UUID authId) {
			return new Request(authId);
		}
	}
}
