package com.gamehostz.main.usecases.getservergameconfig.clientconfigsettings;

import com.gamehostz.main.domain.client.ClientConfigSetting;
import lombok.Data;

import java.util.List;

@Data
public class UEClientConfigSetting extends ClientConfigSetting {

	private List<List<String>> location;
	private String file;
	private String directory;
	private String format;

	public UEClientConfigSetting() {
	}
}
