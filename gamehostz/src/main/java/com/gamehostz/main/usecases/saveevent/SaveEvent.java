package com.gamehostz.main.usecases.saveevent;

import com.gamehostz.main.domain.event.Event;
import com.gamehostz.main.domain.event.EventRepository;
import com.gamehostz.main.domain.event.EventSource;
import com.gamehostz.main.domain.event.EventType;
import com.gamehostz.main.rest.event.EventRequestModel;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.util.UUID;

@Component
public class SaveEvent {
	private final Clock clock;
	private final EventRepository eventRepository;

	public SaveEvent(Clock clock, EventRepository eventRepository) {
		this.clock = clock;
		this.eventRepository = eventRepository;
	}

	public void execute(Request request) {
		Event event = Event.EventBuilder.anEvent(clock)
			.id(UUID.randomUUID())
			.userId(request.userId)
			.serverId(request.serverId)
			.source(request.source)
			.type(request.type)
			.additionalJson(request.additionalJson)
			.build();
		eventRepository.save(event);
	}

	public static class Request {
		private final UUID userId;
		private final UUID serverId;
		private final EventSource source;
		private final EventType type;
		private final String additionalJson;

		Request(UUID userId, UUID serverId, EventSource source, EventType type, String additionalJson) {
			Validate.notNull(source, "source is required");
			Validate.notNull(type, "type is required");
			this.userId = userId;
			this.serverId = serverId;
			this.source = source;
			this.type = type;
			this.additionalJson = additionalJson;
		}

		public static Request of(EventRequestModel eventRequestModel) {
			return new Request(eventRequestModel.getUserId(),
			                   eventRequestModel.getServerId(),
			                   eventRequestModel.getSource(),
			                   eventRequestModel.getType(),
			                   eventRequestModel.getAdditionalJson());
		}
	}
}
