package com.gamehostz.main.usecases.getrunningservers;

import com.gamehostz.main.domain.serverprovider.ProviderError;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.domain.serverprovider.ServerProviderException;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class GetRunningServerOfUser {
	private final ServerInfoRepository serverInfoRepository;

	public GetRunningServerOfUser(ServerInfoRepository serverInfoRepository) {
		this.serverInfoRepository = serverInfoRepository;
	}

	public ServerInfo execute(Request request) {
		return serverInfoRepository.findOneByIdAndCreatedByAndClosedAtIsNull(request.serverId, request.userId)
			.orElseThrow(() -> new ServerProviderException(ProviderError.NO_RUNNING_SERVER));
	}

	public static class Request {
		private final UUID serverId;
		private final UUID userId;

		Request(UUID serverId, UUID userId) {
			this.serverId = serverId;
			this.userId = userId;
		}

		public static Request of(UUID serverId, UUID userId) {
			return new Request(serverId, userId);
		}
	}
}
