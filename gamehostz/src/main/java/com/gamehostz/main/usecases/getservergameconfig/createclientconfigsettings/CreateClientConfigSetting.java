package com.gamehostz.main.usecases.getservergameconfig.createclientconfigsettings;

import com.gamehostz.main.domain.client.ClientConfigSetting;
import com.gamehostz.main.domain.gameserver.GameConfigFile;

public interface CreateClientConfigSetting {
	ClientConfigSetting create(GameConfigFile gameConfigFile, ClientConfigSetting setting);
}
