package com.gamehostz.main.domain.steam;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class GsltToken {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String token;
	@Enumerated(EnumType.STRING)
	private SteamGame game;
	private String memo;
	private boolean inUse;

	private GsltToken(String token, SteamGame game, String memo) {
		this.token = token;
		this.game = game;
		this.memo = memo;
	}

	public GsltToken() {
	}

	public static GsltToken of(String token, SteamGame game, String memo) {
		return new GsltToken(token, game, memo);
	}

	public void closeToken() {
		this.inUse = true;
	}

	public void openToken() {
		this.inUse = false;
	}

	public String getToken() {
		return token;
	}
}
