package com.gamehostz.main.domain.serverprovider.digitalocean;

import com.myjeeva.digitalocean.exception.DigitalOceanException;
import com.myjeeva.digitalocean.exception.RequestUnsuccessfulException;
import com.myjeeva.digitalocean.pojo.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class DigitalOceanImagesJob {
	private Logger log = LoggerFactory.getLogger(DigitalOceanProvider.class);

	private final DigitalOceanProvider digitalOceanProvider;
	public static Map<String, Image> availableImages;

	public DigitalOceanImagesJob(DigitalOceanProvider digitalOceanProvider) {
		this.digitalOceanProvider = digitalOceanProvider;
	}

	@Scheduled(fixedDelay = 1000 * 60 * 60 * 24) // Every 24 hours and on startup
	public void run() throws IOException, RequestUnsuccessfulException, DigitalOceanException {
		log.info("Digital Ocean getting all available images");
		availableImages = digitalOceanProvider.getAvailableImages();
		log.info("Digital Ocean got {} available image(s) {}", availableImages.size(), availableImages);
	}
}
