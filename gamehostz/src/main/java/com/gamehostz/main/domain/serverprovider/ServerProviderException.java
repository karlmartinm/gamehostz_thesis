package com.gamehostz.main.domain.serverprovider;

import com.gamehostz.main.helpers.BadRequestAlertException;

public class ServerProviderException extends BadRequestAlertException {
	private static final long serialVersionUID = 1L;

	public ServerProviderException(ProviderError code) {
		super(code.name());
	}
}
