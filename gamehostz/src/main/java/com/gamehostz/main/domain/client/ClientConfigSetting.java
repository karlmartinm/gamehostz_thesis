package com.gamehostz.main.domain.client;

import lombok.Data;

@Data
public class ClientConfigSetting {
	private String name;
	private String value;

	public ClientConfigSetting() {
	}
}
