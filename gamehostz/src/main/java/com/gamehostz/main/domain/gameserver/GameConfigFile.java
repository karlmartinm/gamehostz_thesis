package com.gamehostz.main.domain.gameserver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameConfigFile {
	@JsonProperty("settings")
	private List<GameConfigFileSetting> gameConfigFileSetting;
	//	@JsonIgnore
	@JsonProperty(value = "file", access = JsonProperty.Access.WRITE_ONLY)
	private String file;
	private String name;
	//	@JsonIgnore
	@JsonProperty(value = "format", access = JsonProperty.Access.WRITE_ONLY)
	private String format;
	//	@JsonIgnore
	@JsonProperty(value = "directory", access = JsonProperty.Access.WRITE_ONLY)
	private String directory;
	@JsonProperty("ram")
	private List<RamSettings> ram;

	public GameConfigFile() {
	}

	public GameConfigFile(List<GameConfigFileSetting> gameConfigFileSetting, String file, String name, String format,
	                      String directory) {
		this.gameConfigFileSetting = gameConfigFileSetting;
		this.file = file;
		this.name = name;
		this.format = format;
		this.directory = directory;
	}

	public List<RamSettings> getRam() {
		return ram;
	}

	public void setRam(List<RamSettings> ram) {
		this.ram = ram;
	}

	public List<GameConfigFileSetting> getGameConfigFileSetting() {
		return gameConfigFileSetting;
	}

	public void setGameConfigFileSetting(List<GameConfigFileSetting> gameConfigFileSetting) {
		this.gameConfigFileSetting = gameConfigFileSetting;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	@Override
	public String toString() {
		return "GameConfigFile{" +
			"gameConfigFileSetting=" + gameConfigFileSetting +
			", file='" + file + '\'' +
			", name='" + name + '\'' +
			", format='" + format + '\'' +
			", directory='" + directory + '\'' +
			'}';
	}
}
