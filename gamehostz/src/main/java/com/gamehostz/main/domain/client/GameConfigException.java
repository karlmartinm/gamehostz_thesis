package com.gamehostz.main.domain.client;

import com.gamehostz.main.helpers.BadRequestAlertException;

public class GameConfigException extends BadRequestAlertException {
	private static final long serialVersionUID = 1L;

	public GameConfigException(GameConfigError code) {
		super(code.name());
	}
}
