package com.gamehostz.main.domain.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.time.Clock;
import java.time.Instant;
import java.util.UUID;

@Entity
@Data
public class Event {

	@Id
	private UUID id;
	private UUID userId;
	private UUID serverId; // TODO remove and use additionalJson instead
	@Enumerated(EnumType.STRING)
	private EventSource source;
	@Enumerated(EnumType.STRING)
	private EventType type;
	@Lob
	private String additionalJson;
	@Column(nullable = false)
	private Instant addedTime;

	public Event() {
	}

	public Event(Clock clock) {
		this.addedTime = Instant.now(clock);
	}

	public static final class EventBuilder {

		private final Event event;

		private EventBuilder(Clock clock) {
			event = new Event(clock);
		}

		public static EventBuilder anEvent(Clock clock) {
			return new EventBuilder(clock);
		}

		public EventBuilder id(UUID id) {
			event.setId(id);
			return this;
		}

		public EventBuilder userId(UUID userId) {
			event.setUserId(userId);
			return this;
		}

		public EventBuilder source(EventSource source) {
			event.setSource(source);
			return this;
		}

		public EventBuilder type(EventType type) {
			event.setType(type);
			return this;
		}

		public EventBuilder additionalJson(String additionalJson) {
			event.setAdditionalJson(additionalJson);
			return this;
		}

		public EventBuilder additionalJson(Object objectTypeJson) {
			return additionalJsonToString(objectTypeJson);
		}

		public EventBuilder additionalJson(SaveDeleteTypeJson saveDeleteTypeJson) {
			return additionalJsonToString(saveDeleteTypeJson);
		}

		private EventBuilder additionalJsonToString(Object additionalJson) {
			try {
				String json = new ObjectMapper().writeValueAsString(additionalJson);
				event.setAdditionalJson(json);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return this;
		}


		public EventBuilder serverId(UUID serverId) {
			event.setServerId(serverId);
			return this;
		}

		public Event build() {
			return event;
		}
	}

	@AllArgsConstructor
	public static class SaveDeleteTypeJson {

		public UUID saveGameId;
	}
}
