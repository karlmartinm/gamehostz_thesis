package com.gamehostz.main.domain.client;

import com.gamehostz.main.helpers.BadRequestAlertException;

public class UserException extends BadRequestAlertException {
	private static final long serialVersionUID = 1L;

	public UserException(UserError code) {
		super(code.name());
	}
}
