package com.gamehostz.main.domain.bilingprocessor;

import com.gamehostz.main.domain.client.UserException;
import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.domain.serverprovider.ShutdownEventType;
import com.gamehostz.main.domain.serverprovider.digitalocean.DigitalOceanProvider;
import com.gamehostz.main.domain.transactions.Transaction;
import com.gamehostz.main.usecases.getbillingamount.GetBillingAmount;
import com.gamehostz.main.usecases.stopserver.DeleteServer;
import com.gamehostz.main.usecases.usepoints.UsePoints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.gamehostz.main.domain.client.UserError.CUSTOMER_NOT_FOUND;
import static java.time.temporal.ChronoUnit.MINUTES;

@Component
public class BillingProcessorJob {
	private Logger log = LoggerFactory.getLogger(DigitalOceanProvider.class);

	private final ServerInfoRepository serverInfoRepository;
	private final UsePoints usePoints;
	private final GetBillingAmount getBillingAmount;
	private final CustomerRepository customerRepository;
	private final DeleteServer deleteServer;
	private int billingInterval;
	private Clock clock;

	// not using @Autowired here because @Value can not be used any other way
	public BillingProcessorJob(@Value("${gamehostz.billing.interval.minutes}") int billingInterval, ServerInfoRepository serverInfoRepository, UsePoints usePoints, CustomerRepository customerRepository, DeleteServer deleteServer, Clock clock) {
		this.serverInfoRepository = serverInfoRepository;
		this.billingInterval = billingInterval;
		this.usePoints = usePoints;
		this.clock = clock;
		this.getBillingAmount = new GetBillingAmount();
		this.customerRepository = customerRepository;
		this.deleteServer = deleteServer;
	}


	@Scheduled(fixedDelay = 1000 * 60 ) // Every 1 minute and on startup
	public void run() throws IOException {
		List<ServerInfo> runningServers = serverInfoRepository.findAllByClosedAtIsNull();
		/*
		server is paid server
		and servers where continue time is null and started at time + 60 min > now()
		or server where continue time + 60 min > now()
		then bill the server with appropriate amount*
		and update continue time to now()
		if user does not have resources to continue, shut down the server (might change to send user a message to pay up)
		*/
		runningServers.stream()
			.filter(this::isServerBillable)
			.forEach(this::billOrCloseServer);
	}

	private boolean isServerBillable(ServerInfo serverInfo) {
		List<Instant> times = new ArrayList<>();
		if (serverInfo.getLastBilled() != null)
			times.add(serverInfo.getLastBilled());
		if (serverInfo.getServerStartTime() != null)
			times.add(serverInfo.getServerStartTime());
		if (serverInfo.getContinuedAt() != null)
			times.add(serverInfo.getContinuedAt());

		Instant latestBillableMoment = times.stream()
			.max(Comparator.comparing(Instant::getEpochSecond))
			.orElseGet(() -> newBillableTime(serverInfo));

		boolean timeToBillTheServer = clock.instant().isAfter(latestBillableMoment.plus(billingInterval, MINUTES));
		boolean paidServer = serverInfo.isPaidServer();
		return paidServer && timeToBillTheServer;
	}

	private Instant newBillableTime(ServerInfo serverInfo) {
		Instant newBillableTime = clock.instant();
		serverInfo.setLastBilled(newBillableTime);
		serverInfoRepository.save(serverInfo);
		return newBillableTime;
	}

	private void billOrCloseServer(ServerInfo serverInfo) {
		if (!billServerAndReturnSuccess(serverInfo))
			deleteServer.execute(DeleteServer.Request.of(serverInfo.getId(), ShutdownEventType.SHUTDOWN_BILLING));
	}

	private boolean billServerAndReturnSuccess(ServerInfo serverInfo) {
		// Note that prices should always be dividable by 4
		BigDecimal billingAmount = getBillingAmount.execute(GetBillingAmount.Request.ofSlug(serverInfo.getSlug()));
		BigDecimal quarterlyBillingAmount = billingAmount.divide(BigDecimal.valueOf(4), RoundingMode.HALF_EVEN);
		Customer customer = customerRepository.findByAuthId(serverInfo.getCreatedBy()).orElseThrow(() -> new UserException(CUSTOMER_NOT_FOUND));
		try {
			Transaction transaction = usePoints.execute(UsePoints.Request.of(customer.getId(), quarterlyBillingAmount));
			log.info("BillingProcessor: billing server: {}, customerId: {}, authId: {}, TransactionId: {}",
				serverInfo.getId(), customer.getId(), customer.getAuthId(), transaction.getId());
			serverInfo.setLastBilled(clock.instant());
			serverInfoRepository.save(serverInfo);
			return true;
		} catch (UserException e) {
			log.info("BillingProcessor: billing unsuccessful for serverId: {}, customerId: {}, authId: {}",
				serverInfo.getId(), customer.getId(), customer.getAuthId());
			return false;
		}
	}
}
