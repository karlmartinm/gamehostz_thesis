package com.gamehostz.main.domain.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ClientConfig {

	private List<ClientConfigSetting> settings;

	public ClientConfig() {
	}

	public ClientConfig(List<ClientConfigSetting> settings) {
		this.settings = settings;
	}

	@JsonProperty("settings")
	public List<ClientConfigSetting> getClientConfigSetting() {
		return settings;
	}

	@Override
	public String toString() {
		return "ClientConfig{" +
			"settings=" + settings +
			'}';
	}
}
