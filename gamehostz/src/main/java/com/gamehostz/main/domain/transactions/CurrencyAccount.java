package com.gamehostz.main.domain.transactions;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
public class CurrencyAccount {
	@Id
	private UUID id;

	@Enumerated(EnumType.STRING)
	private CurrencyType currencyType;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.REFRESH, mappedBy="currencyAccount")
	private List<Transaction> transactions;

	public CurrencyAccount(CurrencyType currencyType) {
		this.id = UUID.randomUUID();
		this.currencyType = currencyType;
	}

	public CurrencyAccount(UUID id, CurrencyType currencyType) {
		this.id = id;
		this.currencyType = currencyType;
	}

	public CurrencyType getCurrencyType() {
		return currencyType;
	}

	public CurrencyAccount() {
	}

	public UUID getId() {
		return id;
	}
}
