package com.gamehostz.main.domain.serverprovider;

import com.gamehostz.main.domain.savegame.SaveGame;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ServerInfoRepository extends CrudRepository<ServerInfo, UUID> {
	// returns only servers that are running
	// because of @Where(clause = "closed_at IS NULL") in ServerInfo.java
	List<ServerInfo> findAllByClosedAtIsNull();

	List<ServerInfo> findAllByCreatedByAndClosedAtIsNull(UUID userId);

	Optional<ServerInfo> findOneByIdAndCreatedByAndClosedAtIsNull(UUID serverId, UUID userId);

	Optional<ServerInfo> findOneByIdAndClosedAtIsNull(UUID serverId);

	Optional<ServerInfo> findFirstBySaveGameIdOrderByClosedAtDesc(UUID saveGameId);
}
