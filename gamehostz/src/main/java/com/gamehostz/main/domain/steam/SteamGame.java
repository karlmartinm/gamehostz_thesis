package com.gamehostz.main.domain.steam;

public enum SteamGame {
	CSGO("730");

	private final String appId;

	SteamGame(String appId) {
		this.appId = appId;
	}
}
