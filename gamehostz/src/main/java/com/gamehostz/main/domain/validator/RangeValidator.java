package com.gamehostz.main.domain.validator;

import com.gamehostz.main.domain.gameserver.GameConfigFileSetting;

import static com.gamehostz.main.domain.validator.ValidationError.INVALID_SETTING;

public class RangeValidator implements Validator {
	@Override
	public void validate(GameConfigFileSetting serverSetting, String clientValue) {
		if (!validRange(serverSetting, clientValue)) {
			throw new ConfigValidationException(INVALID_SETTING);
		}
	}

	private boolean validRange(GameConfigFileSetting setting, String value) {
		Integer intValue = Integer.valueOf(value); // TODO check if not number
		return (Integer.parseInt(setting.getMinValue()) <= intValue
			&& Integer.parseInt(setting.getMaxValue()) >= intValue);
	}
}
