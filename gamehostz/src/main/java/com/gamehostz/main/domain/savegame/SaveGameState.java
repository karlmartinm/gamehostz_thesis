package com.gamehostz.main.domain.savegame;

public enum SaveGameState {
	ACTIVE, // saved and ready to load
	RUNNING, // loaded and running
	SAVING, // in the process of saving

	// used for save-delete microservice json creation (usecases.lambda.getsaves.GetSaves) - not actually overwritten yet
	// server_info.save_overwritten = true means save file actually deleted
	OVERWRITTEN,

	// used by save game deletion lambda function to tell java that zip is too old and was deleted.
	// This results with status DELETED in db
	EXPIRED,
	DELETE_REQUEST, // user request save delete but not yet deleted
	DELETED // save actually deleted
}
