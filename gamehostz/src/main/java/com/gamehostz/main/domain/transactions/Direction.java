package com.gamehostz.main.domain.transactions;

public enum Direction {
	DEBIT, CREDIT
}
