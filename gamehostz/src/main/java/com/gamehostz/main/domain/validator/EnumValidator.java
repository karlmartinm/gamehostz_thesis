package com.gamehostz.main.domain.validator;

import com.gamehostz.main.domain.gameserver.GameConfigFileSetting;

import static com.gamehostz.main.domain.validator.ValidationError.INVALID_SETTING;

public class EnumValidator implements Validator {
	@Override
	public void validate(GameConfigFileSetting serverSetting, String clientValue) {
		if (!validEnum(serverSetting, clientValue)) {
			throw new ConfigValidationException(INVALID_SETTING);
		}
	}

	private boolean validEnum(GameConfigFileSetting setting, String value) {
		return setting.getAllowedValues().contains(value);
	}

}
