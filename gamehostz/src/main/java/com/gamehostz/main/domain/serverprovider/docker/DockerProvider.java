package com.gamehostz.main.domain.serverprovider.docker;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.domain.serverprovider.ServerProvider;
import com.gamehostz.main.domain.serverprovider.ServerProviderException;
import com.gamehostz.main.domain.serverprovider.digitalocean.DigitalOceanProvider;
import com.gamehostz.main.usecases.generateuserdata.GenerateUserData;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.Clock;
import java.time.Instant;
import java.util.UUID;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.domain.serverprovider.ProviderError.GENERIC_ERROR;
import static com.gamehostz.main.domain.serverprovider.ProviderError.NO_RUNNING_SERVER;
import static com.gamehostz.main.domain.serverprovider.ServerProviderName.LOCAL_DOCKER;
import static org.apache.commons.codec.Charsets.UTF_8;

@Component
public class DockerProvider implements ServerProvider {
	private final Clock clock;
	private final ServerInfoRepository serverInfoRepository;
	private final GenerateUserData generateUserData;
	private Logger log = LoggerFactory.getLogger(DigitalOceanProvider.class);
	private Process process;


	public DockerProvider(Clock clock, ServerInfoRepository serverInfoRepository, GenerateUserData generateUserData) {
		this.clock = clock;
		this.serverInfoRepository = serverInfoRepository;
		this.generateUserData = generateUserData;
	}

	@Override
	public ServerInfo startServer(String region, String ramSize, String gameName, ClientConfig gameConfig, UUID userId, UUID saveFileId,
								  UUID saveGameId) {
		UUID serverId = UUID.randomUUID();
		GameConfig internalGameConfig = gameConfigs.get(gameName);

		String exec_script = generateUserData.execute(serverId, LOCAL_DOCKER, internalGameConfig, saveFileId);
		try {
			FileUtils.write(new File("local/base_image/execScript.sh"), exec_script, UTF_8);
			process = Runtime.getRuntime().exec("cmd /c start \"\" local\\start_script.bat");
		} catch (IOException e) {
			log.error("Start server Docker local exception: {}", e);
			throw new ServerProviderException(GENERIC_ERROR);
		}

		ServerInfo serverInfo = ServerInfo.Builder.aServerInfo()
			.id(serverId)
			.externalId("external_id")
			.gameAbbreviation(gameName)
			.gameConfig(gameConfig)
			.createdBy(userId)
			.saveGameId(saveGameId)
			.slug(ramSize)
			.regionCode(region)
			.createdAt(Instant.now(clock))
			.paidServer(true)
			.build();

		serverInfo.setAddress("localhost");
		serverInfoRepository.save(serverInfo);
		return serverInfo;
	}

	@Override
	public ServerInfo getServerInfo(UUID serverId) {
		return serverInfoRepository.findOneByIdAndClosedAtIsNull(serverId)
			.orElseThrow(() -> new ServerProviderException(NO_RUNNING_SERVER));
	}

	@Override
	public void stopServer(UUID serverId) {
		if (process!= null) {
			process.destroy();
		} else {
			log.error("no process to kill Docker");
			throw new ServerProviderException(GENERIC_ERROR);
		}
	}
}
