package com.gamehostz.main.domain.savegame;

import com.gamehostz.main.usecases.getsavegames.SaveGameDTO;
import com.gamehostz.main.usecases.loadserver.LoadServerData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface SaveGameRepository extends CrudRepository<SaveGame, UUID> {

	//	@Query(value = "SELECT new com.gamehostz.main.usecases.getsavegames.SaveGameDTO(si.id, si.saveGameStackId, si.gameAbbreviation, si.closedAt)"
	//		+
	//		"FROM ServerInfo si, SaveGame sg " +
	//		"WHERE si.createdBy = :userId and " +
	//		"sg.state = com.gamehostz.main.domain.savegame.SaveGameState.ACTIVE and " +
	//		"sg.saveGameStackId = si.saveGameStackId and " +
	//			"si.saveGameStackId IN (SELECT DISTINCT si2.saveGameStackId FROM ServerInfo si2 WHERE si2.createdBy = :userId)")

	//	@Query(value = "SELECT new com.gamehostz.main.usecases.getsavegames.SaveGameDTO(si.id, si.saveGameStackId, si.gameAbbreviation, si.closedAt)"
	//		+
	//		" FROM (" +
	//		" SELECT si.save_game_stack_id, si.id, si.server_type, si.closed_at" +
	//		" FROM server_info si, save_game sg" +
	//		" WHERE si.created_by = :userId" +
	//		" AND sg.state = com.gamehostz.main.domain.savegame.SaveGameState.ACTIVE" +
	//		" AND sg.save_game_stack_id = si.save_game_stack_id" +
	//		" ORDER BY si.save_game_stack_id, closed_at DESC" +
	//		") as s sub " +
	//		"ORDER BY closed_at DESC")
	//	@Query(value = "SELECT new com.gamehostz.main.usecases.getsavegames.SaveGameDTO(si.id, si.saveGameStackId, si.gameAbbreviation, si.closedAt)"
	//		+
	//		" FROM ServerInfo si, SaveGame sg" +
	//		" WHERE si.createdBy = :userId" +
	//		" AND sg.state = com.gamehostz.main.domain.savegame.SaveGameState.ACTIVE" +
	//		" AND sg.saveGameStackId = si.saveGameStackId" +
	//		" AND si.saveGameStackId IN (SELECT DISTINCT si2.saveGameStackId FROM ServerInfo si2)" +
	//		" ORDER BY si.closedAt DESC")

	//	@Query(value = "SELECT new com.gamehostz.main.usecases.getsavegames.SaveGameDTO(si.id, si.saveGameStackId, si.gameAbbreviation, si.closedAt)"
	//		+ " FROM ServerInfo si"
	//		+ " WHERE"
	//		+ "	si.saveGameStackId IN (SELECT DISTINCT sg.saveGameStackId"
	//		+ " FROM si"
	//		+ " JOIN si.saveGameStackId sg"
	//		+ " ORDER BY si.closedAt) as lollus")
	//	@Query(value = "SELECT new com.gamehostz.main.usecases.getsavegames.SaveGameDTO(si.id, si.saveGameStackId, si.gameAbbreviation, si.closedAt)"
	//		+ " FROM ServerInfo si"
	//		+ " WHERE si.saveGameStackId IN"
	//		+ "   (SELECT DISTINCT sg.saveGameStackId"
	//		+ "    FROM SaveGame sg"
	//		+ "    JOIN ServerInfo si2 ON sg.saveGameStackId = si2.saveGameStackId"
	//		+ "    ORDER BY si2.closedAt) as lollus")

	//	@Query(value = "SELECT DISTINCT si.save_game_stack_id, si.closed_at, si.id, si.server_type FROM server_info si JOIN save_game sg ON si.save_game_stack_id = sg.save_game_stack_id ORDER BY si.closed_at DESC", nativeQuery = true)

	// TODO distinct on this bitch
	@Query(value =
		"SELECT new com.gamehostz.main.usecases.getsavegames.SaveGameDTO(si.id, si.saveGameId, si.gameAbbreviation, si.closedAt, sg.state, si.regionCode, sg.saveSize, si.createdBy)"
			+
			"FROM ServerInfo si, SaveGame sg " +
			"WHERE si.createdBy = :userId AND " +
			"sg.state = com.gamehostz.main.domain.savegame.SaveGameState.RUNNING OR " +
			"sg.state = com.gamehostz.main.domain.savegame.SaveGameState.SAVING OR " +
			"sg.state = com.gamehostz.main.domain.savegame.SaveGameState.ACTIVE AND " +
			"sg.id = si.saveGameId AND " +
			"si.saveDeleted = FALSE AND " +
			"si.saveGameId IN (SELECT DISTINCT si2.saveGameId FROM ServerInfo si2 WHERE si2.createdBy = :userId)" +
			"ORDER BY si.closedAt DESC")
	List<SaveGameDTO> findSaveGamesWithUserId(@Param("userId") UUID userId);

	Optional<SaveGame> findOneByIdOrderByCreatedAtDesc(UUID saveGameId);

	Optional<SaveGame> getOne(UUID id);

	@Query(value =
		"SELECT new com.gamehostz.main.usecases.getsavegames.SaveGameDTO(si.id, si.saveGameId, si.gameAbbreviation, si.closedAt, sg.state, si.regionCode, sg.saveSize, si.createdBy)"
			+
			"FROM ServerInfo si, SaveGame sg " +
			"WHERE sg.state = com.gamehostz.main.domain.savegame.SaveGameState.SAVING OR " +
			"sg.state = com.gamehostz.main.domain.savegame.SaveGameState.ACTIVE AND " +
			"sg.id = si.saveGameId AND " +
			"si.saveDeleted = FALSE " +
			"ORDER BY si.closedAt DESC")
	List<SaveGameDTO> findAllActive();

	@Query(value =
		"SELECT new com.gamehostz.main.usecases.getsavegames.SaveGameDTO(si.id, si.saveGameId, si.gameAbbreviation, si.closedAt, sg.state, si.regionCode, sg.saveSize, si.createdBy)"
			+
			"FROM ServerInfo si, SaveGame sg " +
			"WHERE sg.state = com.gamehostz.main.domain.savegame.SaveGameState.DELETE_REQUEST AND " +
			"sg.id = si.saveGameId AND " +
			"si.saveDeleted = FALSE " +
			"ORDER BY si.closedAt DESC")
	List<SaveGameDTO> findAllDeleteRequests();

	// UUID gameSaveStackId, String serverProvider, String saveGameLocation, String gameAbbreviation, String slug, String region

	@Query(value =
		"SELECT new com.gamehostz.main.usecases.loadserver.LoadServerData(si.saveGameId, si.id, si.serverProvider, sg.saveGameLocation, si.gameAbbreviation, si.slug, si.regionCode)"
			+
			"FROM ServerInfo si, SaveGame sg " +
			"WHERE si.saveGameId = :saveGameId AND " +
			"sg.id = :saveGameId AND " +
			"sg.state = com.gamehostz.main.domain.savegame.SaveGameState.ACTIVE " +
			"ORDER BY si.closedAt DESC")
	Optional<LoadServerData> findSaveGameData(@Param("saveGameId") UUID saveGameId);

	@Query(value =
		"SELECT new com.gamehostz.main.usecases.loadserver.LoadServerData(si.saveGameId, si.id, si.serverProvider, sg.saveGameLocation, si.gameAbbreviation, si.slug, si.regionCode)"
			+
			"FROM ServerInfo si, SaveGame sg " +
			"WHERE si.saveGameId = :saveGameId AND " +
			"sg.id = :saveGameId AND " +
			"si.createdBy = :userId AND " +
			"sg.state = com.gamehostz.main.domain.savegame.SaveGameState.ACTIVE " +
			"ORDER BY si.closedAt DESC")
	List<LoadServerData> findUserSaveGames(@Param("saveGameId") UUID saveGameId, @Param("userId") UUID userId);
}
