package com.gamehostz.main.domain.customer;

import com.gamehostz.main.domain.transactions.CurrencyAccount;
import lombok.Getter;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
public class Customer {
	@Id
	private UUID id;

	@Column(unique=true)
	private UUID authId;

	private Customer() {
	}

	public Customer(UUID id, UUID authId, Set<CurrencyAccount> currencyAccounts) {
		this.id = id;
		this.authId = authId;
		this.currencyAccounts = currencyAccounts;
	}

	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name="customer_currency_account_rel",
		joinColumns=@JoinColumn(name="customer_id", referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="currency_account_id", referencedColumnName="id", unique = true))
	private Set<CurrencyAccount> currencyAccounts;


	public static final class CustomerBuilder {
		private UUID authId;
		private Set<CurrencyAccount> currencyAccounts;

		private CustomerBuilder() {
		}

		public static CustomerBuilder aCustomer() {
			return new CustomerBuilder();
		}

		public CustomerBuilder withAuthId(UUID authId) {
			this.authId = authId;
			return this;
		}

		public CustomerBuilder withCurrencyAccounts(Set<CurrencyAccount> currencyAccounts) {
			this.currencyAccounts = currencyAccounts;
			return this;
		}

		public Customer build() {
			Customer customer = new Customer();
			customer.currencyAccounts = this.currencyAccounts;
			customer.authId = this.authId;
			customer.id = UUID.randomUUID();
			return customer;
		}
	}
}
