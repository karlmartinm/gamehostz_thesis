package com.gamehostz.main.domain.validator;

public enum ValidationError {
	NO_CONFIG_FOUND,
	INVALID_SETTING,
	SETTING_TYPE_MISSING,
	NO_REGION_FOUND,
	NO_RAM_FOUND,
	NULL_CONFIG
}
