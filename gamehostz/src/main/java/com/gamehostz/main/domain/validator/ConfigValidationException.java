package com.gamehostz.main.domain.validator;

import com.gamehostz.main.helpers.BadRequestAlertException;

public class ConfigValidationException extends BadRequestAlertException {
	private static final long serialVersionUID = 1L;

	public ConfigValidationException(ValidationError code) {
		super(code.name());
	}
}
