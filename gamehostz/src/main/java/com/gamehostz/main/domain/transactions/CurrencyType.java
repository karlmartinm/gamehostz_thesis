package com.gamehostz.main.domain.transactions;

import java.math.BigDecimal;

public enum CurrencyType {
	EUR("€", new BigDecimal("120")),
	USD("$", new BigDecimal("110")),
	POINT("Ptz", new BigDecimal("1"));

	private String symbol;
	private BigDecimal multiplier;

	CurrencyType(String symbol, BigDecimal multiplier) {
		this.symbol = symbol;
		this.multiplier = multiplier;
	}

	public String getCurrencySign() {
		return this.symbol;
	}

	public BigDecimal getMultiplier() {
		return multiplier;
	}
}
