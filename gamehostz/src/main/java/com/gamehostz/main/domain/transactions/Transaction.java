package com.gamehostz.main.domain.transactions;

import com.gamehostz.main.Constants;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.util.UUID;

@Entity
public class Transaction {
	@Id
	private UUID id;

	private BigDecimal amount;

	@Enumerated(EnumType.STRING)
	private Direction direction;

	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency_account_id", referencedColumnName = "id")
	private CurrencyAccount currencyAccount;

	@Column(name = "created_at", nullable = false)
	private Instant created_at;

	public Transaction() {
	}

	public Direction getDirection() {
		return direction;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public CurrencyAccount getCurrencyAccount() {
		return currencyAccount;
	}

	public UUID getId() {
		return id;
	}


	public static final class TransactionBuilder {
		private BigDecimal amount;
		private Direction direction;
		private TransactionType transactionType;
		private CurrencyAccount currencyAccount;

		private TransactionBuilder() {
		}

		public static TransactionBuilder aTransaction() {
			return new TransactionBuilder();
		}

		public TransactionBuilder withAmount(BigDecimal amount) {
			this.amount = amount;
			return this;
		}

		public TransactionBuilder withDirection(Direction direction) {
			this.direction = direction;
			return this;
		}

		public TransactionBuilder withTransactionType(TransactionType transactionType) {
			this.transactionType = transactionType;
			return this;
		}

		public TransactionBuilder withCurrencyAccount(CurrencyAccount currencyAccount) {
			this.currencyAccount = currencyAccount;
			return this;
		}

		public Transaction build() {
			Transaction transaction = new Transaction();
			transaction.amount = this.amount;
			transaction.currencyAccount = this.currencyAccount;
			transaction.direction = this.direction;
			transaction.transactionType = this.transactionType;
			transaction.created_at = Instant.now(Clock.system(Constants.OFFSET));
			transaction.id = UUID.randomUUID();
			return transaction;
		}
	}
}
