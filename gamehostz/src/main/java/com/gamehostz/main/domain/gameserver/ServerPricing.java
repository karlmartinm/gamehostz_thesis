package com.gamehostz.main.domain.gameserver;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServerPricing {
	private long id;
	private String name;
	private String slug;
	private long memory;
	private long cpu;
	@JsonProperty("image_name")
	private String imageName;
	@JsonProperty("cost_per_hour")
	private long costPerHour;

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSlug() {
		return slug;
	}

	public long getMemory() {
		return memory;
	}

	public long getCpu() {
		return cpu;
	}

	public String getImageName() {
		return imageName;
	}

	public long getCostPerHour() {
		return costPerHour;
	}
}
