package com.gamehostz.main.domain.client;

public enum GameConfigError {
	SETTING_NOT_FOUND,
	REGION_NOT_FOUND
}
