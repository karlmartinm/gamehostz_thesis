package com.gamehostz.main.domain.steam;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface GsltTokenRepository extends CrudRepository<GsltToken, UUID> {
	@Query("SELECT token " +
		"FROM GsltToken token " +
		"WHERE token.inUse = false")
	List<GsltToken> findAllNotInUse();
}

