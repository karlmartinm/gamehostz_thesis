package com.gamehostz.main.domain.savegame;

public enum SaveGameError {
	LOAD_FAILED,
	SAVING_FAILED,
	DELETE_REQUEST_FAILED
}
