package com.gamehostz.main.domain.steam;

import com.gamehostz.main.helpers.BadRequestAlertException;

public class GsltException extends BadRequestAlertException {
	private static final long serialVersionUID = 1L;

	public GsltException(GsltError code) {
		super(code.name());
	}
}
