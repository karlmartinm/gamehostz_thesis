package com.gamehostz.main.domain.validator;

import com.gamehostz.main.domain.gameserver.GameConfigFileSetting;

public interface Validator {
	void validate(GameConfigFileSetting serverSetting, String clientValue);
}
