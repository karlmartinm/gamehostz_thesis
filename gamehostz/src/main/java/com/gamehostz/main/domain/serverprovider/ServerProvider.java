package com.gamehostz.main.domain.serverprovider;

import com.gamehostz.main.domain.client.ClientConfig;

import java.util.UUID;

public interface ServerProvider {
	// TODO this should include save_game.save_game_location input and usage
	ServerInfo startServer(String region,
	                       String ramSize,
	                       String gameName,
	                       ClientConfig gameConfig,
	                       UUID userId, UUID saveFileId,
	                       UUID saveGameId);

	ServerInfo getServerInfo(UUID serverId);

	void stopServer(UUID serverId);
}
