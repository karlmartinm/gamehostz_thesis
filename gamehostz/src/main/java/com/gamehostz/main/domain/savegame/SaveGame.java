package com.gamehostz.main.domain.savegame;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerProviderName;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.Clock;
import java.time.Instant;
import java.util.UUID;

@Entity
@Data
public class SaveGame {
	@Id
	@Type(type = "pg-uuid")
	private UUID id;

	@Column(nullable = false)
	private Instant createdAt;

	@Enumerated(EnumType.STRING)
	private SaveGameLocation saveGameLocation; // should be foreign key/enum to save game storage source
	@Enumerated(EnumType.STRING)
	private SaveGameState state;
	private long saveSize; // in bytes

	public SaveGame() {
	}

	public SaveGame(UUID id, Clock clock, SaveGameLocation saveGameLocation, SaveGameState state) {
		this.id = id;
		this.createdAt = Instant.now(clock);
		this.saveGameLocation = saveGameLocation;
		this.state = state;
	}

	public SaveGame(UUID id, Instant instant, SaveGameLocation saveGameLocation, SaveGameState state) {
		this.id = id;
		this.createdAt = instant;
		this.saveGameLocation = saveGameLocation;
		this.state = state;
	}

	public static SaveGame createSaveGame(UUID id, Clock clock, SaveGameLocation saveGameLocation, SaveGameState state) {
		return new SaveGame(id, clock, saveGameLocation, state);
	}

	public static SaveGame createSaveGame(UUID id, Instant instant, SaveGameLocation saveGameLocation, SaveGameState state) {
		return new SaveGame(id, instant, saveGameLocation, state);
	}
}
