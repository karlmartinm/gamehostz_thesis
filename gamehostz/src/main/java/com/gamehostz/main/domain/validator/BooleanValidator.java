package com.gamehostz.main.domain.validator;

import com.gamehostz.main.domain.gameserver.GameConfigFileSetting;

import static com.gamehostz.main.domain.validator.ValidationError.INVALID_SETTING;

public class BooleanValidator implements Validator {
	@Override
	public void validate(GameConfigFileSetting serverSetting, String clientValue) {
		if (!validBoolean(clientValue)) {
			throw new ConfigValidationException(INVALID_SETTING);
		}
	}

	private boolean validBoolean(String value) {
		return (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"));
	}
}
