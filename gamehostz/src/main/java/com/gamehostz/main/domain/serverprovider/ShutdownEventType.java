package com.gamehostz.main.domain.serverprovider;

public enum ShutdownEventType {
	SHUTDOWN_USER, // sent from SM
	SHUTDOWN_EMPTY, // sent from SM
	SHUTDOWN_NEVER_STARTED, // sent from SM
	SHUTDOWN_BILLING,
	SHUTDOWN_USER_FALLBACK
}
