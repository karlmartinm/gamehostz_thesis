package com.gamehostz.main.domain.serverprovider.digitalocean;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ServerInfoRepository;
import com.gamehostz.main.domain.serverprovider.ServerProvider;
import com.gamehostz.main.domain.serverprovider.ServerProviderException;
import com.gamehostz.main.usecases.generateuserdata.GenerateUserData;
import com.myjeeva.digitalocean.DigitalOcean;
import com.myjeeva.digitalocean.exception.DigitalOceanException;
import com.myjeeva.digitalocean.exception.RequestUnsuccessfulException;
import com.myjeeva.digitalocean.impl.DigitalOceanClient;
import com.myjeeva.digitalocean.pojo.Droplet;
import com.myjeeva.digitalocean.pojo.Image;
import com.myjeeva.digitalocean.pojo.Network;
import com.myjeeva.digitalocean.pojo.Region;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.domain.serverprovider.ProviderError.*;
import static com.gamehostz.main.domain.serverprovider.ServerProviderName.DIGITAL_OCEAN;

@Component
public class DigitalOceanProvider implements ServerProvider {
	private final static String SERVER_MANAGER = "server-manager";
	private final ServerInfoRepository serverInfoRepository;
	private final Clock clock;
	private final GenerateUserData generateUserData;
	private Logger log = LoggerFactory.getLogger(DigitalOceanProvider.class);
	private DigitalOcean apiClient;

	public DigitalOceanProvider(@Value("${digitalocean.auth}") String authToken,
								ServerInfoRepository serverInfoRepository,
								Clock clock,
								GenerateUserData generateUserData) {
		this.generateUserData = generateUserData;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		this.apiClient = new DigitalOceanClient("v2", authToken, httpClient);
		this.serverInfoRepository = serverInfoRepository;
		this.clock = clock;
	}

	@Override
	public ServerInfo startServer(String region, String ramSize, String gameName, ClientConfig gameConfig, UUID userId,
								  UUID saveFileId, UUID saveGameId) {
		GameConfig internalGameConfig = gameConfigs.get(gameName);
		UUID serverId = UUID.randomUUID();

		try {
			Droplet droplet = new Droplet();
			droplet.setName("testing"); // TODO env variable
			droplet.setSize(ramSize);
			droplet.setImage(findValidImageWithRegionAndGame(region, gameName));
			droplet.setRegion(new Region(region));
			droplet.setInstallMonitoring(true);
			droplet.setTags(List.of(internalGameConfig.getLgsmName(), SERVER_MANAGER));
			droplet.setUserData(generateUserData.execute(serverId, DIGITAL_OCEAN, internalGameConfig, saveFileId));
			Droplet created = apiClient.createDroplet(droplet);
			log.info("droplet created {}", created.toString());

			ServerInfo serverInfo = ServerInfo.Builder.aServerInfo()
				.id(serverId)
				.externalId(created.getId().toString())
				.gameAbbreviation(gameName)
				.gameConfig(gameConfig)
				.createdBy(userId)
				.saveGameId(saveGameId)
				.slug(ramSize)
				.regionCode(region)
				.createdAt(Instant.now(clock))
				.paidServer(true)
				.build();

			serverInfoRepository.save(serverInfo);

			return serverInfo;
		} catch (DigitalOceanException | RequestUnsuccessfulException e) {
			log.error("Start server DigitalOcean exception: {}", e);
			throw new ServerProviderException(GENERIC_ERROR);
		}
	}

	@Override
	public ServerInfo getServerInfo(UUID serverId) {
		// TODO move to not be in DigitalOceanprovider. Used for all server providers. Try to use db instead of DO api
		ServerInfo existingServerInfo = serverInfoRepository.findOneByIdAndClosedAtIsNull(serverId)
			.orElseThrow(() -> new ServerProviderException(NO_RUNNING_SERVER));

		Optional.ofNullable(existingServerInfo.getAddress()).ifPresentOrElse(
			existingServerInfo::setAddress,
			() -> {
				try {
					Droplet dropletInfo = apiClient.getDropletInfo(Integer.parseInt(existingServerInfo.getExternalId()));
					existingServerInfo.setAddress(getDropletNetworks(dropletInfo).getIpAddress());
				} catch (DigitalOceanException | RequestUnsuccessfulException e) {
					log.error("Get server info DigitalOcean exception: {}", e);
					throw new ServerProviderException(GENERIC_ERROR);
				}
			}
		);
		return serverInfoRepository.save(existingServerInfo);
	}

	@Override
	public void stopServer(UUID serverId) {
		ServerInfo serverInfo = serverInfoRepository.findById(serverId)
			.orElseThrow(() -> new ServerProviderException(NO_RUNNING_SERVER));
		try {
			apiClient.deleteDroplet(Integer.valueOf(serverInfo.getExternalId()));
		} catch (DigitalOceanException | RequestUnsuccessfulException e) {
			e.printStackTrace();
			throw new ServerProviderException(GENERIC_ERROR);
		}
	}

	private Network getDropletNetworks(Droplet dropletInfo) {
		return dropletInfo.getNetworks().getVersion4Networks()
			.stream()
			.filter(network -> network.getType().equals("public"))
			.findAny()
			.orElseThrow(() -> new ServerProviderException(NO_IP_AVAILABLE));
	}

	Map<String, Image> getAvailableImages() throws RequestUnsuccessfulException, DigitalOceanException {
		return apiClient.getUserImages(0, 0).getImages().stream()
			.filter(image -> image.getName().contains("active"))
			.collect(Collectors.toMap(Image::getName, image -> image));
	}

	private Image findValidImageWithRegionAndGame(String region, String gameName) throws RequestUnsuccessfulException, DigitalOceanException {
		return this.getAvailableImages().values().stream()
			.filter(image -> image.getRegions().contains(region))
			.filter(image -> image.getName().contains(gameName))
			.findAny()
			.orElseThrow(() -> new ServerProviderException(IMAGE_NOT_FOUND));
	}
}
