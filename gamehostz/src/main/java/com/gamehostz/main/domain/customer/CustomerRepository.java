package com.gamehostz.main.domain.customer;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface CustomerRepository extends CrudRepository<Customer, UUID> {
	Optional<Customer> findByAuthId(UUID authId);
}
