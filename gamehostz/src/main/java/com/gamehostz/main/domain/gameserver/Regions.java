package com.gamehostz.main.domain.gameserver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gamehostz.main.domain.client.GameConfigException;

import java.util.List;

import static com.gamehostz.main.domain.client.GameConfigError.REGION_NOT_FOUND;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Regions {

	private List<Region> regions;

	public List<Region> getRegions() {
		return regions;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	public Region getRegionByCode(String code) {
		return regions
			.stream()
			.filter(r -> r.getCode().equals(code))
			.findFirst()
			.orElseThrow(() -> new GameConfigException(REGION_NOT_FOUND));
	}
}
