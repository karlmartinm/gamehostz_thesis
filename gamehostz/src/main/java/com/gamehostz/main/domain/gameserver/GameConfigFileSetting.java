package com.gamehostz.main.domain.gameserver;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameConfigFileSetting {
	private List<String> allowedValues;
	private String display;
	private String name;
	private String link;
	private String description;
	private FieldType type;
	private String minValue;
	private String maxValue;

	private Object defaultValue;
	//	@JsonIgnore
	@JsonProperty(value = "location", access = JsonProperty.Access.WRITE_ONLY)
	private List<List<String>> location;

	public GameConfigFileSetting() {
	}

	public GameConfigFileSetting(List<String> allowedValues,
	                             String display,
	                             String name,
	                             String link,
	                             String description,
	                             FieldType type,
	                             String minValue,
	                             String maxValue,
	                             List<List<String>> location) {
		this.allowedValues = allowedValues;
		this.display = display;
		this.name = name;
		this.link = link;
		this.description = description;
		this.type = type;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.location = location;
	}

	public List<String> getAllowedValues() {
		return allowedValues;
	}

	public void setAllowedValues(List<String> allowedValues) {
		this.allowedValues = allowedValues;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FieldType getType() {
		return type;
	}

	public void setType(FieldType type) {
		this.type = type;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	public List<List<String>> getLocation() {
		return location;
	}

	public void setLocation(List<List<String>> location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "GameConfigFileSetting{" +
			"allowedValues=" + allowedValues +
			", display='" + display + '\'' +
			", name='" + name + '\'' +
			", link='" + link + '\'' +
			", description='" + description + '\'' +
			", type=" + type +
			", minValue='" + minValue + '\'' +
			", maxValue='" + maxValue + '\'' +
			", location=" + location +
			'}';
	}
}
