package com.gamehostz.main.domain.event;

public enum EventType {
	LOGIN,
	REGISTER,
	USER_COMMAND,
	SERVER_EVENT,
	SAVE_DELETE
}
