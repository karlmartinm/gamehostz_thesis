package com.gamehostz.main.domain.transactions;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface TransactionRepository extends CrudRepository<Transaction, UUID> {
	@Override
	<S extends Transaction> List<S> saveAll(Iterable<S> entities);

	List<Transaction> findAllByCurrencyAccountId(UUID currencyAccountId);
}
