package com.gamehostz.main.domain.transactions;

public enum TransactionType {
	DEPOSIT,
	WITHDRAW,
	CONVERSION,
	PAYMENT
}
