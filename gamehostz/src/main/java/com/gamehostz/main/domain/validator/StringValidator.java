package com.gamehostz.main.domain.validator;

import com.gamehostz.main.domain.gameserver.GameConfigFileSetting;

import static com.gamehostz.main.domain.validator.ValidationError.INVALID_SETTING;

public class StringValidator implements Validator {
	@Override
	public void validate(GameConfigFileSetting serverSetting, String clientValue) {
		if (!validString(serverSetting, clientValue)) {
			throw new ConfigValidationException(INVALID_SETTING);
		}
	}

	private boolean validString(GameConfigFileSetting setting, String value) {
		return true;
	}
}
