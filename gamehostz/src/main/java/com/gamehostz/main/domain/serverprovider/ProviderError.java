package com.gamehostz.main.domain.serverprovider;

public enum ProviderError {
	GENERIC_ERROR, NO_IP_AVAILABLE, NO_RUNNING_SERVER, IMAGE_NOT_FOUND, WRONG_GSLT_TOKEN
}
