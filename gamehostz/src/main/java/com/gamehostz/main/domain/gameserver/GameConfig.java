package com.gamehostz.main.domain.gameserver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gamehostz.main.domain.client.GameConfigException;

import java.util.List;

import static com.gamehostz.main.domain.client.GameConfigError.SETTING_NOT_FOUND;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameConfig {

	// note do not remove getters and setters! they are needed for json mapping
	private String name;
	private String link;
	private String thumbnail;
	private String abbreviation;
	private Integer primaryPort;
	private Integer secondsToStart;
	//	@JsonIgnore
	@JsonProperty(value = "ports", access = JsonProperty.Access.WRITE_ONLY)
	private List<Integer> ports;
	//	@JsonIgnore
	@JsonProperty(value = "folder", access = JsonProperty.Access.WRITE_ONLY)
	private String folder;
	//	@JsonIgnore
	@JsonProperty(value = "lgsmName", access = JsonProperty.Access.WRITE_ONLY)
	private String lgsmName;

	@JsonProperty(value = "supportsSave", access = JsonProperty.Access.READ_WRITE)
	private boolean supportsSave;

	@JsonProperty("config")
	private List<GameConfigFile> gameConfigFiles;

	public GameConfig() {
	}

	public GameConfig(String name, String link, String abbreviation, List<GameConfigFile> gameConfigFiles) {
		this.name = name;
		this.link = link;
		this.abbreviation = abbreviation;
		this.gameConfigFiles = gameConfigFiles;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Integer getPrimaryPort() {
		return primaryPort;
	}

	public void setPrimaryPort(Integer primaryPort) {
		this.primaryPort = primaryPort;
	}

	public List<Integer> getPorts() {
		return ports;
	}

	public void setPorts(List<Integer> ports) {
		this.ports = ports;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String getLgsmName() {
		return lgsmName;
	}

	public void setLgsmName(String lgsmName) {
		this.lgsmName = lgsmName;
	}

	public List<GameConfigFile> getGameConfigFiles() {
		return gameConfigFiles;
	}

	public void setGameConfigFiles(List<GameConfigFile> gameConfigFiles) {
		this.gameConfigFiles = gameConfigFiles;
	}

	public GameConfigFileSetting getConfigNameWithName(String name) {
		return gameConfigFiles.stream()
			.flatMap(gameConfigFile -> gameConfigFile.getGameConfigFileSetting().stream())
			.filter(gameConfigFileSetting -> gameConfigFileSetting.getName().equals(name))
			.findFirst()
			.orElseThrow(() -> new GameConfigException(SETTING_NOT_FOUND));
	}

	@Override
	public String toString() {
		return "GameConfig{" +
			"name='" + name + '\'' +
			", link='" + link + '\'' +
			", abbreviation='" + abbreviation + '\'' +
			", gameConfigFile=" + gameConfigFiles +
			'}';
	}

	public boolean getSupportsSave() {
		return supportsSave;
	}

	public Integer getSecondsToStart() {
		return secondsToStart;
	}

	public void setSecondsToStart(Integer secondsToStart) {
		this.secondsToStart = secondsToStart;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public boolean isSupportsSave() {
		return supportsSave;
	}

	public void setSupportsSave(boolean supportsSave) {
		this.supportsSave = supportsSave;
	}
}

