package com.gamehostz.main.domain.gameserver;

public class RamSettings {
	private long maxPlayers;
	private String ramAmount;

	public RamSettings(long maxPlayers, String ramAmount) {
		this.maxPlayers = maxPlayers;
		this.ramAmount = ramAmount;
	}

	public RamSettings() {
	}

	public long getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(long maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public String getRamAmount() {
		return ramAmount;
	}

	public void setRamAmount(String ramAmount) {
		this.ramAmount = ramAmount;
	}
}
