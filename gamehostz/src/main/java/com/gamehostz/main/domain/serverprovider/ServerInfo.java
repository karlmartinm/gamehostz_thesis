package com.gamehostz.main.domain.serverprovider;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.steam.GsltToken;
import com.gamehostz.main.helpers.ClientConfigToJsonConverter;
import java.time.Clock;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Data;

@Entity
@Data
public class ServerInfo {

	@Id
	private UUID id;
	private String externalId;
	private String address;
	private String port;
	private String gameAbbreviation;
	private String slug;
	private String regionCode;
	@Enumerated(EnumType.STRING)
	private ServerProviderName serverProvider;

	@Column(nullable = false)
	private Instant createdAt;
	// authId from Cognito
	private UUID createdBy;
	private Instant closedAt;
	private UUID saveGameId;
	@Enumerated(EnumType.STRING)
	private ShutdownEventType shutdownEventType;

	private Instant continuedAt;
	private boolean paidServer;
	private Instant lastBilled;

	@Column(columnDefinition = "boolean default false")
	private boolean saveDeleted; // used to ignore old Deleted saves when fetching active saves list from lambda

	@Convert(converter = ClientConfigToJsonConverter.class)
	@Column(columnDefinition = "text")
	private ClientConfig gameConfig;

	@ManyToOne
	private GsltToken gslt;

	public ServerInfo() {
	}

	public void endServer(Clock clock, ShutdownEventType shutdownEventType) {
		this.closedAt = Instant.now(clock);
		this.shutdownEventType = shutdownEventType;
	}

	public Instant getServerStartTime() {
		return this.continuedAt != null ? this.continuedAt : this.createdAt;
	}


	public static final class Builder {

		private ServerInfo serverInfo;

		private Builder() {
			serverInfo = new ServerInfo();
		}

		public static Builder aServerInfo() {
			return new Builder();
		}

		public Builder id(UUID id) {
			serverInfo.setId(id);
			return this;
		}

		public Builder externalId(String externalId) {
			serverInfo.setExternalId(externalId);
			return this;
		}

		public Builder address(String address) {
			serverInfo.setAddress(address);
			return this;
		}

		public Builder port(String port) {
			serverInfo.setPort(port);
			return this;
		}

		public Builder gameAbbreviation(String gameAbbreviation) {
			serverInfo.setGameAbbreviation(gameAbbreviation);
			return this;
		}

		public Builder slug(String slug) {
			serverInfo.setSlug(slug);
			return this;
		}

		public Builder regionCode(String regionCode) {
			serverInfo.setRegionCode(regionCode);
			return this;
		}

		public Builder serverProvider(ServerProviderName serverProvider) {
			serverInfo.setServerProvider(serverProvider);
			return this;
		}

		public Builder createdAt(Instant createdAt) {
			serverInfo.setCreatedAt(createdAt);
			return this;
		}

		public Builder createdBy(UUID createdBy) {
			serverInfo.setCreatedBy(createdBy);
			return this;
		}

		public Builder closedAt(Instant closedAt) {
			serverInfo.setClosedAt(closedAt);
			return this;
		}

		public Builder saveGameId(UUID saveGameId) {
			serverInfo.setSaveGameId(saveGameId);
			return this;
		}

		public Builder shutdownEventType(ShutdownEventType shutdownEventType) {
			serverInfo.setShutdownEventType(shutdownEventType);
			return this;
		}

		public Builder continuedAt(Instant continuedAt) {
			serverInfo.setContinuedAt(continuedAt);
			return this;
		}

		public Builder paidServer(boolean paidServer) {
			serverInfo.setPaidServer(paidServer);
			return this;
		}

		public Builder saveDeleted(boolean saveDeleted) {
			serverInfo.setSaveDeleted(saveDeleted);
			return this;
		}

		public Builder gameConfig(ClientConfig gameConfig) {
			serverInfo.setGameConfig(gameConfig);
			return this;
		}

		public Builder gslt(GsltToken gslt) {
			serverInfo.setGslt(gslt);
			return this;
		}

		public ServerInfo build() {
			return serverInfo;
		}
	}
}
