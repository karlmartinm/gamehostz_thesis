package com.gamehostz.main.domain.savegame;

import com.gamehostz.main.helpers.BadRequestAlertException;

public class SaveGameException extends BadRequestAlertException {
	private static final long serialVersionUID = 1L;

	public SaveGameException(SaveGameError code) {
		super(code.name());
	}
}
