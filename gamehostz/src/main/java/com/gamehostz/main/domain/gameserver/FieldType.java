package com.gamehostz.main.domain.gameserver;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum FieldType {
	@JsonProperty("range")
	RANGE,
	@JsonProperty("enum")
	ENUM,
	@JsonProperty("boolean")
	BOOLEAN,
	@JsonProperty("string")
	STRING
}
