package com.gamehostz.main.domain.serverprovider;

public enum ServerProviderName {
	DIGITAL_OCEAN,
	LOCAL_DOCKER
}
