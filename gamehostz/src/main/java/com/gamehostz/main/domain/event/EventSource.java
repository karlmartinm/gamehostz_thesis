package com.gamehostz.main.domain.event;

public enum EventSource {
	PORTAL,
	SM,
	SMS,
	PROXY
}
