package com.gamehostz.main.rest.event;

import com.gamehostz.main.domain.event.EventSource;
import com.gamehostz.main.domain.event.EventType;

import java.util.UUID;

public class EventRequestModel {
	private UUID userId;
	private UUID serverId;
	private EventSource source;
	private EventType type;
	private String additionalJson;

	public EventRequestModel() {
	}

	private EventRequestModel(Builder builder) {
		userId = builder.userId;
		serverId = builder.serverId;
		source = builder.source;
		type = builder.type;
		additionalJson = builder.additionalJson;
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public UUID getUserId() {
		return userId;
	}

	public EventSource getSource() {
		return source;
	}

	public EventType getType() {
		return type;
	}

	public String getAdditionalJson() {
		return additionalJson;
	}

	public UUID getServerId() {
		return serverId;
	}

	public static final class Builder {
		private UUID userId;
		private UUID serverId;
		private EventSource source;
		private EventType type;
		private String additionalJson;

		private Builder() {
		}

		public Builder userId(UUID userId) {
			this.userId = userId;
			return this;
		}

		public Builder serverId(UUID serverId) {
			this.serverId = serverId;
			return this;
		}

		public Builder source(EventSource source) {
			this.source = source;
			return this;
		}

		public Builder type(EventType type) {
			this.type = type;
			return this;
		}

		public Builder additionalJson(String additionalJson) {
			this.additionalJson = additionalJson;
			return this;
		}

		public EventRequestModel build() {
			return new EventRequestModel(this);
		}
	}
}
