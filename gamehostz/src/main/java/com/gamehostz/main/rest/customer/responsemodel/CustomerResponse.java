package com.gamehostz.main.rest.customer.responsemodel;

import java.math.BigDecimal;

public final class CustomerResponse {
	private boolean newCustomer;
	private BigDecimal pointsAdded;

	public boolean isNewCustomer() {
		return newCustomer;
	}

	public void setNewCustomer(boolean newCustomer) {
		this.newCustomer = newCustomer;
	}

	public BigDecimal getPointsAdded() {
		return pointsAdded;
	}

	public void setPointsAdded(BigDecimal pointsAdded) {
		this.pointsAdded = pointsAdded;
	}

	public CustomerResponse() {
	}


	public static final class Builder {
		private CustomerResponse customerResponse;

		private Builder() {
			customerResponse = new CustomerResponse();
		}

		public static Builder aCustomerResponse() {
			return new Builder();
		}

		public Builder newCustomer(boolean newCustomer) {
			customerResponse.setNewCustomer(newCustomer);
			return this;
		}

		public Builder pointsAdded(BigDecimal pointsAdded) {
			customerResponse.setPointsAdded(pointsAdded);
			return this;
		}

		public CustomerResponse build() {
			return customerResponse;
		}
	}
}
