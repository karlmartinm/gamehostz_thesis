package com.gamehostz.main.rest.customer;

import com.gamehostz.main.rest.customer.responsemodel.CustomerResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.function.Consumer;

public class CustomerResponsePresenter implements Consumer<CustomerResponse> {

  private CustomerResponse response;

  @Override
  public void accept(CustomerResponse response) {
    this.response = response;
  }

  ResponseEntity<CustomerResponse> getViewModel() {
    HttpHeaders headers = new HttpHeaders();

    return ResponseEntity.ok().headers(headers).body(response);
  }
}
