package com.gamehostz.main.rest.stripe;


import com.gamehostz.main.domain.transactions.CurrencyType;
import com.gamehostz.main.usecases.addpoints.AddPoints;
import com.gamehostz.main.usecases.startserver.StartServer;
import com.stripe.exception.SignatureVerificationException;
import com.stripe.model.Event;
import com.stripe.model.EventDataObjectDeserializer;
import com.stripe.model.StripeObject;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping({ "/api/portal/stripe" })
public class StripeController {

	private Logger log = LoggerFactory.getLogger(StartServer.class);
	private final AddPoints addPoints;
	private String secret;

	public StripeController(@Value("${gamehostz.stripe.secret}") String secret, AddPoints addPoints) {
		this.addPoints = addPoints;
		this.secret = secret;
	}

	@PostMapping()
	ResponseEntity webhook(@RequestBody String payload, @RequestHeader("Stripe-Signature") String sigHeader) {
		Event event = null;
		try {
			event = Webhook.constructEvent(payload, sigHeader, this.secret);
		} catch (SignatureVerificationException e) {
			log.warn("Failed signature verification {}", event.toString());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}

		EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
		StripeObject stripeObject = null;

		if (dataObjectDeserializer.getObject().isPresent()) {
			stripeObject = dataObjectDeserializer.getObject().get();
		} else {
			log.warn("Deserialization failed, probably due to an API version mismatch {}", event.toString());
		}

		switch (event.getType()) {
			case "checkout.session.completed":
				if (stripeObject != null) {
					String clientReferenceId = ((Session) stripeObject).getClientReferenceId();
					BigDecimal amount = BigDecimal.valueOf(((Session) stripeObject).getAmountTotal()/100);
					String currency = ((Session) stripeObject).getCurrency().toUpperCase();
					addPoints.execute(AddPoints.Request.of(UUID.fromString(clientReferenceId), amount, CurrencyType.valueOf(currency)));
				}
				break;
			default:
				//log.warn("Unexpected event type {}", event.toString());
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		return new ResponseEntity<>("Success", HttpStatus.OK);
	}
}
