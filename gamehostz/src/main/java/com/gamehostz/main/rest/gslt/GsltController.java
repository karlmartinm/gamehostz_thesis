package com.gamehostz.main.rest.gslt;

import com.gamehostz.main.rest.gslt.responsemodel.GsltResponse;
import com.gamehostz.main.usecases.closegslt.CloseGslt;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_SERVER_MANAGER')")
@RequestMapping("/api/internal/gslt")
public class GsltController {
	private CloseGslt closeGslt;

	public GsltController(CloseGslt closeGslt) {
		this.closeGslt = closeGslt;
	}

	@PostMapping()
	ResponseEntity<GsltResponse> gsltToken(@RequestBody UUID serverId) {
		var presenter = new GsltResponsePresenter();
		closeGslt.execute(CloseGslt.Request.of(serverId), presenter);
		return presenter.getViewModel();
	}
}
