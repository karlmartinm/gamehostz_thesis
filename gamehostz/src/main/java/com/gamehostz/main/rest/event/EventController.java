package com.gamehostz.main.rest.event;

import com.gamehostz.main.usecases.saveevent.SaveEvent;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping({ "/api/portal/event", "/api/internal/event" })
public class EventController {
	private final SaveEvent saveEvent;

	public EventController(SaveEvent saveEvent) {
		this.saveEvent = saveEvent;
	}

	@PostMapping()
		//	@PreAuthorize("hasRole('ROLE_USER')")
	ResponseEntity saveEvent(@RequestBody EventRequestModel eventRequestModel) {
		saveEvent.execute(SaveEvent.Request.of(eventRequestModel));
		return ResponseEntity.ok().build();
	}
}
