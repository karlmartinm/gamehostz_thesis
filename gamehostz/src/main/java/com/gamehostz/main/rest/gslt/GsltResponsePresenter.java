package com.gamehostz.main.rest.gslt;

import com.gamehostz.main.rest.gslt.responsemodel.GsltResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.function.Consumer;

public class GsltResponsePresenter implements Consumer<GsltResponse> {
	private GsltResponse response;

	@Override
	public void accept(GsltResponse response) {
		this.response = response;
	}

	ResponseEntity<GsltResponse> getViewModel() {
		Assert.notNull(response.getGslt(), () -> "GsltToken must be presented");
		HttpHeaders headers = new HttpHeaders();
		return ResponseEntity.ok().headers(headers).body(response);
	}
}
