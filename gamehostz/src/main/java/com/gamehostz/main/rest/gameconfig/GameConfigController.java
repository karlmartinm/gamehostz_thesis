package com.gamehostz.main.rest.gameconfig;

import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.serverprovider.digitalocean.DigitalOceanImagesJob;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.MainApplication.regions;

@RestController
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_USER')")
@RequestMapping("/api/portal/gameconfigs")
public class GameConfigController {

	@GetMapping
	ResponseEntity<Map<String, GameConfig>> gameConfigs() {
		return ResponseEntity.ok(gameConfigs);
	}

	@GetMapping(path = "/{gameName}")
	ResponseEntity<GameConfig> gameConfig(@PathVariable("gameName") String gameName) {
		return ResponseEntity.ok(gameConfigs.get(gameName));
	}

	@GetMapping(path = "/images")
	ResponseEntity images() {
		return ResponseEntity.ok(DigitalOceanImagesJob.availableImages);
	}

	@GetMapping(path = "/regions")
	ResponseEntity regions() {
		return ResponseEntity.ok(regions);
	}
}
