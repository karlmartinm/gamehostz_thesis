package com.gamehostz.main.rest.gameserver;

import com.gamehostz.main.rest.gameserver.responsemodel.ServerGameConfigResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.function.Consumer;

public class ServerGameConfigResponsePresenter implements Consumer<ServerGameConfigResponse> {
	private ServerGameConfigResponse response;

	@Override
	public void accept(ServerGameConfigResponse response) {
		this.response = response;
	}

	ResponseEntity<ServerGameConfigResponse> getViewModel() {
		Assert.notNull(response.getServerId(), () -> "serverId must be presented");

		HttpHeaders headers = new HttpHeaders();

		return ResponseEntity.ok().headers(headers).body(response);
	}
}
