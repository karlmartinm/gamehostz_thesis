package com.gamehostz.main.rest.gameserver.responsemodel;

import com.gamehostz.main.domain.client.ClientConfig;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

public final class ServerInfoResponse {
	private UUID serverId;
	private String serverAddress;
	private String serverPort;
	private ClientConfig clientConfig;
	private String gameAbbreviation;
	private Instant serverStartedAt;
	private Integer secondsToStart;
	private BigDecimal serverPrice;

	public ServerInfoResponse() {
	}

	public String getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public Instant getServerStartedAt() {
		return serverStartedAt;
	}

	public void setServerStartedAt(Instant serverStartedAt) {
		this.serverStartedAt = serverStartedAt;
	}

	public UUID getServerId() {
		return serverId;
	}

	public void setServerId(UUID serverId) {
		this.serverId = serverId;
	}

	public ClientConfig getClientConfig() {
		return clientConfig;
	}

	public void setClientConfig(ClientConfig clientConfig) {
		this.clientConfig = clientConfig;
	}

	public String getGameAbbreviation() {
		return gameAbbreviation;
	}

	public void setGameAbbreviation(String gameAbbreviation) {
		this.gameAbbreviation = gameAbbreviation;
	}

	public Integer getSecondsToStart() {
		return secondsToStart;
	}

	public void setSecondsToStart(Integer secondsToStart) {
		this.secondsToStart = secondsToStart;
	}

	public BigDecimal getServerPrice() {
		return serverPrice;
	}

	public void setServerPrice(BigDecimal serverPrice) {
		this.serverPrice = serverPrice;
	}

	public static final class Builder {
		private ServerInfoResponse serverInfoResponse;

		private Builder() {
			serverInfoResponse = new ServerInfoResponse();
		}

		public static Builder aServerInfoResponse() {
			return new Builder();
		}

		public Builder serverId(UUID serverId) {
			serverInfoResponse.setServerId(serverId);
			return this;
		}

		public Builder serverAddress(String serverAddress) {
			serverInfoResponse.setServerAddress(serverAddress);
			return this;
		}

		public Builder serverPort(String serverPort) {
			serverInfoResponse.setServerPort(serverPort);
			return this;
		}

		public Builder clientConfig(ClientConfig clientConfig) {
			serverInfoResponse.setClientConfig(clientConfig);
			return this;
		}

		public Builder gameAbbreviation(String gameAbbreviation) {
			serverInfoResponse.setGameAbbreviation(gameAbbreviation);
			return this;
		}

		public Builder serverStartedAt(Instant serverStartedAt) {
			serverInfoResponse.setServerStartedAt(serverStartedAt);
			return this;
		}

		public Builder secondsToStart(Integer secondsToStart) {
			serverInfoResponse.setSecondsToStart(secondsToStart);
			return this;
		}

		public Builder serverPrice(BigDecimal serverPrice) {
			serverInfoResponse.setServerPrice(serverPrice);
			return this;
		}

		public ServerInfoResponse build() {
			return serverInfoResponse;
		}
	}
}
