package com.gamehostz.main.rest.gameserver;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.helpers.CognitoUtils;
import com.gamehostz.main.rest.gameserver.responsemodel.ServerInfoResponse;
import com.gamehostz.main.usecases.getrunningservers.GetRunningServers;
import com.gamehostz.main.usecases.getsavegames.GetSaveGames;
import com.gamehostz.main.usecases.getsavegames.SaveGameDTO;
import com.gamehostz.main.usecases.getserverinfo.GetServerInfo;
import com.gamehostz.main.usecases.loadserver.LoadServer;
import com.gamehostz.main.usecases.savedelete.SaveDelete;
import com.gamehostz.main.usecases.startserver.StartServer;
import com.gamehostz.main.usecases.stopserver.DeleteServer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.gamehostz.main.MainApplication.serverPrices;
import static com.gamehostz.main.domain.serverprovider.ShutdownEventType.SHUTDOWN_USER_FALLBACK;

@RestController
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_USER')")
@RequestMapping("/api/portal/gameservers")
public class GameServerControllerPortal {

	private final StartServer startServer;
	private final GetServerInfo getServerInfo;
	private final GetRunningServers getRunningServers;
	private final GetSaveGames getSaveGames;
	private final LoadServer loadServer;
	private final SaveDelete saveDelete;
	private final CognitoUtils cognitoUtils;
	private final DeleteServer deleteServer;

	public GameServerControllerPortal(StartServer startServer,
		GetServerInfo getServerInfo,
		GetRunningServers getRunningServers,
		GetSaveGames getSaveGames,
		LoadServer loadServer,
		SaveDelete saveDelete,
		CognitoUtils cognitoUtils,
		DeleteServer deleteServer) {
		this.startServer = startServer;
		this.getServerInfo = getServerInfo;
		this.getRunningServers = getRunningServers;
		this.getSaveGames = getSaveGames;
		this.loadServer = loadServer;
		this.cognitoUtils = cognitoUtils;
		this.deleteServer = deleteServer;
		this.saveDelete = saveDelete;
	}

	// TODO this should not be accessible to users as it returns other user servers as well
	@GetMapping
	ResponseEntity<List<ServerInfo>> runningServers() {
		List<ServerInfo> runningServers = getRunningServers.execute();
		return ResponseEntity.ok(runningServers);
	}

	@GetMapping(path = "/running")
	ResponseEntity<List<ServerInfoResponse>> userRunningServers() {
		UUID userId = cognitoUtils.getCognitoUserUUIDFromRequest();
		List<ServerInfoResponse> runningServers = getRunningServers.execute(userId);
		return ResponseEntity.ok(runningServers);
	}

	@PostMapping(path = "/start/{region}/{gameName}")
	ResponseEntity startServer(@RequestBody ClientConfig clientConfig,
							   @PathVariable("gameName") String gameName,
							   @PathVariable("region") String region) {
		UUID userId = cognitoUtils.getCognitoUserUUIDFromRequest();
		var presenter = new ServerInfoResponsePresenter();
		startServer.execute(StartServer.Request.of(clientConfig, gameName, region, userId), presenter);
		return presenter.getViewModel();
	}

	@GetMapping(path = "/{serverId}")
	ResponseEntity serverInfo(@PathVariable("serverId") UUID serverId) {
		var presenter = new ServerInfoResponsePresenter();
		getServerInfo.execute(GetServerInfo.Request.of(serverId), presenter);
		return presenter.getViewModel();
	}

	@DeleteMapping(path = "/stop/{serverId}")
	ResponseEntity stopServer(@PathVariable("serverId") UUID serverId) {
		deleteServer.execute(DeleteServer.Request.of(serverId, SHUTDOWN_USER_FALLBACK));
		return ResponseEntity.ok().build();
	}

	@GetMapping(path = "/save-games")
	ResponseEntity saveGames() {
		UUID userId = cognitoUtils.getCognitoUserUUIDFromRequest();
		List<SaveGameDTO> saveGameDTOS = getSaveGames.execute(userId);
		return ResponseEntity.ok(saveGameDTOS);
	}

	@GetMapping(path = "/load/{saveGameId}")
	ResponseEntity loadGame(@PathVariable("saveGameId") UUID saveGameId) {
		UUID userId = cognitoUtils.getCognitoUserUUIDFromRequest();
		var presenter = new ServerInfoResponsePresenter();
		loadServer.execute(LoadServer.Request.of(saveGameId, userId), presenter);
		return presenter.getViewModel();
	}

	@DeleteMapping(path = "/save/{saveGameId}")
	ResponseEntity saveDelete(@PathVariable("saveGameId") UUID saveGameId) {
		UUID userId = cognitoUtils.getCognitoUserUUIDFromRequest();
		saveDelete.execute(SaveDelete.Request.of(saveGameId, userId));
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@GetMapping(path = "/pricing")
	ResponseEntity getPrices() {
		return ResponseEntity.ok(serverPrices);
	}
}
