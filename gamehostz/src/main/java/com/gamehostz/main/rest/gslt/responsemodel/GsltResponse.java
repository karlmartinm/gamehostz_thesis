package com.gamehostz.main.rest.gslt.responsemodel;

public class GsltResponse {
	private String gsltToken;

	private GsltResponse(String gsltToken) {
		this.gsltToken = gsltToken;
	}

	public static GsltResponse of(String gsltToken) {
		return new GsltResponse(gsltToken);
	}

	public String getGslt() {
		return gsltToken;
	}
}
