package com.gamehostz.main.rest.gameserver;

import com.gamehostz.main.rest.gameserver.responsemodel.ServerInfoResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.function.Consumer;

public class ServerInfoResponsePresenter implements Consumer<ServerInfoResponse> {

  private ServerInfoResponse response;

  @Override
  public void accept(ServerInfoResponse response) {
    this.response = response;
  }

  ResponseEntity<ServerInfoResponse> getViewModel() {
    Assert.notNull(response.getServerId(), () -> "serverId must be presented");

    HttpHeaders headers = new HttpHeaders();

    return ResponseEntity.ok().headers(headers).body(response);
  }
}
