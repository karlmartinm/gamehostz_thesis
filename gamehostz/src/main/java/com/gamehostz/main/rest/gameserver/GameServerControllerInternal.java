package com.gamehostz.main.rest.gameserver;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.serverprovider.ServerInfo;
import com.gamehostz.main.domain.serverprovider.ShutdownEventType;
import com.gamehostz.main.usecases.getrunningservers.GetRunningServerOfUser;
import com.gamehostz.main.usecases.getservergameconfig.GetServerGameConfig;
import com.gamehostz.main.usecases.stopserver.DeleteServer;
import com.gamehostz.main.usecases.stopserver.SavingDone;
import com.gamehostz.main.usecases.validateclientsettings.ValidateClientSettings;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_SERVER_MANAGER')")
@RequestMapping("/api/internal/gameservers")
public class GameServerControllerInternal {

	private final GetServerGameConfig getServerGameConfig;
	private final GetRunningServerOfUser getRunningServerOfUser;
	private final DeleteServer deleteServer;
	private final SavingDone savingDone;
	private final ValidateClientSettings validateClientSettings;

	public GameServerControllerInternal(GetServerGameConfig getServerGameConfig,
	                                    GetRunningServerOfUser getRunningServerOfUser,
	                                    DeleteServer deleteServer,
	                                    SavingDone savingDone,
	                                    ValidateClientSettings validateClientSettings) {
		this.getServerGameConfig = getServerGameConfig;
		this.getRunningServerOfUser = getRunningServerOfUser;
		this.deleteServer = deleteServer;
		this.savingDone = savingDone;
		this.validateClientSettings = validateClientSettings;
	}

	@GetMapping(path = "/game-config/{serverId}")
	ResponseEntity serverGameConfig(@PathVariable("serverId") UUID serverId) {
		var presenter = new ServerGameConfigResponsePresenter();
		getServerGameConfig.execute(GetServerGameConfig.Request.of(serverId), presenter);
		return presenter.getViewModel();
	}

	@GetMapping("/{serverId}/{userId}")
	ResponseEntity<ServerInfo> runningServerWithUser(@PathVariable("serverId") UUID serverId,
	                                                 @PathVariable("userId") UUID userId) {
		ServerInfo runningServer = getRunningServerOfUser.execute(GetRunningServerOfUser.Request.of(serverId, userId));
		return ResponseEntity.ok(runningServer);
	}

	@DeleteMapping(path = "/{serverId}/{shutdownEventType}")
	ResponseEntity killServer(@PathVariable("serverId") UUID serverId,
	                          @PathVariable("shutdownEventType") ShutdownEventType shutdownEventType) {
		deleteServer.execute(DeleteServer.Request.of(serverId, shutdownEventType));
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@PutMapping(path = "/save-done/{serverId}/{saveSize}")
	ResponseEntity savingDone(@PathVariable("serverId") UUID serverId,
	                          @PathVariable("saveSize") long saveSize) {
		savingDone.execute(SavingDone.Request.of(serverId, saveSize));
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@PostMapping(path = "/game-config/validate/{gameName}")
	ResponseEntity validateGameConfig(@RequestBody ClientConfig clientConfig,
	                                  @PathVariable("gameName") String gameName) {

		validateClientSettings.validateGame(clientConfig, gameName);
		return ResponseEntity.ok().build();
	}
}
