package com.gamehostz.main.rest.gameconfig;

import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.serverprovider.digitalocean.DigitalOceanImagesJob;
import com.gamehostz.main.usecases.getbillingamount.GetBillingAmount;
import com.gamehostz.main.usecases.getramsettings.GetRamSettings;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.MainApplication.regions;

@RestController
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_USER')")
@RequestMapping("/api/portal/pricing")
public class ServerPricingController {

	private final GetBillingAmount getBillingAmount;
	private final GetRamSettings getRamSettings;

	public ServerPricingController() {
		this.getBillingAmount = new GetBillingAmount();
		this.getRamSettings = new GetRamSettings();
	}

	@GetMapping
	ResponseEntity<Map<String, GameConfig>> gameConfigs() {
		return ResponseEntity.ok(gameConfigs);
	}

	@GetMapping(path = "/{gameName}/{playerCount}")
	ResponseEntity<BigDecimal> gameConfig(@PathVariable("gameName") String gameName, @PathVariable("playerCount") Integer playerCount) {
		String ramAmount = this.getRamSettings.execute(GetRamSettings.Request.of(gameName, playerCount)).getRamAmount();
		BigDecimal amount = getBillingAmount.execute(GetBillingAmount.Request.ofSlug(ramAmount));
		return ResponseEntity.ok(amount);
	}

	@GetMapping(path = "/images")
	ResponseEntity images() {
		return ResponseEntity.ok(DigitalOceanImagesJob.availableImages);
	}

	@GetMapping(path = "/regions")
	ResponseEntity regions() {
		return ResponseEntity.ok(regions);
	}
}
