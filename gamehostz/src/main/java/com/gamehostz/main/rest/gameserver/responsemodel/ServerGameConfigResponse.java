package com.gamehostz.main.rest.gameserver.responsemodel;

import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.domain.client.ClientConfigSetting;
import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.helpers.Utils;
import com.gamehostz.main.usecases.getservergameconfig.ClientConfigSettingSupplier;
import org.springframework.util.Assert;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class ServerGameConfigResponse {
	private UUID serverId;
	private ClientConfig gameConfig;
	private String gameName;
	private long serverEmptyCloseTime;

	public ServerGameConfigResponse() {
	}

	private ServerGameConfigResponse(Builder builder) {
		setServerId(builder.serverId);
		setGameConfig(builder.gameConfig);
		setGameName(builder.gameName);
		setServerEmptyCloseTime(builder.serverEmptyCloseTime);
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public UUID getServerId() {
		return serverId;
	}

	public void setServerId(UUID serverId) {
		this.serverId = serverId;
	}

	public ClientConfig getGameConfig() {
		return gameConfig;
	}

	public void setGameConfig(ClientConfig gameConfig) {
		this.gameConfig = gameConfig;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public long getServerEmptyCloseTime() { return serverEmptyCloseTime; }

	public void setServerEmptyCloseTime(long serverEmptyCloseTime) { this.serverEmptyCloseTime = serverEmptyCloseTime; }

	public static final class Builder {
		private UUID serverId;
		private ClientConfig gameConfig;
		private String gameName;
		private long serverEmptyCloseTime;

		private Builder() {
		}

		public Builder serverId(UUID serverId) {
			this.serverId = serverId;
			return this;
		}

		public Builder gameConfig(ClientConfig gameConfig) {
			this.gameConfig = gameConfig;
			return this;
		}

		public Builder gameName(String gameName) {
			this.gameName = gameName;
			return this;
		}

		public Builder serverEmptyCloseTime(long serverEmptyCloseTime) {
			this.serverEmptyCloseTime = serverEmptyCloseTime;
			return this;
		}

		public ServerGameConfigResponse build() {
			Assert.notNull(this.gameName, "gameName cannot be null when building response for SM");
			this.gameConfig = (this.gameConfig != null) ? getGameConfigForSM(this.gameConfig) : null; // gameConfig is null when load game
			return new ServerGameConfigResponse(this);
		}

		private ClientConfig getGameConfigForSM(ClientConfig clientConfig) {
			GameConfig gameConfig = Utils.getGameConfigByGameName(this.gameName);

			List<ClientConfigSetting> clientConfigSettings = clientConfig.getSettings().stream()
				.map(mapToCorrectFormat(gameConfig))
				.collect(Collectors.toList());

			return new ClientConfig(clientConfigSettings);
		}

		private Function<ClientConfigSetting, ClientConfigSetting> mapToCorrectFormat(GameConfig gameConfig) {
			return setting -> ClientConfigSettingSupplier.supplyClientConfigSetting(gameConfig, setting);
		}
	}
}
