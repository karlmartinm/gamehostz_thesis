package com.gamehostz.main.rest.customer;


import com.gamehostz.main.helpers.CognitoUtils;
import com.gamehostz.main.usecases.createcustomer.CreateCustomer;
import com.gamehostz.main.usecases.getbalance.GetBalance;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping({ "/api/portal/customer" })
public class CustomerController {

	private final CreateCustomer createCustomer;
	private final GetBalance getBalance;
	private final CognitoUtils cognitoUtils;

	public CustomerController(CreateCustomer createCustomer, GetBalance getBalance, CognitoUtils cognitoUtils) {
		this.createCustomer = createCustomer;
		this.getBalance = getBalance;
		this.cognitoUtils = cognitoUtils;
	}

	@PostMapping()
	ResponseEntity createCustomer() {
		UUID userId = cognitoUtils.getCognitoUserUUIDFromRequest();
		var presenter = new CustomerResponsePresenter();
		createCustomer.execute(CreateCustomer.Request.of(userId), presenter);
		return presenter.getViewModel();
	}

	@GetMapping(path = "/balance")
	ResponseEntity getBalance() {
		UUID userId = cognitoUtils.getCognitoUserUUIDFromRequest();
		BigDecimal balance = getBalance.executeByAuth(GetBalance.Request.of(userId));
		return ResponseEntity.ok(balance);
	}
}
