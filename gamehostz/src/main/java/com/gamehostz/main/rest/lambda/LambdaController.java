package com.gamehostz.main.rest.lambda;

import com.gamehostz.main.usecases.lambda.getsaves.GetSaves;
import com.gamehostz.main.usecases.lambda.getsaves.LambdaSaveGame;
import com.gamehostz.main.usecases.lambda.savesdeleted.SavesDeleted;
import java.util.List;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
// TODO lambda auth
@RequestMapping({"/api/internal/lambda"})
public class LambdaController {

	private final GetSaves getSaves;
	private final SavesDeleted savesDeleted;

	public LambdaController(GetSaves getSaves,
		SavesDeleted savesDeleted) {
		this.getSaves = getSaves;
		this.savesDeleted = savesDeleted;
	}

	@GetMapping(path = "/saves")
	ResponseEntity<List<LambdaSaveGame>> serverGameConfig() {
		List<LambdaSaveGame> saves = getSaves.execute();
		return ResponseEntity.ok(saves);
	}

	@DeleteMapping(path = "/saves")
	ResponseEntity savesDeleted(@RequestBody List<LambdaSaveGame> deletedSaveGames) {
		savesDeleted.execute(SavesDeleted.Request.of(deletedSaveGames));
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
