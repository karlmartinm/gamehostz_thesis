package com.gamehostz.main.helpers;

import com.gamehostz.main.usecases.getserverinfo.GetServerInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.HashMap;
import java.util.Map;

public class BadRequestAlertException extends AbstractThrowableProblem {
	private Logger log = LoggerFactory.getLogger(GetServerInfo.class);
	private static final long serialVersionUID = 1L;
	private final String entityName;
	private final String errorKey;

	public BadRequestAlertException(String errorKey) {
		this(null, null, errorKey);
	}

	public BadRequestAlertException(String defaultMessage, String entityName, String errorKey) {
		super(null, errorKey, Status.BAD_REQUEST, entityName, null, null, getAlertParameters(entityName, errorKey));
		this.entityName = entityName;
		this.errorKey = errorKey;
	}

	private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
		Map<String, Object> parameters = new HashMap();
		parameters.put("message", "error." + errorKey);
		parameters.put("params", entityName);
		return parameters;
	}

	public String getEntityName() {
		return this.entityName;
	}

	public String getErrorKey() {
		return this.errorKey;
	}
}
