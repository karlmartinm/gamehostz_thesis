package com.gamehostz.main.helpers;

import java.util.function.Function;

public class StreamExceptionHandler {
	public static <T, R ,E extends Exception> Function<T, R> handleException(FunctionWithException<T, R, E> functionWithException) {
		return arg -> {
			try {
				return functionWithException.apply(arg);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}
}
