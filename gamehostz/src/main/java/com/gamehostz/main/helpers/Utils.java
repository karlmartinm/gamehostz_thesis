package com.gamehostz.main.helpers;

import com.gamehostz.main.domain.gameserver.GameConfig;
import com.gamehostz.main.domain.gameserver.GameConfigFile;
import com.gamehostz.main.domain.validator.ConfigValidationException;

import static com.gamehostz.main.MainApplication.gameConfigs;
import static com.gamehostz.main.domain.validator.ValidationError.NO_CONFIG_FOUND;
import static com.gamehostz.main.domain.validator.ValidationError.SETTING_TYPE_MISSING;

public class Utils {

	private Utils() {}

	public static GameConfig getGameConfigByGameName(String gameName) {
		return gameConfigs.values().stream()
			.filter(x -> x.getAbbreviation().equalsIgnoreCase(gameName))
			.findFirst()
			.orElseThrow(() -> new ConfigValidationException(NO_CONFIG_FOUND));
	}

	public static GameConfigFile getGameConfigFileFromSettingName(GameConfig gameConfig, String name) {
		return gameConfig.getGameConfigFiles().stream()
			.filter(gameConfigFile -> gameConfigFile.getGameConfigFileSetting().stream()
				.anyMatch(gameConfigFileSetting -> gameConfigFileSetting.getName().equals(name))
			)
			.findFirst()
			.orElseThrow(() -> new ConfigValidationException(SETTING_TYPE_MISSING));
	}
}
