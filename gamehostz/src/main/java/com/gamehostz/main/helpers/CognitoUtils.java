package com.gamehostz.main.helpers;

import com.nimbusds.jwt.JWTClaimsSet;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Component
public class CognitoUtils {

	private static String COGNITO_USER_UUID_KEY = "sub";

	private CognitoUtils() {}

	public UUID getCognitoUserUUIDFromRequest() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		JWTClaimsSet claimsSet = Stream.of(principal)
			.filter(JWTClaimsSet.class::isInstance)
			.map(JWTClaimsSet.class::cast)
			.findFirst()
			.orElseThrow(() -> new CognitoUserException(CognitoUserError.INVALID_USER));

		String cognitoUserUUIDString = Stream.of(Optional.ofNullable(claimsSet.getClaim(COGNITO_USER_UUID_KEY))
			.orElseThrow(() -> new CognitoUserException(CognitoUserError.INVALID_USER)))
			.map(Object::toString)
			.findFirst()
			.orElseThrow(() -> new CognitoUserException(CognitoUserError.INVALID_USER));

		return UUID.fromString(cognitoUserUUIDString);
	}


}
