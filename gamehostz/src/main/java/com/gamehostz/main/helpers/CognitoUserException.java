package com.gamehostz.main.helpers;

public class CognitoUserException extends BadRequestAlertException {
	private static final long serialVersionUID = 1L;

	public CognitoUserException(CognitoUserError code) {
		super(code.name());
	}
}
