package com.gamehostz.main.helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamehostz.main.domain.client.ClientConfig;
import com.gamehostz.main.usecases.startserver.StartServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

@Converter
public class ClientConfigToJsonConverter implements AttributeConverter<ClientConfig, String> {

	private Logger log = LoggerFactory.getLogger(StartServer.class);
	private ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(ClientConfig clientConfig) {
		String json = "";
		try {
			json = objectMapper.writeValueAsString(clientConfig);
		} catch (JsonProcessingException e) {
			log.error("Error converting ClientConfig to clientConfigJson ClientConfig", e);
		}
		return json;
	}

	@Override
	public ClientConfig convertToEntityAttribute(String clientConfigJson) {
		ClientConfig clientConfig = null;
		try {
			clientConfig = objectMapper.readValue(clientConfigJson, ClientConfig.class);
		} catch (IOException e) {
			log.error("Error converting clientConfigJson to ClientConfig", e);
		}
		return clientConfig;
	}
}
