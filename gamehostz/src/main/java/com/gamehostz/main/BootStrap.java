package com.gamehostz.main;

import com.gamehostz.main.domain.customer.Customer;
import com.gamehostz.main.domain.customer.CustomerRepository;
import com.gamehostz.main.domain.transactions.CurrencyAccount;
import com.gamehostz.main.domain.transactions.CurrencyType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Component
public class BootStrap implements ApplicationRunner {

	@Autowired
	CustomerRepository customerRepository;

	@Value("${gamehostz.account.id}")
	private String ghAccountId;
	@Value("${gamehostz.account.usd.id}")
	private String ghUsdAccount;
	@Value("${gamehostz.account.eur.id}")
	private String ghEurAccount;
	@Value("${gamehostz.account.points.id}")
	private String ghPointsAccount;


	@Override
	public void run(ApplicationArguments args) throws Exception {
		createGHAccountsIfMissing();
	}

	private void createGHAccountsIfMissing() {
		customerRepository.findById(UUID.fromString(ghAccountId)).orElseGet(() -> {
			Set<CurrencyAccount> currencyAccounts = new HashSet<>();
			currencyAccounts.add(new CurrencyAccount(
				UUID.fromString(ghUsdAccount),
				CurrencyType.USD));
			currencyAccounts.add(new CurrencyAccount(
				UUID.fromString(ghEurAccount),
				CurrencyType.EUR));
			currencyAccounts.add(new CurrencyAccount(
				UUID.fromString(ghPointsAccount),
				CurrencyType.POINT));
			Customer ghAccount = new Customer(UUID.fromString(ghAccountId), UUID.fromString(ghAccountId), currencyAccounts);
			return customerRepository.save(ghAccount);
		});
	}
}
