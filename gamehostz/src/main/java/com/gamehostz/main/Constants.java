package com.gamehostz.main;

import java.time.ZoneId;
import java.util.Locale;
import java.util.UUID;

public final class Constants {
  public static final UUID SYSTEM_USER = UUID.fromString("45661b82-37e8-4d6f-ac58-ec1101cb2787");
  public static final ZoneId OFFSET = ZoneId.of("Europe/Helsinki");
  public static final Locale LOCALE = new Locale("et", "EE");
  public static final int SECURED_DEBIAN9_NYC3 = 54732636;

  private Constants() {}
}
