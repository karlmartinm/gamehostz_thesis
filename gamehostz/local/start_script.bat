(
	cd ../gamehostz-node-server-manager && ^
	npm run build && ^
	Xcopy "dist" "../gamehostz/local/base_image/js-dist/sm" /e /i /y /s /d && ^
	Xcopy "node_modules" "../gamehostz/local/base_image/js-dist/sm/node_modules" /e /i /y /s /d


	cd ../gamehostz-node-sm-shutdown && ^
    npm run build && ^
    Xcopy "dist" "../gamehostz/local/base_image/js-dist/sms" /e /i /y /s && ^
    Xcopy "node_modules" "../gamehostz/local/base_image/js-dist/sms/node_modules" /e /i /y /s /d


    FOR /F "tokens=*" %%i IN ('docker ps -a -q --filter="ancestor=gamehostz-local"') DO @(docker stop %%i && docker rm %%i) && ^
    docker image prune -f
) | set /P "="

cd ../gamehostz/local/base_image

docker build -t gamehostz-local .
docker run -p 3000:3000 -p 25565:25565 -p 8888:8888 -p 27102:27102/tcp -p 27131:27131/tcp -p 27102:27102/udp -p 27131:27131/udp -d -ti gamehostz-local /bin/sh
exit
