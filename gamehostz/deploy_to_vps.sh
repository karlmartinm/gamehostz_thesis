#!/bin/bash

sudo apt-get upgrade && apt-get update
sudo apt-get install openssh-client -y
sudo apt-get install sshpass -y

sshpass -p $4 ssh -o StrictHostKeyChecking=no $1@$2 docker login -u Alvar195 -p $3 docker.pkg.github.com
sshpass -p $4 ssh -o StrictHostKeyChecking=no $1@$2 docker pull docker.pkg.github.com/alvar195/gamehostz/gh-java:latest
sshpass -p $4 ssh -o StrictHostKeyChecking=no $1@$2 "docker container stop gh-java && docker container rm gh-java || true"
sshpass -p $4 ssh -o StrictHostKeyChecking=no $1@$2 docker run -d --restart unless-stopped --network host --name gh-java -p 8070:8070 -d docker.pkg.github.com/alvar195/gamehostz/gh-java:latest
