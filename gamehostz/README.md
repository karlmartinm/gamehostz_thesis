# GameHostZ  
JAVA 11 Spring Boot 2.2.0 app  
  
  
  
  
**Postgresql Setup:**  
```  
cd database  
docker build -t eg_postgresql .  
docker run -p 5432:5432 --name pg_test eg_postgresql  
```  
or run ``./create_database_vps.sh``  
  
port: 5432  
database name: gamehostz  
username: gamehostz  
  
**Graylog Setup:**  
run ``./create_graylog_vps.sh``  
port: 9000  
user: admin    
  
**Nginx Setup for vps:**  
1) ``docker-compose, app.conf, init-letsencrypt.sh`` need to contain the correct external ip  
2) Domain has to be routed to the correct external ip, in AWS that is route53 where we change gamehostz.com route  
  
cd ``cd ./nginx``  
run ``./init-letsencrypt.sh``  
ssl for: https://api.gamehostz.com -> 8066 (Node proxy)  
ssl for: https://api.gamehostz.com/api -> 8070 (main API)  
  
**Node proxy Setup:**  
set github env properties to access vps  
merge master to prod in github  
  
**main API Setup:**  
set github env properties to access vps  
merge master to prod in github  
  
**UI Setup:**  
push to master and code will be deployed to S3  
  
## Dev setup  
  
### Vue  
``npm i``  
``npm run serve``  
  
### Node Proxy  
``npm i``  
``npm start``  
  
  
### Access server manager  
* Start game server from panel  
* Copy IP  
* Connect putty user:admin with private key  
* Check startup log ``cat /var/log/cloud-init-output.log``  
* Check running server manager:  
  
``sudo su client``  
``pm2 list``  
``pm2 logs sm``  
  
  
### Java project  
run ``com.gamehostz.main.MainApplication`` with VM option ``-Dspring.profiles.active=dev``  
use ``application-dev.properties`` file with dev options  
under intellij settings enable ``annotation processing``  
install lombok plugin  
  
  
### how to test/use stripe integration locally:  
``scoop bucket add stripe https://github.com/stripe/scoop-stripe-cli.git``  
``scoop install stripe``  
``stripe listen --forward-to http://localhost:8070/api/portal/stripe``  
use the secret you get from that command in `gamehostz.stripe.secret` in application-dev.properties  
run the rest service  
test out your stripe stuff locally  
  
  
  
### CSGO update  
start snapshot in digital ocean  
log in with client to the snapshot  
``sudo su client``  
``cd ~/sm/csgo``  
``./csgoserver auto-install``  
create new snapshot with vX-active name  
this updates the server, we need to do this time to time  
  
  
  